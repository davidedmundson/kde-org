---
aliases:
- ../announce-applications-18.08.0
changelog: true
date: 2018-08-16
description: KDE stelt KDE Applicaties 18.08.0 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 18.08.0 beschikbaar
version: 18.08.0
---
16 augustus 2018. KDE Applications 18.08.0 is uitgebracht.

We werken voortdurend aan verbetering van de software in onze KDE Application series en we hopen dat u alle nieuwe verbeteringen en reparaties van bugs nuttig vindt!

### Wat is er nieuw in KDE Applicaties 18.08

#### Systeem

{{<figure src="/announcements/applications/18.08.0/dolphin1808-settings.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, de krachtige bestandsbeheerder van KDE, heeft verschillende verbeteringen ontvangen aan de kwaliteit-van leven:

- De dialoog 'Instellingen' is gemoderniseerd om beter onze richtlijnen voor ontwerpen te volgen en meer intuïtief te zijn.
- Verschillende geheugenlekken die uw computer zouden kunnen vertragen zijn geëlimineerd.
- Menu-items 'Nieuw aanmaken' zijn niet langer beschikbaar bij bekijken van de prullenbak.
- De toepassing past zich nu beter aan aan schermen met hoge resolutie.
- Het contextmenu bevat nu meer nuttige opties, waarmee u de weergavemodus kunt sorteren en meer direct kan wijzigingen.
- Sorteren op tiijd van wijzigen is nu 12 keer sneller. U kunt Dolphin nu ook weer starten wanneer u aangemeld bent als de gebruiker root. Aan ondersteuning voor wijzigen van bestanden met root als eigenaar bij gebruik van Dolphin als een normale gebruiker wordt nog steeds gewerkt.

{{<figure src="/announcements/applications/18.08.0/konsole1808-find.png" width="600px" >}}

Meerdere verbeteringen voor <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, de toepassing terminalemulator van KDE, zijn beschikbaar:

- Het widget 'Zoeken' zal nu bovenaan het venster verschijnen zonder uw werkmethode te verstoren.
- Ondersteuning voor meer escape-sequences (DECSCUSR & XTerm Alternate Scroll Mode) is toegevoegd.
- U kunt nu ook elk/alle teken(s) toekennen als een toets voor een sneltoets.

#### Grafische zaken

{{<figure src="/announcements/applications/18.08.0/gwenview1808.png" width="600px" >}}

18.08 is belangrijke uitgave voor <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, de afbeeldingsviewer en organizer van KDE. De laatste maanden hebben medewerkers gewerkt aan een serie van verbeteringen. Hoogtepunten omvatten:

- De statusbalk van Gwenview heeft nu een teller voor afbeeldingen en toont het totale aantal afbeeldingen.
- Het is nu mogelijk om te sorteren op waardering en in aflopende volgorde. Sorteren op datum scheidt nu mappen en archieven en is gerepareerd in sommige situaties.
- Ondersteuning voor slepen-en-loslaten is verbeterd om verslepen van bestanden en mappen naar de modus Weergave toe te staan om ze te tonen, evenals slepen van bekeken items naar externe toepassingen.
- Plakken van gekopieerde afbeeldingen uit Gwenview werkt nu ook voor toepassingen die alleen raw-afbeeldingsgegevens accepteren, maar geen bestandspad. Kopiëren van gewijzigde afbeeldingen wordt nu ook ondersteund.
- De dialoog voor van grootte wijzigen van afbeeldingen is herzien voor grotere bruikbaarheid en om een optie toe te voegen voor van grootte wijzigen van afbeeldingen gebaseerd op een percentage.
- Hulpmiddel voor reductie van rode ogen met een schuifregelaar voor grootte en kruisdraadcursor is gerepareerd.
- Selectie van transparante achtergrond heeft nu een optie voor 'Geen' en kan ook voor SVG's geconfigureerd worden.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-resize.png" width="600px" >}}

Zoomen van de afbeelding is gemakkelijker geworden:

- Ingeschakeld zoomen door schuiven of klikken evenals panning is ook wanneer de hulpmiddelen voor afsnijden of reductie van rode ogen actief zijn.
- Klikken met de middenknop is opnieuw omschakelen tussen passend zoomen en 100% zoom.
- Shift-midden-klikken en sneltoets Shift+F voor omschakelen gevulde zoom.
- Ctrl-klikken zoomt nu sneller en betrouwbaarder.
- Gwenview zoomt nu naar de huidige positie van de cursor voor in-/uitzoomen, gevulde en 100% zoom bewerkingen bij gebruik van de muis en sneltoetsen.

Modus afbeeldingen vergelijken ontving verschillende verbeteringen:

- Grootte en uitlijning van geselecteerde accentuering gerepareerd.
- SVG's overlapping de geselecteerde accentuering gerepareerd.
- Voor kleine SVG-afbeeldingen komen de geselecteerde accentueringen overeen met de afbeeldingsgrootte.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-sort.png" width="600px" >}}

Een aantal kleinere verbeteringen zijn geïntroduceerd om de werkmethode nog plezieriger maken:

- De vervagende overgangen tussen afbeeldingen met verschillende afmetingen en transparanties zijn verbeterd.
- De zichtbaarheid van pictogrammen in sommige zwevende knoppen zijn gerepareerd wanneer een licht kleurschema wordt gebruikt.
- Wanneer een afbeeldingen onder een nieuwe naam wordt opgeslagen, springt de viewer achteraf niet naar een niet gerelateerde afbeelding.
- Wanneer de knop voor delen wordt gebruikt en kipi-plug-ins zijn niet geïnstalleerd, zal Gwenview de gebruiker vragen deze te installeren. Na de installatie worden ze onmiddellijk getoond.
- Zijbalk voorkomt nu per ongeluk verbergen tijdens grootte wijzigen en herinnert zijn breedte.

#### Kantoor

{{<figure src="/announcements/applications/18.08.0/kontact1808.png" width="600px" >}}

<a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, de krachtige e-mailclient van KDE, heeft enige verbeteringen ondergaan in de engine voor ophalen van reisgegevens. Het ondersteunt nu UIC 918.3 en SNCF barcodes van treinkaarjes en in Wikidata op te zoeken locaties van treinstations. Ondersteuning voor meerdere reisroutes is toegevoegd en KMail heeft nu integratie met de app KDE Itinerary.

<a href='https://userbase.kde.org/Akonadi'>Akonadi</a>, het framework voor beheer van persoonlijke informatie, is nu sneller dankzij inhoud van meldingen en functies die XOAUTH ondersteunt voor SMTP, die u in staat stelt inheemse authenticatie met Gmail te gebruiken.

#### Onderwijs

<a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, de frontend van KDE naar mathematische software, slaat nu de status van panelen (\"Variabelen\", \"Help\", etc.) gescheiden op voor elke sessie. Julia-sessies zijn veel sneller geworden om aan te maken.

Gebruikservaring in <a href='https://www.kde.org/applications/education/kalgebra/'>KAlgebra</a>, onze grafische rekenmachine, is aanzienlijk verbeterd voor aanraakapparaten.

#### Hulpmiddelen

{{<figure src="/announcements/applications/18.08.0/spectacle1808.png" width="600px" >}}

Medewerkers aan <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, het veelzijdige hulpmiddel voor schermafdrukken van KDE, hebben zich gefocust op het verbeteren van de modus Rechthoekig gebied:

- In modus rechthoekig gebied is er nu een vergrootglas om u te helpen een pixel-perfecte rechthoek te selecteren.
- U kunt nu de geselecteerde rechthoek verplaatsen en van grootte wijzigen met het toetsenbord.
- Het gebruikersinterface volgt het kleurenschema van de gebruiker en de presentatie van de helptekst is verbeterd.

Om uw schermafdrukken gemakkelijker te delen met anderen, worden koppelingen voor gedeelde afbeeldingen nu automatisch gekopieerd naar het klembord. Schermafdrukken kunnen nu automatisch opgeslagen worden in door de gebruiker gespecificeerde submappen.

<a href='https://userbase.kde.org/Kamoso'>Kamoso</a>, onze webcamrecorder, is bijgewerkt om crashes te voorkomen met nieuwere GStreamer versies.

### Op bugs jagen

Meer dan 120 bugs zijn opgelost in toepassingen inclusief de Kontact Suite, Ark, Cantor, Dolphin, Gwenview, K3b, Kate, Konsole, Okular, Spectacle, Umbrello en meer!

### Volledige log met wijzigingen
