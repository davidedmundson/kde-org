---
aliases:
- ../announce-applications-14.12.3
changelog: true
date: '2015-03-03'
description: KDE stelt Applicaties 14.12.3 beschikbaar.
layout: application
title: KDE stelt KDE Applicaties 14.12.3 beschikbaar
version: 14.12.3
---
3 maart 2015. Vandaag heeft KDE de derde stabiele update vrijgegeven voor <a href='../14.12.0'>KDE Applicaties 14.12</a> Deze uitgave bevat alleen reparaties van bugs en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Met 19 aangegeven reparaties van bugs, inclusief verbeteringen aan het anagramspel Kanagram, het UML-modelleerprogramma Umbrello, de documentviewer Okular en toepassing voor geometrie Kig, aangebracht.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van Plasma Workspaces 4.11.17, KDE Development Platform 4.14.6 en de Kontact Suite 4.14.6.
