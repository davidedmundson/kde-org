---
aliases:
- ../announce-applications-18.04.3
changelog: true
date: 2018-07-12
description: KDE stelt KDE Applicaties 18.04.3 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 18.04.3 beschikbaar
version: 18.04.3
---
12 juli 2018. Vandaag heeft KDE de derde update voor stabiliteit vrijgegeven voor <a href='../18.04.0'>KDE Applicaties 18.04</a> Deze uitgave bevat alleen reparaties van bugs en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Ongeveer 20 aangegeven reparaties van bugs, die verbeteringen bevatten aan Kontact, Ark, Cantor, Dolphin, Gwenview, KMag, naast andere.

Verbeteringen bevatten:

- Compatibiliteit met IMAP-servers die hun mogelijkheden niet aangeven is hersteld
- Ark kan nu ZIP archieven uitpakken waarin juiste items voor mappen ontbreken
- KNotes notities op het scherm volgen weer de muisaanwijzer bij verplaatsing
