---
aliases:
- ../../kde-frameworks-5.16.0
date: 2015-11-13
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Monitor lib: gebruik Kformat::spelloutDuration om tijdtekst aan taalregio aan te passen
- KDE_INSTALL_DBUSINTERFACEDIR gebruiken om dbus-interfaces te installeren
- UnindexedFileIndexer: behandel bestanden die zijn verplaatst toen baloo_file niet actief was
- Verwijder Transaction::renameFilePath en voeg DocumentOperation er voor in de plaats.
- Constuctors met een enkele parameter expliciet maken
- UnindexedFileIndexer: indexeer alleen vereiste delen van bestand
- Transactie: voeg methode toe om timeInfo struct terug te geven
- Mime-types uitzonderen toegevoegd aan configuratie van balooctl
- Databases: QByteArray::fromRawData gebruiken bij doorgeven van gegevens aan een codec
- Balooctl: 'status'-commando verplaatsen naar zijn eigen klasse
- Balooctl: help-menu tonen als het commando not wordt herkend
- Balooshow: sta ons toe om bestanden op te zoeken met hun inode + devId
- Balooctl monitor: stop als baloo sterft
- MonitorCommand: zowel de gestarte als signalen bij beëindiging gebruiken
- Balooctl monitor: naar een juiste commando-klasse verplaatsen
- Dbus-melding toevoegen voor het starten/beëindigen van het indexeren van een bestand
- FileIndexScheduler: beëindig threads geforceerd bij beëindigen
- WriteTransaction commit: ophalen van de positionList vermijden tenzij vereist
- WriteTransaction: extra toekenningen in replaceDocument

### BluezQt

- isBluetoothOperational hangt nu ook af van niet geblokkeerde rfkill
- Bepalen van globale status van rfkill switch repareren
- QML API: markeer eigenschappen zonder meldingssignaal als constanten

### Extra CMake-modules

- Waarschuwing in plaats van fout als ecm_install_icons geen pictogrammen vindt. (bug 354610)
- maak het mogelijk om KDE Frameworks 5 te bouwen met een gewone qt 5.5.x geïnstalleerd uit de normale qt.io installer op mac os
- Haal instellingen in cachevariabelen niet weg in KDEInstallDirs. (bug 342717)

### Frameworkintegratie

- Stel de standaard waarde in voor WheelScrollLines
- Instellingen van WheelScrollLines met Qt &gt;= 5.5 repareren (bug 291144)
- Schakel om naar Noto lettertype voor Plasma 5.5

### KActivities

- Bouwen met Qt 5.3 gerepareerd
- De boost.optional include verplaatst naar de plaats die het gebruikt
- Het gebruik van boost.optional vervangen in vervolg door een dunner optional_view structuur
- Ondersteuning toegevoegd voor een eigen volgorde van gekoppelde resultaten
- Sta QML toe om activiteiten-KCM te activeren
- De ondersteuning voor activiteit verwijderen naar activiteiten-KCM
- Nieuwe UI voor de activiteitenconfiguratie
- Nieuwe configuratie-UI die toevoeging ondersteunt van beschrijving en achtergrondafbeelding
- UI voor instellingen is nu juist in modules opgedeeld

### KArchive

- KArchive repareren voor wijziging van gedrag in Qt 5.6
- Geheugenlekken gerepareerd, lager geheugengebruik

### KAuth

- Proxying van qInfo-berichten behandelen
- Wacht op asynchrone aanroep van de startinghelper om te eindigen alvorens het antwoord te controleren (bug 345234)
- De naam van de variabele repareren, anders is er geen manier om de include te laten werken

### KConfig

- Gebruik van ecm_create_qm_loader repareren.
- Include variabele repareren
- De variant KDE*INSTALL_FULL* gebruiken, zodat er geen meerduidigheid is
- KConfig toestaan om hulpbronnen te gebruiken als configuratiebestanden voor terugval

### KConfigWidgets

- KConfigWidgets alles bevattend maken, bundel het ene globale bestand in een hulpbron
- Maak doctools optioneel

### KCoreAddons

- KAboutData: apidoc "is is" -&gt; "is" addCredit(): ocsUserName -&gt; ocsUsername
- KJob::kill(Quiet) zou ook de gebeurtenislus moeten beëindigen
- Ondersteuning toevoegen voor bureaubladbestandsnaam naar KAboutData
- Juiste escape-teken gebruiken
- Enkele toekenningen verminderen
- Maak KAboutData::translators/setTranslators eenvoudig
- Voorbeeldcode van setTranslator repareren
- desktopparser: skip de sleutel Encoding=
- desktopfileparser: commentaar uit nakijken behandelen
- Sta instellen van servicetypen toe in kcoreaddons_desktop_to_json()
- desktopparser: ontleden van double- en bool-waarden repareren
- KPluginMetaData::fromDesktopFile() toevoegen
- desktopparser: doorgeven van relatieve paden naar servicetype bestanden toestaan
- desktopparser: meer gecategoriseerde logging gebruiken
- QCommandLineParser gebruikt -v voor --version gebruik dus gewoon --verbose
- Heel wat gedupliceerde code voor desktop{tojson,fileparser}.cpp verwijderen
- ServiceType bestanden ontleden bij lezen van .desktop bestanden
- Maak SharedMimeInfo een optioneel vereiste
- Oproep van QString::squeeze() verwijderen
- desktopparser: onnodige utf8 decodering vermijden
- desktopparser: voeg geen ander item toe als item eindigt in een scheidingsteken
- KPluginMetaData: waarschuw wanneer een lijstitem geen JSON-lijst is
- mimeTypes() aan KPluginMetaData toevoegen

### KCrash

- Zoeken naar drkonqui verbeteren en hou het standaard stil als het niet wordt gevonden

### KDeclarative

- ConfigPropertyMap kan nu worden bevraagd voor niet te veranderen configuratieopties met de methode isImmutable(key)
- Unbox QJSValue in configuratie eigenschapmap
- EventGenerator: ondersteuning voor verzenden van wheel-gebeurtenissen toevoegen
- verloren QuickViewSharedEngine initialSize bij initialiseren repareren.
- kritieke regressie voor QuickViewSharedEngine door commit 3792923639b1c480fd622f7d4d31f6f888c925b9 repareren
- laat vooraf gespecificeerde weergavegrootte vooraf gaan aan initiële objectgrootte in QuickViewSharedEngine

### KDED

- Maak doctools optioneel

### Ondersteuning van KDELibs 4

- Niet proberen een QDateTime op te slaan in mmap'ed geheugen
- Synchroniseer en adopteer uriencode.cmake uit kdoctools.

### KDesignerPlugin

- KCollapsibleGroupBox toevoegen

### KDocTools

- pt_BR-entities bijwerken

### KGlobalAccel

- Geen XOR Shift voor KP_Enter (bug 128982)
- Pak alle sleutels voor een symbool (bug 351198)
- Haal niet twee keer keysyms op bij elke indrukken van een toets

### KHTML

- Afdrukken uit KHTMLPart repareren door juist instellen van printSetting-ouder

### KIconThemes

- kiconthemes ondersteunt nu thema's ingebed in qt hulpbronnen binnen de prefix :/icons zoals Qt zelf doet voor QIcon::fromTheme
- Ontbrekende vereiste afhankelijkheden toevoegen

### KImageFormats

- image/vnd.adobe.photoshop herkennen in plaats van image/x-psd
- Draai d7f457a terug om een crash bij verlaten van de toepassing te voorkomen

### KInit

- Maak doctools optioneel

### KIO

- Proxy-url opslaan met juiste schema
- De "new file templates" in de bibliotheek kiofilewidgets meeleveren met een .qrc (bug 353642)
- Middelste muisklik juist afhandelen in navigatormenu
- kio_http_cache_cleaner toepasbaar maken in installatieprogramma/bundels van toepassingen
- KOpenWithDialog: aanmaken van desktop-bestand met leeg mimetype repareren
- Protocolinformatie lezen uit metagegevens van plugin
- Lokaal gebruik van kioslave toestaan
- Een .protocol naar JSON geconverteerd toevoegen
- Dubbel uitgeven van resultaat en ontbrekende waarschuwing wanneer tonen een ontoegankelijke map tegenkomt (bug 333436)
- Relatieve doelen van koppelingen behouden bij kopiëren van symbolische koppelingen. (bug 352927)
- Geschikte pictogrammen gebruiken voor standaard mappen in de persoonlijke map van de gebruiker (bug 352498)
- Een interface toevoegen die een plug-in toestaat om eigen overlay-pictogrammen te tonen
- KNotifications afhankelijkheid in KIO (kpac) optioneel maken
- doctools + portefeuille optioneel maken
- kio-crashes vermijden als er geen dbus-server actief is
- KUriFilterSearchProviderActions toevoegen om een lijst met acties te tonen voor zoeken naar enige tekst met web-websnelkoppelingen
- Verplaats de items voor het menu "Nieuw aanmaken" van kde-baseapps/lib/konq naar kio (bug 349654)
- konqpopupmenuplugin.desktop verplaatsen van kde-baseapps naar kio (bug 350769)

### KJS

- "_timezone" globale variabele gebruiken voor MSVC in plaats van "timezone". repareert bouwen met MSVC 2015.

### KNewStuff

- Homepagina URL en desktop-bestand van 'KDE Partition Manager' repareren

### KNotification

- Nu kparts niet langer knotifications nodig heeft, hebben alleen dingen die dit echt vereisen een vereiste op dit framework
- Beschrijving + doel voor spraak + phonon
- Afhankelijkheid van phonon optioneel maken, een pure interne wijziging, zoals voor spraak.

### KParts

- deleteLater in Part::slotWidgetDestroyed() gebruiken.
- KNotifications afhankelijkheden verwijderen uit KParts
- Functie om lokatie van ui_standards.rc op te vragen gebruiken in plaats van het hard te coderen, laat terugvalhulpbron werken

### KRunner

- RunnerManager: code voor laden van plug-in vereenvoudigen

### KService

- KBuildSycoca: altijd opslaan, zelfs als er geen wijziging in .desktop bestand is opgemerkt. (bug 353203)
- Maak doctools optioneel
- kbuildsycoca: alle bestanden mimeapps.list genoemd in de nieuwe spec ontleden.
- De grootste tijdaanduiding gebruiken in submap als tijdaanduiding van hulpbronmap.
- MIME-typen apart houden bij converteren van KPluginInfo naar KPluginMetaData

### KTextEditor

- highlighting: gnuplot: .plt extensie toevoegen
- validatiehint repareren, dank aan "Thomas Jarosch" &lt;thomas.jarosch@intra2net.com&gt;, hint over de validatie tijdens compileren toevoegen, ook
- Niet crashen wanneer commando niet beschikbaar is.
- Bug #307107 repareren
- Haskell variabelen voor accentuering beginnend met _
- git2 init vereenvoudigen, gegeven dat we een versie die recent genoeg is vereisen (bug 353947)
- standaard configuraties in hulpbron bundelen
- syntaxisaccentuering (d-g): standaard stijlen gebruiken in plaats van hard gecodeerde kleuren
- beter zoeken naar scripts, eerst in die van de gebruiker, dan in onze hulpbronnen, dan in al het andere, op die manier kan de gebruiker onze geleverde scripts overschrijven lokale exemplaren
- ook alle js-zaken in hulpbronnen inpakken, alleen 3 configuratiebestanden ontbreken en ktexteditor zou gewoon gebruikt kunnen worden als een bibliotheek zonder enig gebundeld bestand
- volgende poging: stop alle gebundelde xml-syntaxis bestanden in een hulpbron
- sneltoets voor invoermodus omschakelen toevoegen (bug 347769)
- xml-bestanden in hulpbron bundelen
- syntaxisaccentuering (a-c): naar nieuwe standaard stijlen migreren, hard gecodeerde verwijderen
- syntaxisaccentuering: hard gecodeerde kleuren verwijderen en in plaats daarvan standaard stijlen gebruiken
- syntaxisaccentuering: nieuwe standaard stijlen gebruiken (hard gecodeerde kleuren verwijderen)
- Betere "Import" standaard stijl
- Introduceer "Opslaan als met codering" om een bestand met een andere codering op te slaan, met het netjes gegroepeerde coderingsmenu dat er is en vervanging van alle opslagdialogen met de juiste van het besturingssysteem zonder deze belangrijke mogelijkheid te verliezen.
- bundel ui-bestand in lib, met mijn extensie naar xmlgui
- Afdrukken honoreert opnieuw het geselecteerde lettertype &amp; kleurschema (bug 344976)
- Breeze kleuren gebruiken voor opgeslagen en gewijzigde regels
- Verbeterde standaard kleuren van pictogramrand van schema "Normaal"
- automatisch accolade invoegen: voeg accolade alleen in wanneer de volgende letter leeg is of niet alfanumeriek
- automatisch accolade invoegen: bij verwijderen van accolade aan het begin met backspace, verwijder dan ook die aan het eind
- autobrace: maak verbinding slechts een keer
- Automatisch accolade invoegen: verwijder sluithaakjes onder bepaalde voorwaarden
- shortcutoverride die niet wordt doorgegeven aan het hoofdvenster repareren
- Bug 342659 - standaard kleur van "accentuering van haakje" is moeilijk te zien (in normale schema gerepareerd) (bug 342659)
- Juiste standaard kleuren voor kleur van "Huidig regelnummer"
- haken overeen laten komen &amp; automatische haken: delen code
- haken overeen laten komen: waken tegen negatieve maxLines
- automatisch haken overeen laten komen: omdat de nieuwe reeks overeenkomt met de oude betekent niet dat bijwerken niet nodig is
- Voeg de breedte van een halve spatie toe om de cursor te tekenen aan EOL (einde van de regel)
- HiDPI problemen in de rand van een pictogram repareren
- bug #310712 repareren: verwijder spaties aan het eind ook op de regel met de cursor (bug 310712)
- alleen bericht "markering ingesteld" tonen wanneer invoermodus van vi actief is
- &amp; verwijderen uit de tekst op een knop (bug 345937)
- bijwerken van de kleur van het huidige regelnummer repareren (bug 340363)
- blokhaak invoegen implementeren bij het schrijven van een blokhaak over een selectie (bug 350317)
- automatische blokhaakjes (bug 350317)
- alert HL repareren (bug 344442)
- kolom niet scrollen met dynamische regelafbreking aan
- accentuering herinneren over sessies als deze door de gebruiker was gezet, om deze niet te verliezen na opslaan en herstellen (bug 332605)
- In- uitvouwen voor tex repareren (bug 328348)
- bug #327842 gerepareerd: einde van C-stijl commentaar is fout gedetecteerd (bug 327842)
- dynamische regelafbreking opslaan/herstellen bij opslaan/herstel van sessie (bug 284250)

### KTextWidgets

- Een nieuw submenu aan KTextEdit toevoegen om te schakelen tussen talen voor spellingcontrole
- Laden van Sonnet standaard instellingen repareren

### KWallet Framework

- KDE_INSTALL_DBUSINTERFACEDIR gebruiken om dbus-interfaces te installeren
- Bestandswaarschuwingen van configuratie van KWallet repareren bij aanmelden (bug 351805)
- De kwallet-pam-uitvoer een juist voorvoegsel geven

### KWidgetsAddons

- In te klappen containerwidget toevoegen, KCollapsibleGroupBox
- KNewPasswordWidget: initialisatie van kleur ontbreekt
- KNewPasswordWidget introduceren

### KXMLGUI

- kmainwindow: Vul informatie over de vertaler vooraf in indien beschikbaar. (bug 345320)
- De contextmenutoets (rechtsonder) toestaan deze te koppelen aan sneltoetsen (bug 165542)
- Functie toevoegen om xml bestandslocaties van standards af te vragen
- Het framework kxmlgui toestaan om gebruikt te worden zonder een enkel geïnstalleerd bestand
- Ontbrekende vereiste afhankelijkheden toevoegen

### Plasma Framework

- TabBar items die samengebald zijn bij initiële creatie repareren, wat gezien kan worden in bijv. Kickoff na starten van Plasma
- Laten vallen van bestanden op het bureaublad/paneel en geen selectie bieden van te nemen acties repareren
- QApplication::wheelScrollLines in beschouwing nemen vanuit ScrollView
- BypassWindowManagerHint alleen gebruiken op platform X11
- oude paneelachtergrond verwijderen
- beter leesbare draaischakelaar bij kleine afmetingen
- gekleurde weergavegeschiedenis
- agenda: maak het gehele header-gebied aan te klikken
- agenda: nummer van de huidige dag niet gebruiken in goToMonth
- agenda: bijwerken van decade-overzicht repareren
- Thema breeze pictogrammen bij laden via IconItem
- Eigenschap minimumWidth van knop repareren (bug 353584)
- Introduceert appletCreated signaal
- Plasma Breeze pictogram: touchpad voegt svg id-elementen toe
- Plasma Breeze pictogram: touchpad wijzigen naar grootte 22x22px
- Breeze pictogram: widget-pictogrammen toevoegen aan notities
- Een script om hard gecodeerde kleuren te vervangen door stijlsheets
- SkipTaskbar toepassen bij ExposeEvent
- SkipTaskbar niet zetten bij elke gebeurtenis

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
