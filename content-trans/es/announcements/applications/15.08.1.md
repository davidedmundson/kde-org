---
aliases:
- ../announce-applications-15.08.1
changelog: true
date: 2015-09-15
description: KDE lanza las Aplicaciones de KDE 15.08.1
layout: application
title: KDE lanza las Aplicaciones de KDE 15.08.1
version: 15.08.1
---
Hoy, 15 de septiembre de 2015, KDE ha lanzado la primera actualización de estabilización para las <a href='../15.08.0'>Aplicaciones 15.08</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 40 correcciones de errores registradas, se incluyen mejoras en kdelibs, kdepim, kdenlive, dolphin, marble, kompare, konsole, ark y umbrello.

También se incluye la versión de la Plataforma de desarrollo de KDE 4.14.12 que contará con asistencia a largo plazo.
