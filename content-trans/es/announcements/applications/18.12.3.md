---
aliases:
- ../announce-applications-18.12.3
changelog: true
date: 2019-03-07
description: KDE lanza las Aplicaciones 18.12.2.
layout: application
major_version: '18.12'
release: applications-18.12.3
title: KDE lanza las Aplicaciones de KDE 18.12.3
version: 18.12.3
---
{{% i18n_date %}}

KDE ha lanzado hoy la tercera actualización de estabilización para las <a href='../18.12.0'>Aplicaciones de KDE 18.12</a>. Esta versión solo contiene correcciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de veinte correcciones de errores registradas, se incluyen mejoras en Kontact, Ark, Cantor, Dolphin, Filelight, JuK, Lokalize y Umbrello, entre otras aplicaciones.

Las mejoras incluyen:

- Se ha corregido la carga de archivos comprimidos .tar.zstd en Ark.
- Dolphin ya no se cuelga al detener actividades de Plasma.
- Filelight ya no se cuelga al cambiar a una partición distinta.
