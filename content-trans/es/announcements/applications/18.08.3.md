---
aliases:
- ../announce-applications-18.08.3
changelog: true
date: 2018-11-08
description: KDE lanza las Aplicaciones de KDE 18.08.3
layout: application
title: KDE lanza las Aplicaciones de KDE 18.08.3
version: 18.08.3
---
8 de noviembre de 2018. KDE ha lanzado hoy la tercera actualización de estabilización para las <a href='../18.08.0'>Aplicaciones de KDE 18.08</a>. Esta versión solo contiene correcciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 20 correcciones de errores registradas, se incluyen mejoras para Kontact, Ark, Dolphin, los juegos de KDE, Kate, Okular y Umbrello, entre otras aplicaciones.

Las mejoras incluyen:

- Ahora se recuerda el modo de visualización HTML en KMail, y de nuevo se cargan las images externas, si está permitido.
- Kate recuerda ahora metainformación (marcadores incluidos) entre las sesiones de edición.
- El desplazamiento automático en la interfaz de texto de Telepathy se ha corregido con nuevas versiones de QtWebEngine.
