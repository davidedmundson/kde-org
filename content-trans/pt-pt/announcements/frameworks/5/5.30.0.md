---
aliases:
- ../../kde-frameworks-5.30.0
date: 2017-01-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Ícones do Brisa

- adição dos ícones do Haguichi de Stephen Brandt (obrigado)
- finalização do suporte para os ícones do Calligra
- adição do ícone do CUPS, graças ao 'colin' (erro 373126)
- actualização do ícone do KAlarm (erro 362631)
- adição do ícone do 'krfb' (erro 373362)
- adição do suporte para o tipo MIME 'r' (erro 371811)
- entre outros

### Módulos Extra do CMake

- appstreamtest: lidar com os programas não-instalados
- Activação de avisos coloridos no resultado do Ninja
- Correcção do '::' em falta na documentação da API para despoletar o estilo do código
- Ignorar os ficheiros 'libs/includes/cmakeconfig' da máquina nas ferramentas do Android
- Uso da documentação do 'gnustl_shared' com as ferramentas do Android
- Nunca usar o -Wl,--no-undefined no Mac (APPLE)

### Integração da Plataforma

- Melhorias no KNSHandler do KPackage
- Pesquisa por uma versão mais correcta do AppstreamQt
- Adição do suporte do KNetStuff para o KPackage

### KActivitiesStats

- Capacidade de compilar com o '-fno-operator-names'
- Não obter mais recursos ligados quando um deles desaparecer
- Adição do papel do modelo para obter a lista de actividades ligadas para um recurso

### Ferramentas de Doxygen do KDE

- Adição do Android à lista de plataformas disponíveis
- Inclusão dos ficheiros de código do ObjC++

### KConfig

- Geração de uma instância com o KSharedConfig::Ptr para o 'singleton' e 'arg'
- kconfig_compiler: uso de ponteiros nulos no código gerado

### KConfigWidgets

- Esconder a acção "Mostrar o Menu" se todos os menus forem nativos
- KConfigDialogManager: eliminação das classes do kdelibs3

### KCoreAddons

- Devolução de tipos 'stringlist/boolean' no KPluginMetaData::value
- DesktopFileParser: Tratamento do campo ServiceTypes

### KDBusAddons

- Adição de interfaces em Python para o KDBusAddons

### KDeclarative

- Introdução da importação do QML do org.kde.kconfig com o KAuthorized

### KDocTools

- kdoctools_install: correspondência à localização completa do programa (erro 374435)

### KGlobalAccel

- [runtime] Introdução de uma variável de ambiente KGLOBALACCEL_TEST_MODE

### Extensões da GUI do KDE

- Adição de interfaces para o KGuiAddons

### KHTML

- Definição de dados do 'plugin' para que funcione o Visualizador de Imagens Incorporado

### KIconThemes

- Informação ao QIconLoader também quando o tema de ícones da área de trabalho mudar (erro 365363)

### KInit

- Validação da propriedade X-KDE-RunOnDiscreteGpu ao iniciar a aplicação com o 'klauncher'

### KIO

- O KIO::iconNameForUrl irá agora devolver ícones especiais para as localizações o XDG, como a pasta de Imagens do utilizador
- ForwardingSlaveBase: correcção da passagem da opção Overwrite ao kio_desktop (erro 360487)
- Melhoria e exportação da classe KPasswdServerClient, a API do cliente do 'kpasswdserver'
- [KPropertiesDialog] Eliminação da opção "Colocar na bandeja do sistema"
- [KPropertiesDialog] Não mudar o "Name" dos ficheiros .desktop do "Atalho", se o nome do ficheiro for apenas para leitura
- [KFileWidget] Uso do urlFromString em vez do QUrl(QString) no setSelection (erro 369216)
- Adição de opção para executar uma aplicação numa placa gráfica separada no KPropertiesDialog
- finalização adequada do DropJob após largar um item sobre um executável
- Correcção do uso de categorias de registo no Windows
- DropJob: emitir um 'started' da tarefa de cópia após a criação
- Adição do suporte para a chamada suspend() num CopyJob antes de o iniciar
- Adição do suporte para a suspensão imediata das tarefas, pelo menos para o SimpleJob e o FileCopyJob

### KItemModels

- Actualização de 'proxies' para a classe de erros realizada recentemente (tratamento do 'layoutChanged')
- Possibilidade de o KConcatenateRowsProxyModel funcionar no QML
- KExtraColumnsProxyModel: Persistência dos índices dos modelos após a emissão do  'layoutChange', não antes
- Correcção de validação 'assert' (no beginRemoveRows) ao desmarcar um filho vazio de um filho seleccionado no KOrganizer

### KJobWidgets

- Não colocar as janelas de progresso em primeiro plano (erro 333934)

### KNewStuff

- [Botão do GHNS] Esconder quando se aplicar a restrição do KIOSK
- Correcção da configuração do ::Engine quando for criado com um local absoluto do ficheiro de configuração

### KNotification

- Adição de um teste manual para os lançamentos do Unity
- [KNotificationRestrictions] Possibilidade ao utilizador de definir o texto da razão da restrição

### Plataforma KPackage

- [PackageLoader] Não aceder a KPluginMetadata's inválidos (erro 374541)
- correcção da descrição da opção '-t' na página de manual
- Melhoria nas mensagens de erro
- Correcção da mensagem de ajuda do '--type'
- Melhoria no processo de instalação dos pacotes do KPackage
- Instalação de um ficheiro 'kpackage-generic.desktop'
- Exclusão dos ficheiros do 'qmlc' na instalação
- Não listar os plasmóides em separado nos ficheiros 'metadata.desktop' e '.json'
- Correcção adicional para os pacotes com tipos diferentes mas ID's iguais
- Correcção no erro do 'cmake' quando dois pacotes com tipos diferentes têm o mesmo ID

### KParts

- Invocação do novo checkAmbiguousShortcuts() do MainWindow::createShellGUI

### KService

- KSycoca: não seguir as ligações simbólicas para pastas, dado que cria o perigo de recursividade

### KTextEditor

- Correcção: Encaminhamento do arrastamento dos resultados de texto numa selecção inválida (erro 374163)

### Plataforma da KWallet

- Definição do GpgME++ com a versão mínima obrigatória 1.7.0
- Reversão do "Se não for encontrado o 'Gpgmepp', tentar usar o KF5Gpgmepp"

### KWidgetsAddons

- Adição de interfaces em Python
- Adição do KToolTipWidget, uma dica que contém outro elemento
- Correcção das verificações do KDateComboBox para as datas introduzidas de forma válida
- KMessageWidget: uso de uma cor vermelha mais escura quando o tipo é Error (erro 357210)

### KXMLGUI

- Aviso no arranque sobre atalhos de teclado ambíguos (com a excepção do Shift+Delete)
- O MSWin e o Mac têm comportamentos de gravação automática do tamanho da janela semelhantes
- Mostrar a versão da aplicação no cabeçalho da janela 'Acerca' (erro 372367)

### Ícones do Oxygen

- sincronização com os ícones do Brisa

### Plataforma do Plasma

- Correcção das propriedades 'renderType' para os vários componentes
- [ToolTipDialog] Uso do KWindowSystem::isPlatformX11(), que está em 'cache'
- [Item de Ícones] Correcção da actualização do tamanho implícito quando o tamanho dos ícones muda
- [Janela] Uso do setPosition / setSize em vez de definir tudo individualmente
- [Unidades] Tornar a propriedade 'iconSizes' constante
- Agora existe uma instância global do "Units" que reduz o consumo de memória e o tempo de criação dos itens SVG
- [Item de Ícones] Suporte para ícones não-quadrados (erro 355592)
- pesquisa/substituição de tipos antigos fixos no código do 'plasmapkg2' (erro 374463)
- Correcção dos tipos X-Plasma-Drop* (erro 374418)
- [ScrollViewStyle do Plasma] Mostrar o fundo da barra de deslocamento apenas à passagem do cursor
- Correcção do #374127: má colocação das mensagens nas janelas acopláveis
- Descontinuação da API Plasma::Package no PluginLoader
- Verificação secundaria da representação a usar no 'setPreferredRepresentation'
- [declarativeimports] Uso do QtRendering nos telefones
- um painel vazio tem sempre "applets carregadas" (erro 373836)
- Emissão do toolTipMainTextChanged se mudar como resposta a uma mudança de título
- [TextField] Possibilidade de desactivação do botão de visualização da senha através de restrição do KIOSK
- [AppletQuickItem] Suporte à mensagem de erro no lançamento
- Correcção da lógica para o tratamento de setas nas línguas RTL (erro 373749)
- TextFieldStyle: Correcção do valor do 'implicitHeight' para que o cursor de texto fique centrado

### Sonnet

- cmake: verificação também do hunspell-1.6

### Realce de Sintaxe

- Correcção do realce do Makefile.inc, adicionando a prioridade ao makefile.xml
- Realce dos ficheiros SCXML como XML
- makefile.xml: diversas melhorias, lista demasiado longa para a apresentar aqui
- sintaxe de python: adição dos literais-f e do tratamento de textos

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
