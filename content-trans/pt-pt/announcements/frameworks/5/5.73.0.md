---
aliases:
- ../../kde-frameworks-5.73.0
date: 2020-08-01
layout: framework
libCount: 70
---
### Ícones do Brisa

+ Centrar os ícones a 16px 'help-about' e 'help-whatsthis'
+ Adição do ícone 'kirigami-gallery'
+ Adição do ícone do Kontrast
+ Adição do ícone do tipo MIME applications/pkcs12
+ Fazer com que os ícones 'audio-volume' do Brisa Escuro correspondam ao Brisa
+ Mudança do estilo do microfone para ficar mais consistente com outros ícones de audio e mais alinhadas ao pixel
+ Uso de uma opacidade de 35% para as ondas desvanecidas nos ícones 'audio-volume' e tornar as ondas de silêncio desvanecidas
+ Tornar iguais os ícones 'audio-off' e 'audio-volume-muted'
+ Adição do ícone 'snap-angle'
+ Correcção da associação do application-x-ms-shortcut de 22px a um ícone a 16px
+ Correcção de inconsistências nos ícones de tipos MIME entre as versões claras/escuras
+ Correcção das diferenças invisíveis entre claro/escuro nos ícones do 'vscode' e do 'wayland'
+ Adição do ícone 'document-replace' (Como na acção Sobrepor)
+ Adição de modelo para criar programas em Python que editam SVG's
+ Adição do ícone do estado SMART (erro 423997)
+ Adição dos ícones "task-recurring" e "appointment-recurring" (erro 397996)

### Módulos Extra do CMake

+ Adição do ecm_generate_dbus_service_file
+ Adição do 'ecm_install_configured_file'
+ Exportação do Wayland_DATADIR

### KActivitiesStats

+ Ignorar o BoostConfig.cmake se presente (erro 424799)

### KCMUtils

+ Suporte para o indicador de realce dos módulos baseados no QWidget e no QtQuick
+ Adição de um método para limpar o selector do 'plugin' (erro 382136)

### KConfig

+ Actualização do 'sGlobalFileName' quando o TestMode do QStandardPaths está activado
+ Documentação da API: é esperada uma codificação explícita do estado para os nomes dos grupos &amp; e chaves do KConfig

### KConfigWidgets

+ KCModule: Indicação quando uma configuração foi alterada face ao valor predefinido
+ Ao repor a predefinição do sistema, não usar a paleta-padrão do estilo

### KCoreAddons

+ Introdução do KRandom::shuffle(contentor)

### KDeclarative

+ SettingStateBinding : exposição se o realce não-predefinido está activo
+ Validação de que o KF5CoreAddons está instalado antes de usar o KF5Declarative
+ Adição do KF5::CoreAddons à interface pública do KF5::QuickAddons
+ Introdução dos elementos SettingState* para facilitar a criação de KCM's
+ suporte para as notificações de configuração no 'configpropertymap'

### Extensões da GUI do KDE

+ Migração do KCursorSaver a partir do libkdepim, melhorada

### KImageFormats

+ Adaptação da licença para LGPL-2.0-ou-posterior

### KIO

+ KFilterCombo: o 'application/octet-stream' também possui o 'hasAllFilesFilter'
+ Introdução do OpenOrExecuteFileInterface para lidar com a abertura de executáveis
+ RenameDialog: Mostrar se os ficheiros forem idênticos (erro 412662)
+ [janela de mudança de nomes] Migração do botão Sobrepor para o KStandardGuiItem::Overwrite (erro 424414)
+ Mostrar até três acções de itens de ficheiros de forma incorporada, e não apenas uma (erro 424281)
+ KFileFilterCombo: Mostrar as extensões se existir um comentário de MIME repetido
+ [KUrlCompletion] Não adicionar o '/' às pastas completas
+ [Properties] Adição do algoritmo SHA512 ao elemento de códigos de validação (erro 423739)
+ [WebDav] Correcção das cópias que incluem substituições no 'slave' de WebDAV (erro 422238)

### Kirigami

+ Suporte para a visibilidade das acções no GlobalDrawer
+ Introdução do componente FlexColumn
+ Optimizações para formatos móveis
+ Uma camada também deverá cobrir a barra de páginas
+ PageRouter: uso de facto das páginas pré-carregadas no 'push'
+ Melhoria da documentação do (Abstract)ApplicationItem
+ Correcção de estoiros de memória no 'teardown' do PageRouter
+ Possibilitar o deslocamento da ScrollablePage com o teclado
+ Melhoria da acessibilidade no campos de entrada do Kirigami
+ Correcção da compilação estática
+ PageRouter: Adição de API's de conveniência para outras tarefas manuais
+ Introdução da API do ciclo de vida do PageRouter
+ Forçar uma ordem de Z baixa na implementação por 'software' do ShadowedRectangle

### KItemModels

+ KSelectionProxyModel: permitir usar o modelo com o contexto no estilo novo
+ KRearrangeColumnsProxyModel: correcção do hasChildren() quando não estiverem configuradas colunas

### KNewStuff

+ [Janela do QtQuick] uso do ícone de actualização que existir (erro 424892)
+ [Janela do GHNS do QtQuick] Melhoria dos textos da lista suspensa
+ Não mostrar o selector do 'downloaditem' para apenas um item de transferência
+ Ajustar um pouco a disposição para um formato mais equilibrado
+ Correcção da disposição do cartão de estado do EntryDetails
+ Só compilar com o Core, não com o 'plugin' 'quick' (porque não irá funcionar)
+ Algum embelezamento do ecrã de boas-vindas (e não usar janelas em QML)
+ Uso da janela quando é passado um ficheiro 'ksnrc', caso contrário não a usa
+ Remoção do botão da janela principal
+ Adição de uma janela (para mostrar a outra janela quando é passado um ficheiro)
+ Adição dos novos blocos para a aplicação principal
+ Adição de um modelo simples (pelo menos por agora) para mostrar os ficheiros 'ksnrc'
+ Migração do ficheiro QML principal para um QRC
+ Passage da ferramenta de testes do Dialog do KNewStuff para uma ferramenta adequada
+ Permitir a remoção de itens actualizáveis (erro 260836)
+ Não colocar em primeiro plano o primeiro elemento automaticamente (erro 417843)
+ Corrigir a apresentação dos botões de Detalhes e Desinstalar no modo de visualização Lado-a-Lado (erro 418034)
+ Correcção do movimento dos botões quando é inserido o texto da pesquisa (erro 406993)
+ Correcção do parâmetro em falta para o texto traduzido
+ Correcção da lista de instalação dos detalhes na janela do QWidgets (erro 369561)
+ Adição de dicas para os diferentes modos de visualização na janela em QML
+ Marcar o item como não-instalado se a instalação falhar (erro 422864)

### KQuickCharts

+ Ter também as propriedades do modelo em conta ao usar o ModelHistorySource

### KRunner

+ Implementação do monitor do KConfig para os 'plugins' e KCM's de execução activos (erro 421426)
+ Não remover o método virtual da compilação (erro 423003)
+ Descontinuação do AbstractRunner::dataEngine(...)
+ Correcção dos módulos de execução e da configuração de execução desactivados do plasmóide
+ Atraso da emissão dos avisos de migração dos meta-dados até ao KF 5.75

### KService

+ Adição de substituto para invocar o terminal com as variáveis ENV (erro 409107)

### KTextEditor

+ Adição de ícones para todos os botões da mensagem de ficheiro modificado (erro 423061)
+ Uso dos URL's canónicos do docs.kde.org

### Plataforma da KWallet

+ uso de um Name melhor e respeito do HIG
+ Marcação da API como descontinuada também na descrição da interface de D-Bus
+ Adição de uma cópia do org.kde.KWallet.xml sem a API descontinuada

### KWayland

+ plasma-window-management: Adaptação às modificações no protocolo
+ PlasmaWindowManagement: adopção das alterações no protocolo

### KWidgetsAddons

+ KMultiTabBar: pintura dos ícones das páginas activos à passagem do rato
+ Correcção do KMultiTabBar para pintar o ícone deslocado para baixo/assinalado de acordo com o desenho do estilo
+ Uso do novo ícone de sobreposição para o item gráfico de Sobreposição (erro 406563)

### KXMLGUI

+ Tornar o KXmlGuiVersionHandler::findVersionNumber público no KXMLGUIClient

### NetworkManagerQt

+ Remoção do registo de senhas

### Plataforma do Plasma

+ Configuração da pertença do 'cpp' na instância 'units'
+ Adição de algumas importações do PlasmaCore em falta
+ Remoção da utilização das propriedades de contexto do tema e unidades
+ PC3: Melhoria da aparência do Menu
+ Ajuste dos ícones de áudio de novo para corresponder aos ícones do Brisa
+ Possibilitar a invocação do showTooltip() a partir do QML
+ [PlasmaComponents3] Respeito da propriedade 'icon.[name|source]'
+ Filtrar com base nos factores de formato se definidos
+ Actualização do estilo do ícone de silêncio para corresponder aos ícones do Brisa
+ Remoção do registo inválido de tipos MIME para os tipos onde falte o "*" no nome
+ Uso de uma opacidade de 35% para os elementos desvanecidos nos ícones da rede
+ Não mostrar as janelas do Plasma nos selectores de tarefas (erro 419239)
+ Correcção do erro no URL do Qt para o truque do desenho de texto
+ Não usar o cursor da mão por não ser consistente
+ [ExpandableListItem] uso de tamanhos-padrão dos botões
+ [PlasmaComponents3] Mostrar o efeito de foco do ToolButton e omitir a sombra se for plano
+ Unificação da altura dos botões, campos de texto e listas suspensas do PC3
+ [PlasmaComponents3] Adição das funcionalidades em falta do TextField
+ [ExpandableListItem] Correcção de um erro disparatado
+ Remodelação do button.svg para o tornar mais fácil de compreender
+ Cópia dos encaminhamentos do DataEngine antes da iteração (erro 423081)
+ Tornar a potência do sinal dos ícones da rede mais visível (erro 423843)

### QQC2StyleBridge

+ Uso do estilo "elevado" para os botões de ferramentas não-planos
+ Só reservar o espaço para o ícone no botão de ferramentas se existir de facto um ícone
+ Suporte da apresentação de uma seta do menu nos botões de ferramentas
+ Corrigir de facto a altura do separador do menu com PPP's elevados
+ Actualização do Mainpage.dox
+ Correcção da altura adequada do MenuSeparator (erro 423653)

### Solid

+ Limpeza do 'm_deviceCache' antes de efectuar uma introspecção de novo (erro 416495)

### Realce de Sintaxe

+ Conversão do DetectChar para o Detect2Chars adequado
+ Mathematica: algumas melhorias
+ Doxygen: correcção de alguns erros ; DoxygenLua: começa apenas com um --- ou --!
+ AutoHotkey: remodelação completa
+ Nim: correcção dos comentários
+ Adição do realce de sintaxe do Nim
+ Scheme: correcção do identificador
+ Scheme: adição do comentário de dados, comentário encadeados e outras melhorias
+ Substituição de algumas expressões regulares por AnyChar, DetectChar, Detect2Chars ou StringDetect
+ language.xsd: remoção do HlCFloat e introdução do tipo 'char'
+ KConfig: correcção do $(...) e dos operadores + algumas melhorias
+ ISO C++: correcção da performance no realce dos números
+ Lua: atributos com o Lua54 e mais algumas melhorias
+ substituição do RegExpr=[.]{1,1} pelo DetectChar
+ Adição do realce de PureScript com base nas regras do Haskell. Ele gera uma boa aproximação e ponto de partida para o PureScript
+ README.md: uso dos URL's canónicos do docs.kde.org

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
