---
aliases:
- ../announce-applications-17.04.0
changelog: true
date: 2017-04-20
description: O KDE Lança as Aplicações do KDE 17.04.0
layout: application
title: O KDE Lança as Aplicações do KDE 17.04.0
version: 17.04.0
---
20 de Abril de 2017. As Aplicações do KDE 17.04 estão aí. De forma global, temos trabalhado para tornar as aplicações e as bibliotecas subjacentes mais estáveis e simples de usar. Ao eliminar inconsistências e ao ouvir as suas reacções, a equipa tornou o pacote das Aplicações do KDE menos susceptíveis a problemas e muito mais amigáveis na utilização.

Desfrute das suas novas aplicações!

#### <a href="https://edu.kde.org/kalgebra/">KAlgebra</a>

{{<figure src="/announcements/applications/17.04.0/kalgebra1704.jpg" width="600px" >}}

Os programadores do KAlgebra estão no seu caminho de convergência em particular, tendo migrado a versão móvel do pacote educativo abrangente para o Kirigami 2.0 -- a plataforma preferida para integrar as aplicações do KDE nas plataformas de computadores pessoais e dispositivos móveis.

Para além disso, a versão para computador também migrou a infra-estrutura de 3D para o GLES, a aplicação que permite ao programa desenhar funções 3D tanto no computador como nos dispositivos móveis. Isto torna o código mais simples e fácil de manter.

#### <a href="http://kdenlive.org/">Kdenlive</a>

{{<figure src="/announcements/applications/17.04.0/kdenlive1704.png" width="600px" >}}

O editor de vídeo do KDE está a tornar-se mais estável e mais rico em funcionalidades para cada nova versão. Desta vez, os programadores remodelaram a janela de selecção de perfis para tornar mais simples a definição do tamanho do ecrã, a taxa de imagens e outros parâmetros do seu filme.

Agora também poderá reproduzir directamente o seu vídeo a partir da notificação, assim que terminar a composição do mesmo. Alguns estoiros que ocorriam quando se moviam os 'clips' ao longo da linha temporal foram corrigidos; por outro lado, o Assistente de DVD's foi melhorado.

#### <a href="https://userbase.kde.org/Dolphin">Dolphin</a>

{{<figure src="/announcements/applications/17.04.0/dolphin1704.png" width="600px" >}}

O nosso explorador de ficheiros favorito e portal para tudo (excepto talvez o mundo subterrâneo) teve diversas remodelações e melhorias de usabilidade para o tornar ainda mais poderoso.

Os menus de contexto no painel <i>Locais</i> (por omissão, à esquerda na área de visualização principal) foram limpos e agora é possível interagir com os elementos de meta-dados nas dicas. As dicas, já agora, também funcionam agora no Wayland.

#### <a href="https://www.kde.org/applications/utilities/ark/">Ark</a>

{{<figure src="/announcements/applications/17.04.0/ark1704.png" width="600px" >}}

A conhecida aplicação gráfica para criar, descomprimir e gerir pacotes comprimidos para os ficheiros e pastas agora vem com uma função <i>Procurar</i> para o ajudar a encontrar ficheiros em pacotes cheios de ficheiros.

Também lhe permite activar e desactivar os 'plugins' directamente a partir da janela para <i>Configurar</i>. Por falar em 'plugins', o novo 'plugin' da Libzip melhora o suporte para pacotes ZIP.

#### <a href="https://minuet.kde.org/">Minuet</a>

{{<figure src="/announcements/applications/17.04.0/minuet1704.png" width="600px" >}}

Se estiver a ensinar ou a aprender música, terá de ver o Minuet. A nova versão oferece mais exercícios de escalas e tarefas de exercício auditivo para escalas 'bebop', maiores/menores harmónicas, pentatónicas e simétricas.

Também poderá definir ou fazer testes com o novo <i>Modo de testes</i> para exercícios de respostas. Poderá vigiar o seu progresso, executando uma sequência de 10 exercícios e irá obter estatísticas proporcionais quando terminar.

### E ainda há mais!

O <a href='https://okular.kde.org/'>Okular</a>, o visualizador de documentos do KDE, recebeu pelo menos uma dúzia de alterações que adicionam funcionalidades e que adequam a sua usabilidade aos ecrãs tácteis. O <a href='https://userbase.kde.org/Akonadi'>Akonadi</a> e diversas outras aplicações que compõem o <a href='https://www.kde.org/applications/office/kontact/'>Kontact</a> (o pacote de e-mail/calendários/'groupware' do KDE) foram revistas, depuradas e optimizadas para o ajudar a tornar-se mais produtivo.

O <a href='https://www.kde.org/applications/games/kajongg'>Kajongg</a>, o <a href='https://www.kde.org/applications/development/kcachegrind/'>KCachegrind</a> e outros (<a href='https://community.kde.org/Applications/17.04_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>Notas de Lançamento</a>) foram agora migrados para as Plataformas do KDE 5 e estamos à espera da sua reacção e opiniões sobre as funcionalidades mais recentes que foram introduzidas com esta versão.

O <a href='https://userbase.kde.org/K3b'>K3b</a> juntou-se ao pacote lançado das Aplicações do KDE.

### Eliminação de Erros

Foram resolvidos mais de 95 erros nas aplicações, incluindo o Kopete, o KWalletManager, o Marble, o Spectacle, entre outros!

### Registo de Alterações Completo
