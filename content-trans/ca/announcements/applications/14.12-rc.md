---
aliases:
- ../announce-applications-14.12-rc
custom_spread_install: true
date: '2014-11-27'
description: KDE distribueix la versió candidata 14.12 de les aplicacions del KDE.
layout: application
title: KDE distribueix la versió candidata 14.12 de les aplicacions del KDE
---
27 de novembre de 2014. Avui KDE distribueix la versió candidata de les noves versions de les aplicacions del KDE. S'han congelat les dependències i les funcionalitats, i ara l'equip del KDE se centra a corregir els errors i acabar de polir-la.

Amb diverses aplicacions basades en els Frameworks 5 del KDE, la distribució 14.12 de les aplicacions del KDE necessita una prova exhaustiva per tal de mantenir i millorar la qualitat i l'experiència d'usuari. Els usuaris reals són imprescindibles per mantenir l'alta qualitat del KDE, perquè els desenvolupadors no poden provar totes les configuracions possibles. Comptem amb vós per ajudar a trobar errors amb anticipació, a fi que es puguin solucionar abans de la publicació final. Considereu unir-vos a l'equip instal·lant la versió candidata <a href='https://bugs.kde.org/'>i informant de qualsevol error</a>.

#### Instal·lació dels paquets executables 14.12 versió candidata de les aplicacions del KDE

<em>Paquets</em>. Alguns venedors de Linux/UNIX OS han proporcionat gentilment els paquets executables de la 14.12 versió candidata (internament 14.11.97) de les aplicacions del KDE per a algunes versions de la seva distribució, i en altres casos ho han fet voluntaris de la comunitat. En les setmanes vinents poden arribar a estar disponibles paquets binaris addicionals, i també actualitzacions dels paquets actualment disponibles.

<em>Ubicacions dels paquets</em>. Per a una llista actual dels paquets executables disponibles dels quals s'ha informat l'equip de llançament del KDE, si us plau, visiteu el <a href='https://community.kde.org/KDE_SC/Binary_Packages#'>wiki de la comunitat</a>.

#### Compilació de les aplicacions 14.12 versió candidata del KDE

El codi font complet per a la 14.12 versió candidata de les aplicacions del KDE es pot <a href='https://download.kde.org/unstable/applications/14.11.97/src/'>descarregar de franc</a>. Les instruccions per a compilar i instal·lar el programari estan disponibles a la <a href='/info/applications/applications-14.11.97'>pàgina d'informació de la versió candidata de les aplicacions del KDE</a>.
