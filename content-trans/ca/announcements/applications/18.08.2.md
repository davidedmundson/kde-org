---
aliases:
- ../announce-applications-18.08.2
changelog: true
date: 2018-10-11
description: KDE distribueix les aplicacions 18.08.2 del KDE
layout: application
title: KDE distribueix les aplicacions 18.08.2 del KDE
version: 18.08.2
---
11 d'octubre de 2018. Avui KDE distribueix la segona actualització d'estabilització per a les <a href='../18.08.0'>aplicacions 18.08 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha més d'una dotzena d'esmenes registrades d'errors que inclouen millores al Kontact, Dolphin, Gwenview, KCalc i Umbrello, entre d'altres.

Les millores inclouen:

- En arrossegar un fitxer al Dolphin, ja no s'activarà accidentalment el canvi de nom en línia
- El KCalc torna a permetre les tecles «punt» i «coma» en introduir decimals
- S'ha solucionat un error visual a la baralla de cartes Paris per als jocs de cartes del KDE
