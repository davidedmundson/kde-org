---
date: '2014-04-16'
description: KDE Ships Applications and Platform 4.13.
hidden: true
title: KDE-sovellukset 4.13 hyötyvät uudesta semanttisesta hausta ja niissä on uusia
  ominaisuuksia
---
KDE-yhteisö tiedottaa ylpeänä suurista päivityksistä KDE-sovelluksiin. Päivityksiin kuuluu uusia ominaisuuksia sekä korjauksia. Kontactiin (henkilökohtaisten tietojen hallintaan) on tehty paljon työtä: siinä on uusia ominaisuuksia ja se hyötyy parannuksista KDE:n semanttisen haun teknologiaan. Asiakirjakatselin Okularin ja edistyneen tekstimuokkaimen Katen käyttöliittymiä on paranneltu, ja niissä on uusia ominaisuuksia. Opetusohjelmien ja pelien parissa uutta on Artikulate, ohjelma vieraiden kielen puhumisen harjoittelemiseen. Marbleen (työpöytämaapalloon) on lisätty tuki Auringolle, Kuulle, planeetoille, polkupyöräreittien haulle sekä merimaileille. Palapeli (ohjelma palapelien kokoamiseen) on hypännyt uusiin ulottuvuuksiin.

{{<figure src="/announcements/4/4.13.0/screenshots/applications.png" class="text-center" width="600px">}}

## KDE:n Kontact on nopeampi ja siinä on uusia ominaisuuksia

KDE:n Kontact Suiten ohjelmissa on uusia ominaisuuksia. KMail tukee nyt pilvitallennusta, ja siinä on parantunut Sieve-tuki palvelinpuolen suodattimille. KNotes osaa nyt luoda hälytyksiä, ja siihen on lisätty hakutoiminnallisuus. Kontactin välimuistin toimintaa on myös parannettu, mikä nopeuttaa lähes kaikkia toimintoja.

### Pilvitallennustuki

KMailissa on nyt pilvitallennustuki eli KMail voi tallentaa suuret liitteet pilvitallennuspalveluihin ja sisällyttää linkin tiedostoihin sähköpostiviesteihin. Tuettuja pilvitallennuspalveluja ovat muiden muassa Dropbox, Box, KolabServer, YouSendIt, Ubuntu One ja Hubic. Tarjolla on lisäksi yleinen WebDav-vaihtoehto. <em>storageservicemanager</em>-apuohjelma auttaa hallitsemaan näissä palveluissa olevia tiedostoja.

{{<figure src="/announcements/4/4.13.0/screenshots/CloudStorageSupport.png" class="text-center" width="600px">}}

### Paljon parempi Sieve-tuki

Sieve-suodattimet (teknologia, jonka avulla KMail pystyy käsittelemään palvelimella toimivia suodattimia) tukevat nyt poissaoloviestejä useille palvelimille. KSieveEditor-apuohjelma mahdollistaa käyttäjien muokata sieve-suodattimia lisäämättä palvelinta Kontactiin.

### Muut muutokset

Pikasuodatinpalkissa on pieniä käyttöliittymäparannuksia. Pikasuodatus hyötyy suuresti KDE-ohjelmistoalusta 4.13:n paremmista hakuominaisuuksista. Hakemisesta on tullut nopeampaa ja luotettavampaa. Viestin kirjoitusikkunaan on lisätty verkko-osoitteen lyhennystoiminto, joka täydentää nykyisiä käännös- ja tekstileiketyökaluja.

Henkilökohtaisten tietojen (PIM) tunnisteet ja merkinnät tallennetaan nyt Akonadiin. Tulevissa versioissa ne tallennetaan myös palvelimille (IMAP- tai Kolab-palvelimille), mikä mahdollistaa tunnisteiden jakamisen useiden tietokoneiden kesken. Akonadiin on lisätty Google Drive API -tuki. Akonadi tukee nyt myös kolmannen osapuolen hakuliitännäisiä (mikä tarkoittaa, että hakutulokset saadaan erittäin nopeasti) ja palvelinpuolen hakua (voidaan hakea muitakin kuin paikallisen indeksointipalvelun indeksoimia tietoja).

### KNotes ja KAddressbook

KNotesiin on tehty paljon työtä. Siitä on korjattu useita vikoja sekä pieniä ärsyttävyyksiä. Uutta on mahdollisuus asettaa hälytyksiä ja muistiinpanoista etsiminen. Voit lukea lisää englanniksi <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>täältä</a>. KAddressBookissa on nyt tulostustuki. Lisätietoja on englanniksi <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>täällä</a>.

### Suorituskykyparannuksia

Kontact performance is noticeably improved in this version. Some improvements are due to the integration with the new version of KDE’s <a href='http://dot.kde.org/2014/02/24/kdes-next-generation-semantic-search'>Semantic Search</a> infrastructure, and the data caching layer and loading of data in KMail itself have seen significant work as well. Notable work has taken place to improve support for the PostgreSQL database. More information and details on performance-related changes can be found in these links:

- Storage Optimizations: <a href='http://www.progdan.cz/2013/11/kde-pim-sprint-report/'>sprint report</a>
- speed up and size reduction of database: <a href='http://lists.kde.org/?l=kde-pim&amp;m=138496023016075&amp;w=2'>mailing list</a>
- optimization in access of folders: <a href='https://git.reviewboard.kde.org/r/113918/'>review board</a>

### KNotes ja KAddressbook

KNotesiin on tehty paljon työtä. Siitä on korjattu useita vikoja sekä pieniä ärsyttävyyksiä. Uutta on mahdollisuus asettaa hälytyksiä ja muistiinpanoista etsiminen. Voit lukea lisää englanniksi <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>täältä</a>. KAddressBookissa on nyt tulostustuki. Lisätietoja on englanniksi <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>täällä</a>.

## Okularin käyttöliittymää on hiottu

This release of the Okular document reader brings a number of improvements. You can now open multiple PDF files in one Okular instance thanks to tab support. There is a new Magnifier mouse mode and the DPI of the current screen is used for PDF rendering, improving the look of documents. A new Play button is included in presentation mode and there are improvements to Find and Undo/Redo actions.

{{<figure src="/announcements/4/4.13.0/screenshots/okular.png" class="text-center" width="600px">}}

## Katessa on parempi tilarivi, vastaavien sulkeiden animointi ja parempia liitännäisiä

The latest version of the advanced text editor Kate introduces <a href='http://kate-editor.org/2013/11/06/animated-bracket-matching-in-kate-part/'>animated bracket matching</a>, changes to make <a href='http://dot.kde.org/2014/01/20/kde-commit-digest-5th-january-2014'>AltGr-enabled keyboards work in vim mode</a> and a series of improvements in the Kate plugins, especially in the area of Python support and the <a href='http://kate-editor.org/2014/03/16/coming-in-4-13-improvements-in-the-build-plugin/'>build plugin</a>. There is a new, much <a href='http://kate-editor.org/2014/01/23/katekdevelop-sprint-status-bar-take-2/'>improved status bar</a> which enables direct actions like changing the indent settings, encoding and highlighting, a new tab bar in each view, code completion support for <a href='http://kate-editor.org/2014/02/20/lumen-a-code-completion-plugin-for-the-d-programming-language/'>the D programming language</a> and <a href='http://kate-editor.org/2014/02/02/katekdevelop-sprint-wrap-up/'>much more</a>. The team has <a href='http://kate-editor.org/2014/03/18/kate-whats-cool-and-what-should-be-improved/'>asked for feedback on what to improve in Kate</a> and is shifting some of its attention to a Frameworks 5 port.

## Sekalaisia uusia ominaisuuksia kaikkialle

Konsole brings some additional flexibility by allowing custom stylesheets to control tab bars. Profiles can now store desired column and row sizes. See more <a href='http://blogs.kde.org/2014/03/16/konsole-new-features-213'>here</a>.

Umbrello makes it possible to duplicate diagrams and introduces intelligent context menus which adjust their contents to the selected widgets. Undo support and visual properties have been improved as well. Gwenview <a href='http://agateau.com/2013/12/12/whats-new-in-gwenview-4.12/'>introduces RAW preview support</a>.

{{<figure src="/announcements/4/4.13.0/screenshots/marble.png" class="text-center" width="600px">}}

The sound mixer KMix introduced remote control via the DBUS inter-process communication protocol (<a href='http://kmix5.wordpress.com/2013/12/28/kmix-dbus-remote-control/'>details</a>), additions to the sound menu and a new configuration dialog (<a href='http://kmix5.wordpress.com/2013/12/23/352/'>details</a>), and a series of bug fixes and smaller improvements.

Dolphinin hakukäyttöliittymä on muutettu hyödyntämään uutta hakuinfrastruktuuria. Myös sen suorituskykyä on parannettu. Lue lisää <a href='http://freininghaus.wordpress.com/2013/12/12/a-brief-history-of-dolphins-performance-and-memory-usage'>viime vuonna tehtyjen optimointien yleiskatsauksesta</a>.

KHelpCenteriin on lisätty osioiden järjestäminen aakkosjärjestykseen, ja luokat on järjestetty uudelleen käytön helpottamiseksi.

## Pelit ja opetusohjelmat

KDE's game and educational applications have received many updates in this release. KDE's jigsaw puzzle application, Palapeli, has gained <a href='http://techbase.kde.org/Schedules/KDE4/4.13_Feature_Plan#kdegames'>nifty new features</a> that make solving large puzzles (up to 10,000 pieces) much easier for those who are up to the challenge. KNavalBattle shows enemy ship positions after the game ends so that you can see where you went wrong.

{{<figure src="/announcements/4/4.13.0/screenshots/palapeli.png" class="text-center" width="600px">}}

KDE's Educational applications have gained new features. KStars gains a scripting interface via D-BUS and can use the astrometry.net web services API to optimize memory usage. Cantor has gained syntax highlighting in its script editor and its Scilab and Python 2 backends are now supported in the editor. Educational mapping and navigation tool Marble now includes the positions of the <a href='http://kovalevskyy.tumblr.com/post/71835769570/news-from-marble-introducing-sun-and-the-moon'>Sun, Moon</a> and <a href='http://kovalevskyy.tumblr.com/post/72073986685/news-from-marble-planets'>planets</a> and enables <a href='http://ematirov.blogspot.ch/2014/01/tours-and-movie-capture-in-marble.html'>capturing movies during virtual tours</a>. Bicycle routing is improved with the addition of cyclestreets.net support. Nautical miles are now supported and clicking a <a href='http://en.wikipedia.org/wiki/Geo_URI'>Geo URI</a> will now open Marble.

#### KDE-sovellusten asentaminen

KDE-ohjelmat, mukaan lukien kaikki KDE:n kirjastot ja sovellukset, ovat ilmaisesti saatavilla avoimen lähdekoodin lisenssien mukaisesti. KDE-ohjelmia voi käyttää useilla laitekokoonpanoilla ja suoritinarkkitehtuureilla kuten ARMilla ja x86:lla, useissa käyttöjärjestelmissä, ja ne toimivat kaikenlaisten ikkunointiohjelmien ja työpöytäympäristojen kanssa. Linux- ja UNIX-pohjaisten käyttöjärjestelmien lisäksi useimmista KDE-sovelluksista on saatavilla Microsoft Windows -versiot <a href='http://windows.kde.org'>KDE-ohjelmat Windowsissa</a> -sivustolta ja Apple Mac OS X -versiot <a href='http://mac.kde.org/'>KDE-ohjelmat Macillä</a> -sivustolta. Joistain KDE-sovelluksista on verkossa kokeellisia versioita, jotka toimivat useilla mobiilialustoilla kuten Meegolla, Microsoftin Windows Mobilella ja Symbianilla, mutta niitä ei tällä hetkellä tueta. <a href='http://plasma-active.org'>Plasma Active</a> on käyttökokemus laajemmalle laitekirjolle kuten esimerkiksi tablettitietokoneille ja muille mobiililaitteille.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.13.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### Paketit

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.13.0 for some versions of their distribution, and in other cases community volunteers have done so.

##### Pakettien sijainnit

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.13.0'>Community Wiki</a>.

The complete source code for 4.13.0 may be <a href='/info/4/4.13.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.13.0 are available from the <a href='/info/4/4.13.0#binary'>4.13.0 Info Page</a>.

#### Järjestelmävaatimukset

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.

In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.
