---
aliases:
- ../announce-4.11.1
date: 2013-09-03
description: KDE levererar första betaversion av Plasma arbetsrymder, program, och
  plattform 4.11.1.
title: KDE levererar uppdateringar av Plasma arbetsrymder, program, och plattform
  under september
---
3:e september, 2013. Idag ger KDE ut uppdateringar av arbetsrymder, program och utvecklingsplattform. Dessa uppdateringar är de första av en serie månadsvisa stabilitetsuppdateringar av 4.11-serien. Som meddelades vid utgivningen, kommer arbetsrymder att fortsätta uppdateras under de kommande två åren. Den här utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 70 registrerade felrättningar omfattar förbättringar av fönsterhanteraren Kwin, filhanteraren Dolphin, med flera. Användare kan förvänta sig att Plasma-skrivbordet startar snabbare, att Dolphin panorerar jämnare, och att diverse program och verktyg använder mindre minne. Förbättringar omfattar återkomst av drag och släpp från aktivitetshanteraren till skrivbordsvisningen, rättningar av färgläggning och färger i Kate och MÅNGA småfel omhändertagna i spelet Kmahjongg. Det finns många stabilitetsrättningar och det vanliga tillägget av översättningar.

En fullständigare <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2011-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.1&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>lista</a> över ändringar finns i KDE:s felspårningsverktyg. För en detaljerad lista över ändringar som ingår i 4.11.1 kan man också bläddra i Git-loggarna.

Gå till <a href='/info/4/4.11.1'>informationssidan om 4.11.1</a> för att ladda ner källkod eller paket att installera. Om du vill ta reda på mer om 4.11-versionerna av KDE:s arbetsyta, program och utvecklingsplattform, se <a href='/announcements/4.11/'>versionsfakta för 4.11</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Det nya arbetsflödet för att skicka senare i Kontact` width="600px">}}

KDE:s programvara, inklusive alla bibliotek och program är fritt tillgängliga med licenser för öppen källkod. KDE:s programvara kan erhållas som källkod och som diverse binärformat från <a href='http://download.kde.org/stable/4.11.1/'>download.kde.org</a> eller från något av de <a href='/distributions'>större GNU/Linux- och UNIX-systemen</a> som levereras idag.
