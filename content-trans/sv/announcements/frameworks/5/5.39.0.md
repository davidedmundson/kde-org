---
aliases:
- ../../kde-frameworks-5.39.0
date: 2017-10-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Matcha bara riktiga Mime-typer, inte exempelvis "raw CD image" (fel 364884)
- Ta bort pf.path() från omgivning innan referensen strulades till av det.remove()
- Rätta protokollbeskrivning för I/O-slaven tags
- Anse att markdown-filer är dokument

### Breeze-ikoner

- Lägg till  ikon för menyöverflöde (fel 385171)

### Extra CMake-moduler

- Rätta kompilering av Python-bindningar efter 7af93dd23873d0b9cdbac192949e7e5114940aa6

### Integrering med ramverk

- Gör så att KStandardGuiItem::discard motsvarar QDialogButtonBox::Discard

### KActivitiesStats

- Ändra standardgränsen för frågor till noll
- Lägg till alternativ för att aktivera modelltestare

### KCMUtils

- Gör KCMultiDialog rullningsbar (fel 354227)

### KConfig

- Avråd från användning av KStandardShortcut::SaveOptions

### KConfigWidgets

- Avråd från användning av KStandardAction::PasteText och KPasteTextAction

### KCoreAddons

- desktoptojson: Förbättra heuristik för detektering av föråldrade tjänstetyper (bug 384037)

### KDeclarative

- Licensera om till LGPL2.1+
- Tillägg av metoden openService() i KRunProxy

### KFileMetaData

- Rätta krasch när mer än en instans av ExtractorCollection förstörs

### KGlobalAccel

- Återställ "KGlobalAccel: Konvertera till nya metoden symXModXToKeyQt i KKeyServer, för att rätta numeriska tangenter (fel 183458)" (fel 384597)

### KIconThemes

- Lägg till en metod för att nollställa egen palett
- Använd qApp-&gt;palette() när ingen egen är inställd
- Allokera riktig buffertstorlek
- Tillåt att en egen palett ställs in istället för colorSets
- Exponera stilmallens colorset

### KInit

- Windows: Rätta 'klauncher använder absolut installationssökväg vid kompileringstillfälle för att hitta kioslave.exe'

### KIO

- kioexec: Bevaka filen när dess kopiering är färdig (fel 384500)
- KFileItemDelegate: Reservera alltid utrymme för ikoner (fel 372207)

### Kirigami

- Instansiera inte temafil i BasicTheme
- Lägg till en ny framåtknapp
- Mindre kontrast för arkets rullningsbakgrund
- Tillförlitligare infoga och ta bort från menyöverflöde
- Bättre återgivning av sammanhangsikon
- Centrera åtgärdsknappen noggrannare
- Använd ikonstorlekar för åtgärdsknappar
- Bildpunktsperfekta ikonstorlekar på skrivbordet
- Markerad effekt för falsk greppikon
- Rätta färg på grepp
- Bättre färg för huvudåtgärdsknappen
- Rätta sammanhangsberoende meny för skrivbordsstil
- Bättre "mer"-meny för verktygsraden
- En riktig meny för mellanliggande sidors sammanhangsberoende meny
- Lägg till ett textfält som ska visa ett numeriskt tangentbord
- Krascha inte när startad med icke-existerande stilar
- Färgmängdskoncept i tema
- Förenkla hjulhantering (fel 384704)
- Nytt exempelprogram med QML-huvudfiler för skrivbord och mobil
- Säkerställ att currentIndex är giltig
- Skapa appstream-metadata för galleriprogrammet
- Sök efter QtGraphicalEffects så att paketerare inte glömmer det
- Inkludera inte kontroll av nedre dekoration (fel 384913)
- Ljusare färgläggning när listvy inte har något aktivt fokus
- Visst stöd för höger till vänster-layout
- Inaktivera genvägar när en åtgärd är inaktiverad
- Skapa hela insticksprogramstrukturen i byggkatalogen
- Rätta handikappstöd för galleriets huvudsida
- Om plasma inte är tillgängligt är inte heller KF5Plasma det. Bör rätta CI-felet.

### KNewStuff

- Kräv Kirigami 2.1 istället för 1.0 för KNewStuffQuick
- Skapa KPixmapSequence på ett riktigt sätt
- Klaga inte om filen knsregistry inte är tillgänglig innan den är nyttig

### Ramverket KPackage

- kpackage: Skicka med en kopia av servicetypes/kpackage-generic.desktop
- kpackagetool: Skicka med en kopia av servicetypes/kpackage-generic.desktop

### KParts

- KPartsApp-mall: Rätta installationsplats för kparts-skrivbordsfilen

### KTextEditor

- Ignorera standardmarkering i ikonkant för enstaka valbar markering
- Använd QActionGroup för val av inmatningsläge
- Rätta saknad stavningskontrollrad (fel 359682)
- Rätta reservvärdet "svärta" för Unicode-tecken &gt; 255 (fel 385336)
- Rätta åskådliggörande av efterföljande blanktecken för höger till vänsterrader

### Kwayland

- Skicka bara OutputConfig sendApplied / sendFailed till rätt resurs
- Krascha inte om en klient (lagligt) använder borttagen global kontrasthanterare
- Stöd XDG v6

### KWidgetsAddons

- KAcceleratorManager: Ange ikontext för åtgärder för att ta bort CJK-markörer (fel 377859)
- KSqueezedTextLabel: Tryck ihop text när indentering eller marginal ändras
- Använd ikonen edit-delete för destruktiv kasseringsåtgärd (fel 385158)
- Rätta fel 306944 - Använd mushjulet för att öka/minska datumen (fel 306944)
- KMessageBox: Använd frågeteckenikon för frågedialogrutor
- KSqueezedTextLabel: Respektera indentering, marginal och rambredd

### KXMLGUI

- Rätta omritningssnurra för KToolBar (fel 377859)

### Plasma ramverk

- Rätta org.kde.plasma.calendar med Qt 5.10
- [FrameSvgItem] Iterera underliggande noder på ett riktigt sätt
- [Containment Interface] Lägg inte till omgivningsåtgärder i miniprogramåtgärder på skrivbordet
- Lägg till ny komponent för nedtonade beteckningar i Item Delegates
- Rätta FrameSVGItem med programvaruåtergivning
- Animera inte IconItem i programvaruläge
- [FrameSvg] Använd ny variant av anslutning
- Möjlighet att ställa in ett anslutet färgomfång att inte ärva
- Lägg till extra visuell indikering för tangentbordsfokus av kryssruta och alternativknappar
- Återskapa inte en noll-bildpunktsavbildning
- Skicka objekt till rootObject() eftersom det nu är en singleton (fel 384776)
- Lista inte fliknamn två gånger
- Acceptera inte aktivt fokus på flik
- Registrera version 1 av QQuickItem
- [Plasma Components 3] Rätta höger till vänster i vissa grafiska komponenter
- Rätta ogiltigt id i viewitem
- Uppdatera e-postunderrättelseikon för bättre kontrast (fel 365297)

### qqc2-desktop-style

Ny modul: QtQuickControls 2 stil som använder QWidget's QStyle för att rita. Det gör det möjligt att åstadkomma en större grad av konsekvens mellan QWidget-baserade och QML-baserade program.

### Solid

- [solid/fstab] Lägg till stöd för x-gvfs stilalternativ i fstab
- [solid/fstab] Byt tillverkar- och produktegenskaper, tillåt i18n för beskrivning

### Syntaxfärgläggning

- Rätta ogiltiga itemData-referenser av 57 färgläggningsfiler
- Lägg till stöd för egna sökvägar för programspecifika syntax- och temadefinitioner
- AppArmor: Rätta D-Bus regler
- Färgläggningsindexerare: ta bort kontroller för mindre while-snurra
- ContextChecker: Stöd  '!' kontextbyte och fallthroughContext
- Färgläggningsindexerare: Kontrollera att refererade kontextnamn finns
- Ändra licens för färgläggning av gmake till MIT-licens
- Låt gmake-färgläggning vinna över Prolog för .pro-filer (fel 383349)
- Stöd "@"-makron med parenteser i clojure
- Lägg till syntaxfärgläggning för AppArmor-profiler
- Färgläggningsindexerare: Fånga ogiltiga intervall med a-Z/A-z i reguljära uttryck
- Rätta intervall med felaktiga versaler i reguljära uttryck
- Lägg till saknade referensfiler för tester. Ser riktigt ut, tycker jag.
- Lägg till stöd för Intel HEX-filer i databasen för syntaxfärgläggning
- Inaktivera stavningskontroll för strängar i Sieve-skript

### ThreadWeaver

- Rätta minnesläcka

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
