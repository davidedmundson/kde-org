---
aliases:
- ../../kde-frameworks-5.35.0
date: 2017-06-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- förbättra felunderrättelser

### BluezQt

- Skicka en explicit argumentlista. Det förhindrar att QProcess försöker hantera vår sökväg som innehåller mellanslag via ett skal.
- Rätta att egenskapsändringar missas omedelbart efter ett objekt har lagts till (fel 377405)

### Breeze-ikoner

- Uppdatera awk Mime-typ eftersom det är ett skriptspråk (fel 376211)

### Extra CMake-moduler

- Återställ dold synlighetstestning med Xcode 6.2
- ecm_qt_declare_logging_category(): Unikare inkluderingsskydd för deklarationsfil
- Lägg till eller förbättra "Genererad. Redigera inte"-meddelanden och gör dem konsekventa
- Lägg till en ny FindGperf-modul 
- Ändra standardinstallationssökväg i pkgconfig för FreeBSD

### KActivitiesStats

- Rätta kactivities-stats i lager 3

### KDE Doxygen-verktyg

- Ta inte hänsyn till nyckelord Q_REQUIRED_RESULT

### KAuth

- Verifiera att vem som än anropar oss verkligen är den han säger

### KCodecs

- Generera gperf-utdata vid byggning

### KCoreAddons

- Säkerställ att riktiga frön per tråd används i KRandom
- Bevaka inte sökvägar från QRC (fel 374075)

### KDBusAddons

- Inkludera inte pid i DBus-sökväg när flatpak används

### KDeclarative

- Skicka signalen MouseEventListener::pressed konsekvent
- Läck inte MimeData-objekt (fel 380270)

### Stöd för KDELibs 4

- Hantera att det finns mellanslag i sökvägen för CMAKE_SOURCE_DIR

### KEmoticons

- Rättning: Qt5::DBus används bara privat

### KFileMetaData

- Använd /usr/bin/env för att lokalisera python2

### KHTML

- Generera gperf-utdata för kentities vid byggning
- Generera gperf-utdata för doctypes vid byggning

### KI18n

- Utöka programmeringshandboken med anmärkningar om påverkan av setlocale()

### KIO

- Hantera ett problem där vissa element i program (t.ex. Dolphins filvisning) inte gick att komma åt i konfigurationer med flera högupplösningsskärmar (fel 363548)
- [RenameDialog] Tvinga enkelt textformat
- Identifiera PIE-binärfiler (application/x-sharedlib) som körbara filer (fel 350018)
- kärna: Exponera GETMNTINFO_USES_STATVFS i inställningshuvud
- PreviewJob: Hoppa över fjärrkataloger. För dyra att förhandsgranska (fel 208625)
- PreviewJob: Rensa bort tomma tillfälliga filer när get() misslyckas (fel 208625)
- Snabba upp detaljerad visning av trädvyer genom att undvika för många storleksändringar av kolumner

### KNewStuff

- Använd en enda QNAM (och en diskcache) för HTTP-jobb
- Intern cache för leverantörsdata vid initiering

### KNotification

- Rätta att KSNIs inte kan registrera tjänst med flatpak
- Använd programnamn istället för process-id när SNI DBus-tjänst skapas

### KPeople

- Exportera inte symboler för privata bibliotek
- Rätta export av symboler för KF5PeopleWidgets och KF5PeopleBackend
- begränsa #warning till GCC

### Ramverket KWallet

- Ersätt kwalletd4 efter överföring är klar
- Komplettering av signalnamn för överföringsmodul
- Stata bara timer för överföringsmodul om det behövs
- Kontrollera unik programinstans så tidigt som möjligt

### Kwayland

- Lägg till requestToggleKeepAbove/below
- Behåll QIcon::fromTheme i huvudtråden
- Ta bort pid changedSignal i Client::PlasmaWindow
- Lägg till pid i Plasma fönsterhanteringsprotokoll

### KWidgetsAddons

- KViewStateSerializer: Rätta krasch när vyn förstörs innan tillståndsserialisering (fel 353380)

### KWindowSystem

- Bättre rättning av NetRootInfoTestWM i sökväg med mellanslag

### KXMLGUI

- Ställ in huvudfönstret som överliggande objekt för fristående menyer
- När menyhierarkier byggs, använd deras omgivningar som överliggande objekt

### Plasma ramverk

- Lägg till VLC-ikon för systembricka
- Plasmoid-mallar: Använd bilden som är en del av paketet (igen)
- Lägg till mall för Plasma QML-miniprogram med QML-filändelse
- Skapa om plasmashellsurf vid exponera, förstör eller dölj

### Syntaxfärgläggning

- Haskell: Färglägg "julius" kvasicitering genom att använda Normal##Javascript-regler
- Haskell: Aktivera också färgläggning av hamlet för "shamlet" kvasicitering

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
