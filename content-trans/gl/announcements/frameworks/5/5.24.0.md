---
aliases:
- ../../kde-frameworks-5.24.0
date: 2016-07-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Cambios xerais

- The list of supported platforms for each framework is now more explicit. Android has been added to the list of supported platforms in all frameworks where this is the case.

### Baloo

- DocumentUrlDB::del: só aseverar que os fillos do directorio existen
- Ignorar consultas incorrectas que teñen un operador binario sen un primeiro argumento

### Iconas de Breeze

- Moitas iconas novas ou melloradas
- corrixir o fallo 364931 a icona user-idle non era visíbel (fallo 364931)
- Engadir un programa para converter ficheiros ligados simbolicamente en referencias de qrc

### Módulos adicionais de CMake

- Integrar as rutas de biblioteca relativas en APK
- Use "${BIN_INSTALL_DIR}/data" for DATAROOTDIR on Windows

### KArchive

- Ensure extracting an archive does not install files outside the extraction folder, for security reasons. Instead, extract such files to the root of the extraction folder.

### KBookmarks

- Limpar KBookmarkManagerList antes de saír de qApp para evitar bloqueos indefinidos co fío de D-Bus

### KConfig

- Marcar authorizeKAction() como obsoleto en favor de authorizeAction()
- Corrixir a reproducibilidade das construcións asegurando a codificación UTF-8

### KConfigWidgets

- KStandardAction::showStatusbar: Retirar a acción desexada

### KDeclarative

- Facer epoxy opcional

### KDED

- [OS X] converter kded5 nun axente, e construílo como unha aplicación normal

### Compatibilidade coa versión 4 de KDELibs

- Retirar a clase KDETranslator, xa non hai kdeqt.po
- Documentar o substituto de use12Clock()

### KDesignerPlugin

- Engadir compatibilidade con KNewPasswordWidget

### KDocTools

- Permitir a KDocTools atopar sempre polo menos as súas propias cousas instaladas
- Usar CMAKE_INSTALL_DATAROOTDIR en vez de «share» para buscar docbook
- Actualizar o docbook de páxina de manual de qt5options a Qt 5.4
- Actualizar o docbook de páxina de manual de kf5options

### KEmoticons

- Mover o tema Glass a kde-look

### KGlobalAccel

- Usar QGuiApplication en vez de QApplication

### KHTML

- Corrixir a aplicación do valor de herdanza para a propiedade de atallo de contorno
- Xestionar initial e inherit para o radio dos bordos
- Descartar a propiedade se capturamos unha lonxitude ou porcentaxe incorrecta como background-size
- cssText debe ter como saída os valores desas propiedades separados por comas
- Corrixir a análise de background-clip no atallo
- Facer que background-size analice a abreviatura in
- Marcar as propiedades como definidas ao repetir padróns
- Corrixir a herdanza de propiedades de fondo
- Corrixir a aplicación de Initial e Inherit para a propiedade background-size
- Despregar o ficheiro de KXMLGUI de KHTML nun ficheiro de recurso de Qt

### KI18n

- Buscar tamén as variantes sen espazos ao redor dos valores da variábel de contorno LANGUAGE nos catálogos
- Corrixir a análise de valores de variábeis de contorno de modificador de WRT e conxunto de códigos, que se facía na orde incorrecta

### KIconThemes

- Engadir a posibilidade de cargar usando un tema de iconas dun ficheiro RCC automaticamente
- Documentar o despregue de tema de iconas en MacOS e Windows, véxase https://api.kde.org/frameworks/kiconthemes/html/index.html

### KInit

- Permitir un tempo límite en reset_oom_protection mentres se espera por SIGUSR1

### KIO

- KIO: engadir SlaveBase::openPasswordDialogV2 para unha mellor comprobación de erros, os escravos de KIO deberían pasar a usalo
- Corrixir que KUrlRequester abrise un diálogo de ficheiro no directorio incorrecto (fallo 364719)
- Corrixir reinterpretacións inseguras de KDirModelDirNode*
- Engadir a opción KIO_FORK_SLAVES de CMake para definir o valor predeterminado
- Filtro de ShortUri: corrixir o filtrado de mailto:usuario@máquina
- Engadir OpenFileManagerWindowJob para salientar un ficheiro dentro dun cartafol
- KRun: engadir o método «runApplication»
- Engadir o fornecedor de busca SoundCloud
- Corrixir un problema de aliñamento co estilo «macintosh» nativo de OS X

### KItemModels

- Engadir KExtraColumnsProxyModel::removeExtraColumn, StatisticsProxyModel vaino necesitar

### KJS

- kjs/ConfigureChecks.cmake - definir HAVE_SYS_PARAM_H de maneira axeitada

### KNewStuff

- Asegurarse de que se ten un tamaño para ofrecer (fallo 364896)
- Corrixir «O diálogo de descargas falla cando faltan todas as categorías»

### KNotification

- Corrixir notificación por barra de tarefas

### KNotifyConfig

- KNotifyConfigWidget: engadir o método disableAllSounds() (fallo 157272)

### KParts

- Engadir unha opción para desactivar a xestión de KParts de títulos de xanela
- Engadir un elemento de menú de doazón no menú de axuda das nosas aplicacións

### Kross

- Corrixir o nome do enumerador de QDialogButtonBox «StandardButtons»
- Retirar o primeiro intento de cargar a biblioteca porque vamos intentar libraryPaths de todos xeitos
- Corrixir unha quebra cando un método exposto a Kross devolve un QVariant con datos que non poden cambiar de lugar
- Non usar reinterpretacións estilo C a void* (fallo 325055)

### KRunner

- [QueryMatch] Engadir iconName

### KTextEditor

- Mostrar a vista previa de texto da barra de desprazamento tras un atraso de 250 ms
- ocultar a vista previa e outras cousas ao desprazar o contido da vista
- definir o pai e a vista de ferramenta, creo que é necesario para editar a entrada do cambiador de tarefas en Win10
- Retirar «KDE-Standard» da caixa de codificación
- Activar a vista previa de cartafoles de maneira predeterminada
- Evitar o subliñado descontinuo para a vista previa e evitar corromper a caché de disposición de liñas
- Activar sempre a opción «Mostrar a vista previa do texto pregado»
- TextPreview: axustar a altura de grooveRect cando se activa scrollPastEnd
- Vista previa da barra de desprazamento: usar un rectángulo con suco se a barra de desprazamento non usa a altura completa
- Engadir KTE::MovingRange::numberOfLines() como se fixo con KTE::Range
- Vista previa de pregado de código: definir a altura da mensaxe emerxente para que as liñas agochadas encaixen
- Engadir unha opción para desactivar a vista previa do texto pregado
- Engadir unha liña de modo «folding-preview» (vista previa de pregado) de tipo booleano
- Ver ConfigInterface: permitir «folding-preview» de tipo booleano
- Engadir bool KateViewConfig::foldingPreview() e setFoldingPreview(bool)
- Funcionalidade: mostrar a vista de previa do texto ao cubrir un bloque de código pregado
- KateTextPreview: engadir setShowFoldedLines() e showFoldedLines()
- Engadir as liñas de modo «scrollbar-minimap» (bool) e «scrollbar-preview» (bool)
- Activar a barra de desprazamento minimapa de maneira predeterminada
- Nova funcionalidade: mostrar a vista previa do texto ao cubrir a barra de desprazamento
- KateUndoGroup::editEnd(): pasar KTE::Range mediante referencia constante
- Corrixir a xestión de atallos de modo vim tras os cambios de comportamento de Qt 5.5 (fallo 353332)
- Autobrace: non inserir o carácter «'» no texto
- ConfigInterface: engadir a clave de configuración scrollbar-minimap para activar ou desactivar o minimapa de desprazamento
- Corrixir KTE::View::cursorToCoordinate() cando o trebello de mensaxe superior é visíbel
- Cambios internos na barra de ordes emuladas
- Corrixir o debuxado de artefactos ao desprazar mentres as notificacións son visíbeis (fallo 363220)

### KWayland

- Engadir un evento de parent_window á interface Window de Plasma
- Xestionar correctamente a destrución dun recurso de Pointer, Keyboard ou Touch
- [servidor] Retirar código morto: KeyboardInterface::Private::sendKeymap
- [servidor] Engadir a posibilidade de definir manualmente a DataDeviceInterface da selección do portapapeis
- [servidor] Asegurarse de que Resource::Private::get devolve un nullptr se recibe un nullptr
- [servidor] Engadir unha comprobación de recursos en QtExtendedSurfaceInterface::close
- [servidor] Retirar a definición do punteiro de SurfaceInterface nos obxectos aos que se fai referencia ao destruílos
- [servidor] Corrixir unha mensaxe de erro na interface de QtSurfaceExtension
- [servidor] Introducir un sinal Resource::unbound que se emita desde o xestor de desasociación
- [servidor] Non aseverar ao destruír un BufferInterface ao que aínda se fai referencia
- Engadir unha solicitude de destrutor a org_kde_kwin_shadow e org_kde_kwin_shadow_manager

### KWidgetsAddons

- Corrixir a lectura de datos de Unihan
- Corrixir o tamaño mínimo de KNewPasswordDialog (fallo 342523)
- Corrixir un construtor considerado ambiguo por MSVC 2015
- Corrixir un problema de aliñamento co estilo «macintosh» nativo en OS X (fallo 296810)

### KXMLGUI

- KXMLGui: corrixir os índices de fusión ao retirar clientes de XMLGUI con accións en grupos (fallo 64754)
- Non avisar sobre «o ficheiro atopouse nun lugar compacto» se nin sequera se atopou
- Engadir un elemento de menú de doazón no menú de axuda das nosas aplicacións

### NetworkManagerQt

- Non definir unha etiqueta de peap en base á versión de peap
- Facer as comprobacións de versión do xestor de rede en tempo de execución (para evitar a diferenza entre tempo de compilación e de execución) (fallo 362736)

### Infraestrutura de Plasma

- [Calendario] Voltear os botóns de frecha nos idiomas de dereita a esquerda
- Plasma::Service::operationDescription() debería devolver un QVariantMap
- Non incluír contedores incrustados en containmentAt(pos) (fallo 361777)
- corrixir o tema de cores da icona de reiniciar o sistema (pantalla de acceso) (fallo 364454)
- desactivar as miniaturas da barra de tarefas con llvmpipe (fallo 363371)
- gardar contra miniaplicativos incorrectos (fallo 364281)
- PluginLoader::loadApplet: restaurar a compatibilidade con miniaplicativos instalados incorrectamente
- cartafol correcto para PLASMA_PLASMOIDS_PLUGINDIR
- PluginLoader: mellorar a mensaxe de erro sobre a compatibilidade con versións de complementos
- Corrixir a comprobación para manter QMenu na pantalla en disposicións con varias pantallas
- Covo tipo de contedor para a área de notificacións

### Solid

- Corrixir a comprobación de que a CPU sexa válida
- Ler /proc/cpuinfo de procesadores ARM
- Atopar as CPU por subsistema en vez de por controlador

### Sonnet

- Marcar o executábel do asistente como aplicación sen interface gráfica de usuario
- Permitir compilar nsspellcheck en mac de maneira predeterminada

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
