---
aliases:
- ../announce-applications-19.04.2
changelog: true
date: 2019-06-06
description: KDE Ships Applications 19.04.2.
layout: application
major_version: '19.04'
release: applications-19.04.2
title: KDE publica a versión 19.04.2 das aplicacións de KDE
version: 19.04.2
---
{{% i18n_date %}}

Today KDE released the second stability update for <a href='../19.04.0'>KDE Applications 19.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Case cincuenta correccións de erros inclúen melloras en, entre outros, Kontact, Ark, Dolphin, JuK, Kdenlive, KmPlot, Okular e Spectacle.

Entre as melloras están:

- Corrixiuse unha quebra ao ver certos documentos EPUB en Okular
- As chaves segredas volven poder exportarse desde o xector de criptografía Kleopatra
- O lembrador de eventos de KAlarm volve poder iniciarse coas últimas bibliotecas de xestión de información persoal
