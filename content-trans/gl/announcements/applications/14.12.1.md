---
aliases:
- ../announce-applications-14.12.1
changelog: true
date: '2015-01-08'
description: KDE Ships KDE Applications 14.12.1.
layout: application
title: KDE publica a versión 14.12.1 das aplicacións de KDE
version: 14.12.1
---
January 13, 2015. Today KDE released the first stability update for <a href='../14.12.0'>KDE Applications 14.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Entre as máis de 50 correccións rexistradas están melloras para a ferramenta de arquivado Ark, o modelador de UML Umbrello, o visor de documentos Okular, a aplicación para aprender pronuncias Artikulate e o cliente de escritorio remoto KRDC.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.15, KDE Development Platform 4.14.4 and the Kontact Suite 4.14.4.
