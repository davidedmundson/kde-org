---
aliases:
- ../announce-applications-18.12.2
changelog: true
date: 2019-02-07
description: KDE publie la version 18.12.2 des applications.
layout: application
major_version: '18.12'
release: applications-18.12.2
title: KDE publie la version 18.12.2 des applications de KDE.
version: 18.12.2
---
{{% i18n_date %}}

Aujourd'hui, KDE a publié la seconde mise à jour de consolidation pour les <a href='../18.12.0'>applications de KDE %[2 ]s</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traductions, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus d'une douzaine de corrections de bogues apportent des améliorations à Kontact, Ark, Konsole, Lokalize, Umbrello et bien d'autres.

Les améliorations incluses sont :

- Ark ne supprime plus les fichiers enregistrés à l'intérieur d'un afficheur intégré.</li.
- Le carnet d'adresses se souvient maintenant des anniversaires lors de la fusion des contacts.</li>
- Plusieurs mises à jour d'affichage manquant dans les diagrammes ont été corrigés dans Umbrello.</li>
