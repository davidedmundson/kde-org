---
aliases:
- ../announce-applications-17.12-rc
custom_spread_install: true
date: 2017-12-01
description: KDE publie la version candidate 17.12 des applications de KDE.
layout: application
release: applications-17.11.90
title: KDE publie la version candidate des applications de KDE en version 17.12
---
01 Décembre 2017. Aujourd'hui, KDE a publié la version candidate des nouvelles versions des applications KDE. Les dépendances et les fonctionnalités sont figées. L'attention de l'équipe KDE se porte à présent sur la correction des bogues et les dernières finitions.

Veuillez vérifier les <a href='https://community.kde.org/Applications/17.12_Release_Notes'>notes de mises à jour de la communauté</a> pour plus d'informations sur les nouveaux fichiers compressés, maintenant bâtis pour KDE et les problèmes connus. Une annonce plus complète sera disponible avec la mise à jour finale.

Les versions des applications de KDE 17.12 ont besoin de tests intensifs pour maintenir et améliorer la qualité et l'interface utilisateur. Les utilisateurs actuels sont très importants pour maintenir la grande qualité de KDE. En effet, les développeurs ne peuvent tester toutes les configurations possibles. Votre aide est nécessaire pour aider à trouver les bogues suffisamment tôt pour qu'ils puissent être corrigés avant la version finale. Veuillez contribuer à l'équipe en installant la version candidate et <a href='https://bugs.kde.org/'>en signalant tout bogue</a>.

#### Installation des paquets binaires de la version candidate des applications KDE 17.12

<em>Paquets</em>Quelques fournisseurs de systèmes d'exploitation Linux / Unix fournissent gracieusement pour certaines versions de leurs distributions, des paquets binaires pour les applications 17.12 en version candidate (en interne 17.11.90). Dans d'autres cas, des bénévoles de la communauté le font aussi. Des paquets binaires supplémentaires, et également des mises à jour des paquets actuels, seront éventuellement mis à disposition dans les prochaines semaines.

<em>Emplacements des paquets</em>. Pour obtenir une liste à jour des paquets binaires disponibles, connus par l'équipe de publication KDE, veuillez visiter le <a href='http://community.kde.org/Binary_Packages</a> Wiki de la communauté</a>.

#### Compilation de la version candidate 17.12 des applications KDE

Le code source complet de la version candidate des applications de KDE 17.12 peut être <a href='http://download.kde.org/unstable/applications/17.11.90/src/'> librement téléchargé</a>. Les instructions de compilation et d'installation sont disponibles sur la <a href='/info/applications/applications-17.11.90'>page d'informations de la version candidate des applications KDE </a>.
