---
aliases:
- ../announce-applications-18.04.2
changelog: true
date: 2018-06-07
description: KDE publie les applications de KDE en version 18.04.2
layout: application
title: KDE publie les applications de KDE en version 18.04.2
version: 18.04.2
---
07 juin 2018. Aujourd'hui, KDE a publié la seconde mise à jour de consolidation pour les <a href='../18.04.0'>applications 18.04 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction, permettant une mise à jour sûre et appréciable pour tout le monde

Plus de 25 corrections de bogues apportent des améliorations à Kontact, Cantor, Dolphin, Gwenview, KGpg, Kig, Konsole, Lokalize, Okular et bien d'autres.

Les améliorations incluses sont :

- Les opérations sur image dans Gwenview peuvent maintenant être relancées après les avoir annulées.
- KGpg ne se plante plus lors du déchiffrement de messages sans en-tête de version.
- L'exportation de feuilles de travail Cantor vers LaTeX a été corrigée pour les matrices Maxima.
