---
aliases:
- ../../kde-frameworks-5.56.0
date: 2019-03-09
layout: framework
libCount: 70
---
### Baloo

- Replace several Q_ASSERTs with proper checks
- Check string length to avoid crash for "tags:/" URL
- [tags_kio] Fix local file tagging by checking only tag: urls for double slashes
- Hardcoding the Remaining Time Update Interval
- Fix regression for matching explicitly included folders
- Nettoyage des entrées « idempotent » de la table de correspondance des types « MIME »
- [baloo/KInotify] Notify if folder was moved from unwatched place (bug 342224)
- Handle folders matching substrings of included/excluded folders correctly
- [balooctl] Normalize include/exclude paths before using it for the config
- Optimize Baloo::File copy assign operator, fix Baloo::File::load(url)
- Utiliser le contenu pour déterminer le type « MIME » (bogue 403902)
- [Extractor] Exclude GPG encrypted data from being indexed (bug 386791)
- [balooctl] Actually abort a malformed command instead of just saying so
- [balooctl] Add missing help for "config set", normalize string
- Replace recursive isDirHidden with iterative one, allow const argument
- S'assurer que seuls les dossiers sont ajoutés à l'analyseur « inotify »

### Icônes « Breeze »

- Ajout d'une icône pour « code-oss »
- [Icônes « Breeze »] Ajout d'icônes de caméra vidéo
- [breeze-icons] Use new suspend, hibernate and switch user icons in Breeze icon theme
- Ajout de versions « 16 pixels » et « 22 pixels » pour l'icône de joystick pour les périphériques
- Make Breeze theme tooltip texts consistent
- Ajout d'icônes pour la batterie
- Rename "visibility" and "hint" icons to "view-visible" and "view-hidden"
- [breeze-icons] Add monochrome/smaller SD card and memory stick icons (bug 404231)
- Ajout d'icônes de périphériques pour les drones
- Change C/C++ header/source mimetype icons to circle/line style
- Fix missing shadows on C/C++ header mimetype icons (bug 401793)
- Suppression de l'icône monochrome pour les préférences de polices
- Amélioration de l'icône de sélection de polices
- Use new bell-style icon for all users of preferences-desktop-notification (bug 404094)
- [breeze-icons] Add 16px versions of gnumeric-font.svg and link gnumeric-font.svg to font.svg
- Add preferences-system-users symlinks that point to yast-users icon
- Ajout d'une icône de non-modification

### Modules additionnels « CMake »

- Fix releaseme checkout when this is included in a sub-directory
- Nouveau module de recherche pour « Canberra »
- Update Android toolchain files to reality
- Ajout d'une vérification de compilation pour « FindEGL »

### KActivities

- Use natural sorting in ActivityModel (bug 404149)

### KArchive

- Guard KCompressionDevice::open being called with no backend available (bug 404240)

### KAuth

- Tell people they should mostly be using KF5::AuthCore
- Compile our own helper against AuthCore and not Auth
- Introduction de « KF5AuthCore »

### KBookmarks

- Replace KIconThemes dependency with equivalent QIcon usage

### KCMUtils

- Utilisation du nom « KCM » dans l'en-tête « KCM »
- Add missing ifndef KCONFIGWIDGETS_NO_KAUTH
- Adaptation aux modifications dans « kconfigwidgets »
- Sync QML module padding to reflect system setting pages

### KCodecs

- Correctif pour la vulnérabilité « CVE-2013-0779 »
- QuotedPrintableDecoder::decode: return false on error instead of asserting
- Indiquer que « KCodecs::uuencode » ne fait rien
- nsEUCKRProber/nsGB18030Prober::HandleData don't crash if aLen is 0
- nsBig5Prober::HandleData: Don't crash if aLen is 0
- KCodecs::Codec::encode: Don't assert/crash if makeEncoder returns null
- nsEUCJPProbe::HandleData: Don't crash if aLen is 0

### KConfig

- Write valid UTF8 characters without escaping (bug 403557)
- KConfig : gérer de façon correcte les liens symboliques vers des dossiers 

### KConfigWidgets

- Passer la mesure de performances si aucun fichier de thème n'est trouvé
- Ajout d'une note pour « KF6 » pour utiliser la version principale de « KF5::Auth »
- Mettre en cache la configuration par défaut de KColorScheme

### KCoreAddons

- Créer de téléphone : liens pour les numéros de téléphone

### KDeclarative

- Utilisation de « KPackage::fileUrl »pour la prise en charge des paquets « rcc KCMs »
- [GridDelegate] Fix long labels blending into each other (bug 404389)
- [GridViewKCM] improve contrast and legibility for delegates' inline hover buttons (bug 395510)
- Correct the accept flag of the event object on DragMove (bug 396011)
- Use different "None" item icon in grid view KCMs

### KDESU

- kdesud: KAboutData::setupCommandLine() already sets help &amp; version

### KDocTools

- Port cross-compilation support to KF5_HOST_TOOLING
- Only report DocBookXML as found if it was actually found
- Ajouter des entités d'Espagne

### KFileMetaData

- [Extractor] Add metadata to extractors (bug 404171)
- Ajouter un extracteur aux fichiers « AppImage »
- Nettoyage de l'extracteur « ffmpeg »
- [ExternalExtractor] Provide more helpful output when extractor fails
- Format EXIF photo flash data (bug 343273)
- Avoid side effects due to stale errno value
- Utilisation de « KFormat » pour les taux binaire et d'échantillonnage
- Ajouter des unités au taux de trame et aux données « GPS »
- Add string formatting function to property info
- Avoid leaking a QObject in ExternalExtractor
- Handle &lt;a&gt; as container element in SVG
- Check Exiv2::ValueType::typeId before converting it to rational

### KImageFormats

- ras : correction d'un plantage sur fichiers incorrects
- ras : protection aussi de la palette « QVector »
- ras : réglage de la vérification du maximum de fichiers
- xcf: Fix uninitialized memory use on broken documents
- add const, helps understand the function better
- ras: tweak max size that "fits" in a QVector
- ras: don't assert because we try to allocate a huge vector
- ras : protection contre la division par zéro
- xcf : ne pas diviser par 0.
- tga: fail gracefully if readRawData errors
- ras: fail gracefully on height*width*bpp &gt; length

### KIO

- kioexec: KAboutData::setupCommandLine() already sets help &amp; version
- Fix crash in Dolphin when dropping trashed file in trash (bug 378051)
- Middle-elide very long filenames in error strings (bug 404232)
- Ajouter la prise en charge des portails dans KRun
- [KPropertiesDialog] Fix group combobox (bug 403074)
- Properly attempt to locate the kioslave bin in $libexec AND $libexec/kf5
- Utiliser « AuthCore » au lieu de « Auth »
- Add icon name to service providers in .desktop file
- Read IKWS search provider icon from desktop file
- [PreviewJob] Also pass along that we're the thumbnailer when stat'ing file (bug 234754)

### Kirigami

- remove the broken messing with contentY in refreshabeScrollView
- add OverlayDrawer to the stuff documentable by doxygen
- Faire correspondre l'élément courant à l'affichage
- Couleur adéquate pour l'icône de flèche vers le bas
- SwipeListItem: make space for the actions when !supportsMouseEvents (bug 404755)
- ColumnView and partial C++ refactor of PageRow
- Possibilité d'utiliser au moins les contrôles 2.3 comme avec Qt 5.10
- Corrections de la hauteur des ascenseurs horizontaux
- Improve ToolTip in the ActionTextField component
- Ajout d'un composant « ActionTextField »
- Corrections de l'espacement des boutons (bogue 404716)
- Correction sur la taille des boutons (bogue 404715)
- GlobalDrawerActionItem: properly reference icon by using group property
- show separator if header toolbar is invisible
- Ajout d'un fond d'écran de page par défaut
- DelegateRecycler: Fix translation using the wrong domain
- Correction de l'avertissement lors de l'utilisation de « QQuickAction »
- Supprimer certaines constructions non nécessaires de « QString »
- Don't show the tooltip when the drop-down menu is shown (bug 404371)
- Ne pas afficher les ombres quand fermées
- Ajout des propriétés nécessaires pour la couleur alternative
- revert most of the icon coloring heuristics change
- Gestion correcte des propriétés groupées
- [PassiveNotification] Don't start timer until window has focus (bug 403809)
- [SwipeListItem] Use a real toolbutton to improve usability (bug 403641)
- support for optional alternating backgrounds (bug 395607)
- N'afficher les poignées que lorsqu'elles sont des actions visibles
- Prise en charge des icônes colorées pour les boutons d'actions
- Toujours afficher le bouton de retour sur les calques
- Mise à jour de la documentation « SwipeListItem » vers « QQC2 »
- Correction de la logique de « updateVisiblePAges »
- Affichage des pages visibles dans « pagerow »
- hide breadcrumb when the current page has a toolbar
- Prise en charge de la surcharge de page de styles pour les barres d'outils
- new property in page: titleDelegate to override the title in toolbars

### KItemModels

- KRearrangeColumnsProxyModel: make the two column-mapping methods public

### KNewStuff

- Ne pas prendre en compte les contenus non valables dans les listes
- Correction d'une fuite mémoire trouvée par « asan »

### KNotification

- Portage de « findcanberra » vers « ECM »
- Ajouter Android dans la liste des systèmes officiellement pris en charge

### Environnement de développement « KPackage »

- Supprimer l'alarme pour l'obsolescence de « kpackage_install_package »

### KParts

- templates: KAboutData::setupCommandLine() already sets help &amp; version

### Kross

- Install Kross modules to ${KDE_INSTALL_QTPLUGINDIR}

### KService

- kbuildsycoca5: no need to repeat work of KAboutData::setupCommandLine()

### KTextEditor

- Tenter d'améliorer la hauteur de dessin pour les lignes de texte - bogue 403868. Éviter de couper « _ » et d'autres parties encore coupées : choses en double hauteur comme le mélange anglais / arabe. Voir le bogue 404713.
- Use QTextFormat::TextUnderlineStyle instead of QTextFormat::FontUnderline (bug 399278)
- Rend possible l'affichage de tous les espaces dans le document (bogue 342811)
- Ne pas imprimer les lignes d'indentation
- KateSearchBar: Show also search has wrapped hint in nextMatchForSelection() aka Ctrl-H
- katetextbuffer: refactor TextBuffer::save() to better separate code paths
- Utiliser « AuthCore » au lieu de « Auth »
- Refactor KateViewInternal::mouseDoubleClickEvent(QMouseEvent *e)
- Améliorations du complètement
- Set the color scheme to Printing for Print Preview (bug 391678)

### KWayland

- Only commit XdgOutput::done if changed (bug 400987)
- FakeInput: add support for pointer move with absolute coordinates
- Add missing XdgShellPopup::ackConfigure
- [server] Add surface data proxy mechanism
- [server] Ajout d'un signal « selectionChanged »

### KWidgetsAddons

- Utilisation de l'icône « Non »correcte pour « KStandardGuiItem »

### Environnement de développement de Plasma

- [Icon Item] Block next animation also based on window visibility
- Afficher un avertissement si un module externe nécessite une nouvelle version
- Bump the theme versions because icons changed, to invalidate old caches
- [breeze-icons] Remaniement de « system.svgz »
- Make Breeze theme tooltip texts consistent
- Change glowbar.svgz to smoother style (bug 391343)
- Do background contrast fallback at runtime (bug 401142)
- [breeze desktop theme/dialogs] Add rounded corners to dialogs

### Motif

- pastebin: don't show progress notifications (bug 404253)
- sharetool : affichage des « URL » partagées au dessus
- Fix sharing files with spaces or quotes in names via Telegram
- Have ShareFileItemAction provide an output or an error if they are provided (bug 397567)
- Activer le partage de liens « URL » par courriel

### QQC2StyleBridge

- Use PointingHand when hovering links in Label
- Respecter la propriété d'affichage des boutons
- clicking on empty areas behaves like pgup/pgdown (bug 402578)
- Prise en charge d'une icône dans les listes déroulantes
- prendre en charge une « api » de positionnement de texte
- Prendre en charge les icônes à partir de fichiers locaux dans les boutons
- Use the correct cursor when hovering over the editable part of a spinbox

### Opaque

- Bring FindUDev.cmake up to ECM standards

### Sonnet

- Handle the case if createSpeller is passed an unavailable language

### Coloration syntaxique

- Correction de l'alarme de suppression de dépôts
- MustacheJS: also highlight template files, fix syntax and improve support for Handlebars
- Rendre les contextes inutilisés fatals pour l'indexeur
- Update example.rmd.fold and test.markdown.fold with new numbers
- Installation de l'en-tête pour « DefinitionDownloader »
- Mise à jour de « octave.xml  » vers Octave 4.2.0
- Improve highlighting of TypeScript (and React) and add more tests for PHP
- Ajouter plus de coloration syntaxiques pour les langages de bloc avec marqueurs
- Retour des définitions triées pour les noms de fichiers et les types « MIME »
- Ajout d'une mise à jour pour les références manquantes
- BrightScript: Unary and hex numbers, @attribute
- Avoid duplicate *-php.xml files in "data/CMakeLists.txt"
- Add functions returning all definitions for a mimetype or file name
- Mise à jour du type alphabétique « MIME » « haskell »
- Prévention de l'assertion dans le chargement « regex »
- cmake.xml : mises à jour vers la version 3.14
- CubeScript: fixes line continuation escape in strings
- Ajout de quelques tutoriels minimaux pour l'ajout de tests
- R Markdown : amélioration du repliage de blocs
- HTML: highlight JSX, TypeScript &amp; MustacheJS code in the &lt;script&gt; tag (bug 369562)
- AsciiDoc : ajout du repliement pour les sections
- Coloration syntaxique pour le thème « FlatBuffers »
- Ajoute de quelques constantes et fonctions pour Maxima

### Informations sur la sécurité

Le code publié a été signé en « GPG » avec la clé suivante  pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empreinte de la clé primaire : 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
