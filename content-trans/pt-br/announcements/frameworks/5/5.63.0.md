---
aliases:
- ../../kde-frameworks-5.63.0
date: 2019-10-14
layout: framework
libCount: 70
---
### Ícones Breeze

- Melhoria do ícone do KFloppy (erro 412404)
- Adição dos ícones de acções 'format-text-underline-squiggle' (erro 408283)
- Adição de ícone colorido 'preferences-desktop-filter (erro 406900)
- Adição de ícone da aplicação de controlo de drones Kirogi
- Adição de programas para criar uma 'webfont' (tipo de letra da Web) a partir de todos os ícones de acções do Brisa
- Adição do ícone 'enablefont' e 'disablefont' para o KCM do KFontInst
- Correcção dos ícones 'system-reboot' enormes que rodam numa direcção inconsistente (erro 411671)

### Módulos extra do CMake

- Novo módulo ECMSourceVersionControl
- Correcção do FindEGL ao usar o Emscripten
- ECMAddQch: adição do argumento INCLUDE_DIRS

### Integração do Framework

- garantia de que o winId() não é invocado em elementos não-nativos (erro 412675)

### kcalendarcore

Novo módulo, anteriormente conhecido como 'kcalcore' no 'kdepim'

### KCMUtils

- Supressão dos eventos do rato nos KCM's que provoca movimentos das janelas
- ajuste das margens do KCMultiDialog (erro 411161)

### KCompletion

- [KComboBox] Desactivação adequada da completação incorporada do Qt [correcção de regressão]

### KConfig

- Correcção da geração de propriedades que começam com uma letra maiúscula

### KConfigWidgets

- Mudança do KColorScheme para ser compatível com o QVariant

### kcontacts

Novo módulo, anteriormente parte do KDE PIM

### KCoreAddons

- Adição do KListOpenFilesJob

### KDeclarative

- Remoção do contexto do QQmlObjectSharedEngine em sincronização com o QQmlObject
- [KDeclarative] Migração do método obsoleto QWheelEvent::delta() para o angleDelta()

### KDELibs 4 Support

- Suporte para o NetworkManager 1.20 e compilação efectiva da infra-estrutura do NM

### KIconThemes

- Descontinuação dos métodos globais [Small|Desktop|Bar]Icon()

### KImageFormats

- Adição de ficheiros para testar o erro 411327
- xcf: Correcção de regressão ao ler ficheiros com propriedades "não suportadas"
- xcf: Leitura adequada da resolução da imagem
- Migração do carregamento de imagens HDR (Radiance RGBE) para o Qt5

### KIO

- [Painel de Locais] Remodelação da secção Gravados Recentemente
- [DataProtocol] compilação sem a conversão implícita de ASCII
- Considerar o uso dos métodos de WebDAV suficientes para assumir WebDAV
- O REPORT também suporta o cabeçalho Depth
- Tornar a conversão do QSslError::SslError &lt;-&gt; KSslError::Error reutilizável
- Descontinuação do construtor KSslError::Error do KSslError
- [Windows] correcção da listagem da pasta-mãe do C:\xpto, que é C:\ e não C:
- Correcção de estoiro à saída no kio_file (erro 408797)
- Adição dos operadores == e != no KIO::UDSEntry
- Substituição do KSslError::errorString pelo QSslError::errorString
- Tarefa de movimentação/cópia: ignorar o 'stat' das origens se a pasta de destino não tiver permissões de escrita (erro 141564)
- Correcção da interacção com executáveis do DOS/Windows no KRun::runUrl
- [KUrlNavigatorPlacesSelector] Identificação adequada da acção 'teardown' (erro 403454)
- KCoreDirLister: correcção de estoiro ao criar pastas novas a partir do KFileWidget (erro 401916)
- [kpropertiesdialog] adição de ícones para a secção do tamanho
- Adição de ícones para os menus "Abrir Com" e "Acções"
- Evitar inicializar uma variável desnecessária
- Passagem de mais funcionalidades do KRun::runCommand/runApplication para o KProcessRunner
- [Permissões Avançadas] Correcção dos nomes dos ícones (erro 411915)
- [KUrlNavigatorButton] Correcção da uso do [] fora dos limites
- Fazer com que o KSslError guarde internamente um QSslError
- Divisão do KSslErrorUiData no KTcpSocket
- Migração do kpac do QtScript

### Kirigami

- fazer sempre 'cache' apenas do último item
- mais 'z' (erro 411832)
- correcção da versão da importação no PagePoolAction
- O PagePool é do Kirigami 2.11
- term em conta a velocidade de arrastamento quando terminar uma torção
- Correcção da cópia dos URL's para a área de transferência
- melhor verificação se se está a fazer uma atribuição de item-pai a um Item real
- suporte básico para as acções do ListItem
- introdução do 'cachePages'
- correcção da compatibilidade com o Qt5.11
- introdução do PagePoolAction
- nova classe: PagePool para gerir a reciclagem das páginas após serem retiradas da pilha
- melhoria da aparência das barras de páginas
- alguma margem à direita (erro 409630)
- Reversão do erro "Compensar os tamanhos enores dos ícones em dispositivos móveis no ActionButton"
- não fazer com que os itens da lista pareçam inactivos (erro 408191)
- Reversão da alteração "Remover a escala da unidade de tamanhos dos ícones para o isMobile"
- O Layout.fillWidth deverá ser feito pelo cliente (erro 411188)
- Adição de um modelo para o desenvolvimento de aplicações do Kirigami
- Adição de um modo para centrar as acções e omitir o título ao usar um estilo ToolBar (erro 402948)
- Compensar os tamanhos enores dos ícones em dispositivos móveis no ActionButton
- Correcção de alguns erros em execução sobre propriedades indefinidas
- Correcção da cor de fundo do ListSectionHeader para alguns esquemas de cores
- Remoção do item de conteúdo personalizado do separador ActionMenu

### KItemViews

- [KItemViews] Migração para a API não-obsoleta do QWheelEvent

### KJobWidgets

- limpeza dos objectos relacionados com o DBus de forma suficientemente antecipada para evitar um estoiro ao sair do programa

### KJS

- Adição das funções do String startsWith(), endsWith() e includes() de JS
- Correcção do Date.prototype.toJSON() chamado com objectos não-Date

### KNewStuff

- Mudança do KNewStuffQuick para ficar a par de funcionalidades com o KNewStuff(Widgets)

### KPeople

- Adopção do Android como uma plataforma suportada
- Instalação do avatar predefinido com o 'qrc'
- Agrupamento dos ficheiros de 'plugins' no Android
- Desactivação de partes do DBus no Android
- Correcção de um estoiro ao monitorizar um contacto que é removido no PersonData (erro 410746)
- Uso de tipos completamente qualificados nos sinais

### KRunner

- Consideração das localizações UNC como contexto do NetworkShare

### KService

- Passagem da 'Amusement' (Diversão) para a pasta 'Games' em vez de Games &gt; Toys (erro 412553)
- [KService] Adição de construtor por cópia
- [KService] adição do workingDirectory(), descontinuação do path()

### KTextEditor

- tentar evitar problemas na antevisão do texto
- Expansão variável: Uso interno do std::function
- O QRectF em vez do QRect resolve problemas de recorte (erro 390451)
- o problema de desenho seguinte desaparece se ajustar um pouco o rectângulo de recorte (erro 390451)
- evitar a magia de escolha dos tipos de letra e activação da suavização (erro 390451)
- KadeModeMenuList: correcção de fugas de memória e outros erros
- tentar a pesquisa por tipos de letra possíveis, funciona razoavelmente bem se não usa factores de escala estranhos como por exemplo 1.1
- Menu do modo da barra de estado: Reutilização do QIcon vazio que está implicitamente partilhado
- Exposição do KTextEditor::MainWindow::showPluginConfigPage()
- Substituição do QSignalMapper por uma função lambda
- KateModeMenuList: uso do QString() para textos vazios
- KateModeMenuList: adição da secção "Melhores Ocorrências de Pesquisa" e correcções para o Windows
- Expansão variável Suporte para QTextEdits
- Adição de atalho do teclado para mudança dos modos de introdução de dados para o menu de edição (erro 400486)
- Janela da expansão variável: tratamento adequado das mudanças de selecção e activação dos itens
- Janela de expansão variável: adição do campo de texto do filtro
- Cópia de segurança ao gravar: Suporte para as substituições do texto da data e hora (erro 403583)
- Expansão variável: Preferir o valor de retorno sobre o argumento de retorno
- Início preliminar da janela de variáveis
- uso do novo formato da API

### KWallet Framework

- Suporte para HiDPI

### KWayland

- Ordenação alfabética dos ficheiros na lista do CMake

### KWidgetsAddons

- Tornar o botão OK configurável no KMessageBox::sorry/detailedSorry
- [KCollapsibleGroupBox] Correcção de aviso do QTimeLine::start durante a execução
- Melhoria da nomenclatura dos métodos de ícones do KTitleWidget
- Adição de métodos de alteração com QIcon para as janelas de senhas
- [KWidgetAddons] migração para a API não-obsoleta do Qt

### KWindowSystem

- Mudança do XCB para obrigatório se compilar a infra-estrutura do X
- Uso cada vez menor da opção enumerada obsoleta NET::StaysOnTop

### KXMLGUI

- Passagem do item "Modo de Ecrã Completo" do menu Configuração para o menu Ver (erro 106807)

### NetworkManagerQt

- ActiveConnection: ligação do sinal 'stateChanged()' à interface correcta

### Plasma Framework

- Exportação da categoria de registo básica do Plasma, adição de uma categoria para um qWarning
- [pluginloader] Uso de registos com categorias
- tornar o 'editMode' uma propriedade global do 'corona'
- Respeito do factor de velocidade global da animação
- instalação adequada de todo o 'plasmacomponent3'
- [Dialog] Aplicação do tipo de janela após a mudança de opções
- Mudança da lógica de revelação do botão da senha
- Correcção de estoiro na finalização do ConfigLoader do Applet (erro 411221)

### QQC2StyleBridge

- Correcção de diversos erros do sistema de compilação
- leitura das margens a partir do qstyle
- [Tab] Correcção do dimensionamento (erro 409390)

### Realce de sintaxe

- Adição do realce de sintaxe para o RenPy (.rpy) (erro 381547)
- Regra WordDetect: detecção dos separadores no extremo interno do texto
- Realce dos ficheiros GeoJSON como se fossem JSON normal
- Adição do realce de sintaxe para as legendas de texto do SubRip (SRT)
- Correcção do 'skipOffse' com uma RegExpr dinâmica (erro 399388)
- bitbake: tratamento da consola e do Python incorporados
- Jam: correcção do identificador numa SubRule
- Adição da definição de sintaxe para o Perl6 (erro 392468)
- suporte da extensão .inl para o C++, não usada pelos outros ficheiros XML de momento (erro 411921)
- suporte do *.rej para o realce de diferenças (erro 411857)

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
