---
aliases:
- ../../kde-frameworks-5.16.0
date: 2015-11-13
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Biblioteca do monitor: Uso do Kformat::spelloutDuration para formatar o texto de data/hora
- Uso do KDE_INSTALL_DBUSINTERFACEDIR para instalar as interfaces D-Bus
- UnindexedFileIndexer: Tratamento dos arquivos que foram movidos quando o <i>baloo_file</i> não estava em execução
- Remoção do <i>Transaction::renameFilePath</i> e adição de uma <i>DocumentOperation</i>para ele.
- Tornar explícitos os construtores com um único parâmetro
- UnindexedFileIndexer: só indexar as partes necessárias do arquivo
- Transaction: adição de método para devolver uma estrutura <i>timeInfo</i>
- Adição da exclusão de tipos MIME na configuração do <i>balooctl</i>
- Databases: Uso do QByteArray::fromRawData ao passar os dados para um codec
- Balooctl: O comando 'status' foi movido para sua própria classe
- Balooctl: Apresentação do menu de ajuda se o comando não for reconhecido
- Balooshow: Permissão para pesquisa de arquivos pelo seu inode + ID de dispositivo
- Monitor do Balooctl: parar se o Baloo for finalizado
- MonitorCommand: Uso dos sinais <i>started</i> e <i>finished</i>
- Monitor do Balooctl: Movido para uma classe de comando adequada
- Adição de notificação do D-Bus quando ao iniciar/terminar a indexação de um arquivo
- FileIndexScheduler: Interromper as tarefas à força na saída
- WriteTransaction: Evitar a leitura da <i>positionList</i> a menos que seja necessário
- WriteTransaction: Verificações extra no <i>replaceDocument</i>

### BluezQt

- O <i>isBluetoothOperational</i> agora também depende do <i>rfkill</i> desbloqueado
- Correção da análise do estado global do botão <i>rfkill</i>
- API QML: Marcação das propriedades sem sinal de notificação como constantes

### Módulos extra do CMake

- Aviso em vez de erro se o <i>ecm_install_icons</i> não encontrar ícones. (erro <a href='https://bugs.kde.org/show_bug.cgi?id=354610'>354610</a>)
- Possibilidade de compilação do KDE Frameworks 5 com um Qt 5.5.x simples e instalado a partir do instalador normal do qt.io no Mac OS
- Não limpar as variáveis de cache no KDEInstallDirs (erro <a href='https://bugs.kde.org/show_bug.cgi?id=342717'>342717</a>)

### Integração do Framework

- Definição do valor padrão para <i>WheelScrollLines</i>
- Correção da configuração do <i>WheelScrollLines</i> com Qt &gt;= 5.5 (erro <a href='https://bugs.kde.org/show_bug.cgi?id=291144'>291144</a>)
- Troca da fonte para a Noto no Plasma 5.5

### KActivities

- Correção da compilação no Qt 5.3
- A inclusão <i>boost.optional</i> foi movida para o local que a usa
- Substituição do uso do <i>boost.optional</i> nas continuações com uma estrutura <i>optional_view</i> mais reduzida
- Adição do suporte para uma ordenação personalizada dos resultados ligados
- Permissão para que o QML invoque o KCM das atividades
- Adição do suporte para a exclusão de atividades no KCM de atividades
- Nova interface de configuração da atividade
- Nova interface de configuração com suporte a adição de uma descrição e um papel de parede
- A interface de configuração está agora devidamente modularizada

### KArchive

- Correção do KArchive para a alteração de comportamento no Qt 5.6
- Correção de vazamentos de memória, menos uso de memória

### KAuth

- Tratamento das mensagens <i>qInfo</i> indiretas
- Espera pelo fim do utilitário da chamada assíncrona antes de verificar a resposta (erro <a href='https://bugs.kde.org/show_bug.cgi?id=345234'>345234</a>)
- Correção do nome da variável, caso contrário, não existe forma da inclusão funcionar

### KConfig

- Correção do uso do <i>ecm_create_qm_loader</i>
- Correção da variável de <i>include</i>
- Uso da variante KDE*INSTALL*FULL*, de forma a não existir ambiguidade
- Permissão do KConfig para usar alguns recursos, como os arquivos de configuração de contingência

### KConfigWidgets

- Tornar o KConfigWidgets independente, fornecendo um arquivo global em um recurso
- Tornar o <i>doctools</i> opcional

### KCoreAddons

- KAboutData: correção do "is is" -&gt; "is" addCredit(): ocsUserName -&gt; ocsUsername
- O KJob::kill(Quiet) também deverá sair do ciclo de eventos
- Adição do suporte para o nome do arquivo <i>desktop</i> no KAboutData
- Uso dos caracteres de escape corretos
- Redução de algumas alocações
- Simplificação do KAboutData::translators/setTranslators
- Correção do código de exemplo do <i>setTranslator</i>
- desktopparser: correção da chave Encoding=
- desktopfileparser: Comentários de revisão do endereço
- Permitir a definição de tipos de serviços no kcoreaddons_desktop_to_json()
- desktopparser: Correção do processamento de valores de precisão dupla e booleana
- Adição do KPluginMetaData::fromDesktopFile()
- desktopparser: Permissão para que arquivos do tipo "service" usem caminhos relativos
- desktopparser: Uso de registros de eventos com mais categorias
- O QCommandLineParser usa o '-v' para '--version', então use apenas '--verbose'
- Remoção de vários códigos duplicado no desktop{tojson,fileparser}.cpp
- Processamento dos arquivos <i>ServiceType</i> ao ler arquivos <i>.desktop</i>
- Tornar o <i>SharedMimeInfo</i> um requisito opcional
- Remoção de chamada ao QString::squeeze()
- desktopparser: Evitar a decodificação desnecessária de UTF-8
- desktopparser: Não adicionar outro elemento se o mesmo terminar com um separador
- KPluginMetaData: Aviso quando um elemento da lista não é uma lista em JSON
- Adição do mimeTypes() ao KPluginMetaData

### KCrash

- Melhoria na pesquisa do <b>drkonqui</b> e mantê-lo silencioso por padrão, caso não seja encontrado

### KDeclarative

- O ConfigPropertyMap pode agora ser pesquisado por opções de configuração imutáveis, usando o método isImmutable(chave)
- Retirar o QJSValue no mapa de propriedades de configuração
- EventGenerator: Adição do suporte para o envio de eventos da roda do mouse
- Correção do <i>initialSize</i> do QuickViewSharedEngine perdido na inicialização
- Correção de uma regressão crítica do QuickViewSharedEngine pelo commit (<a href='https://quickgit.kde.org/?p=kdeclarative.git&amp;a=commit&amp;h=3792923639b1c480fd622f7d4d31f6f888c925b9'>3792923639b1c480fd622f7d4d31f6f888c925b9</a>)
- Tornar o tamanho da janela pré-determinado precedente em relação ao tamanho inicial do objeto no QuickViewSharedEngine

### KDED

- Tornar o <i>doctools</i> opcional

### KDELibs 4 Support

- Não tentar armazenar um <i>QDateTime</i> na memória mapeada com <i>mmap</i>
- Sincronização e adoção do <i>uriencode.cmake</i> a partir do <b>kdoctools</b>

### KDesignerPlugin

- Adição do KCollapsibleGroupBox

### KDocTools

- Atualização das <i>entities</i> pt_BR

### KGlobalAccel

- Não efetuar um XOR do Shift para o KP_Enter (erro <a href='https://bugs.kde.org/show_bug.cgi?id=128982'>128982</a>)
- Captura de todas as teclas de um símbolo (erro <a href='https://bugs.kde.org/show_bug.cgi?id=351198'>351198</a>)
- Não obter duas vezes os <i>keysyms</i> para cada tecla pressionada

### KHTML

- Correção da impressão no KHTMLPart, definindo corretamente o item-pai do <i>printSetting</i>

### KIconThemes

- O <i>kiconthemes</i> agora tem suporte a temas incorporados nos recursos do Qt dentro do prefixo <b>:/icons</b>, da mesma forma como o Qt faz ele próprio para o QIcon::fromTheme
- Adição de dependências obrigatórias que faltavam

### KImageFormats

- Reconhecimento do tipo <i>image/vnd.adobe.photoshop</i> em vez do <i>image/x-psd</i>
- Reversão parcial do (<a href='https://quickgit.kde.org/?p=kimageformats.git&amp;a=commit&amp;h=d7f457a'>d7f457a</a>) para evitar uma falha ao sair do aplicativo

### KInit

- Tornar o <i>doctools</i> opcional

### KIO

- Gravação da URL do <i>proxy</i> com o esquema correto
- Emissão dos "modelos de novos arquivos" na biblioteca <i>kiofilewidgets</i>, usando um <i>.qrc</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=353642'>353642</a>)
- Tratamento adequado do botão do meio do mouse no menu de navegação
- Tornar o <i>kio_http_cache_cleaner</i> instalável através dos instaladores/pacotes de aplicativos
- KOpenWithDialog: Correção da criação de arquivos <i>desktop</i> com tipo MIME vazio
- Leitura da informação do protocolo a partir dos metadados do plugin
- Permissão da instalação de um <i>kioslave</i> local
- Adição de um <i>.protocol</i> convertido para JSON
- Correção da dupla emissão do resultado e do aviso ausente quando a listagem acessar uma pasta inacessível (erro <a href='https://bugs.kde.org/show_bug.cgi?id=333436'>333436</a>)
- Preservação dos destinos dos links relativos ao copiar links simbólicos (erro <a href='https://bugs.kde.org/show_bug.cgi?id=352927'>352927</a>)
- Uso de ícones adequados para as pastas padrão da pasta do usuário (erro <a href='https://bugs.kde.org/show_bug.cgi?id=352498'>352498</a>)
- Adição de uma interface que permite ao plugin mostrar ícones sobrepostos personalizados
- Mudança da dependência do KIO no KNotifications (kpac) para opcional
- Mudança do <i>doctools</i> e <i>wallet</i> para opcional
- Proteção contra falhas do KIO se o servidor D-Bus não estiver em execução
- Adição do KUriFilterSearchProviderActions para mostrar uma lista de ações quando se procura por algum texto usando os atalhos da Web
- Os elementos do menu "Criar novo" da pasta 'kde-baseapps/lib/konq' foi movido para o <i>kio</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=349654'>349654</a>)
- O konqpopupmenuplugin.desktop do kde-baseapps foi movido para o <i>kio</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=350769'>350769</a>)

### KJS

- Usar a variável global "_timezone" no MSVC em vez da "timezone". Corrige a compilação com o MSVC 2015

### KNewStuff

- Correção do arquivo .desktop do 'Gerenciador de Partições do KDE' e a URL do site

### KNotification

- Agora que o <b>KParts</b> não precisa mais do KNotifications, apenas as coisas que precisam mesmo de notificações estão realmente dependentes deste framework
- Adição de descrição + objetivo para a fala + Phonon
- Mudança da dependência do Phonon para opcional - alteração puramente interna, como é feito na fala

### KParts

- Usar o <i>deleteLater</i> no Part::slotWidgetDestroyed()
- Remoção da dependência do KParts no KNotifications
- Usar uma função para consultar a localização do arquivo 'ui_standards.rc' em vez de incluir uma localização fixa no código, permitindo que funcione com uma contingência de recursos

### KRunner

- RunnerManager: Simplificação do código de carregamento de plugins

### KService

- KBuildSycoca: salvar sempre, mesmo que não tenham sido detectadas alterações no arquivo <i>.desktop</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=353203'>353203</a>)
- Tornar o <i>doctools</i> opcional
- Kbuildsycoca: processamento de todos os arquivos <i>mimeapps.list</i> mencionados na nova especificação
- Uso da data mais recente na subpasta como data da pasta de recursos
- Manutenção dos tipos MIME em separado ao converter o KPluginInfo no KPluginMetaData

### KTextEditor

- Realce: gnuplot: adição da extensão .plt
- Correção da sugestão de validação, graças a "Thomas Jarosch" &lt;thomas.jarosch@intra2net.com&gt; e também a adição de sugestão sobre a validação do tempo de compilação
- Não finaliza inesperadamente quando o comando não está disponível
- Correção do erro <a href='https://bugs.kde.org/show_bug.cgi?id=307107'>307107</a>
- Realce da variáveis de Haskell que começam com _
- Simplificação da inicialização do git2, uma vez que é necessária uma versão suficientemente recente (erro <a href='https://bugs.kde.org/show_bug.cgi?id=353947'>353947</a>)
- Configurações padrão do pacote no recurso
- Realce de sintaxe (d-g): uso dos estilos padrão em vez de cores fixas no código
- Melhor pesquisa de scripts: primeiro nos locais do usuário; depois nos nossos recursos; e depois em todos os outros scripts. Dessa forma, o usuário poderá sobrescrever os scripts enviados por nós com versões locais
- Empacotamento de tudo que estiver em JS nos recursos. Assim, apenas 3 arquivos configuração ausentes e o <i>ktexteditor</i> também poderiam passar a ser usados como biblioteca sem quaisquer arquivos empacotados
- Próxima tentativa: colocação de todos os arquivos de sintaxe em XML num recurso
- Adição de um atalho de mudança de modo de introdução de dados (erro <a href='https://bugs.kde.org/show_bug.cgi?id=347769'>347769</a>)
- Colocação dos arquivos XML em um recurso
- Realce de sintaxe (a-c): migração para novos estilos padrão, remoção de cores fixas no código
- Realce de sintaxe: remoção de cores fixas no código e uso dos estilos padrão como alternativa
- Realce de sintaxe: usar os novos estilos padrão (remoção de cores fixas)
- Melhor estilo padrão de "Importação"
- Introdução da opção "Salvar com a codificação" para salvar um arquivo com uma codificação diferente, usando o menu de codificação devidamente agrupado e substituindo todas as caixas de diálogo de gravação com as corretas do sistema operacional, sem perder esta importante funcionalidade.
- Colocação do arquivo UI em uma biblioteca, usando a extensão do <i>xmlgui</i>
- A impressão volta a respeitar o esquema de fontes e cores (erro <a href='https://bugs.kde.org/show_bug.cgi?id=344976'>344976</a>)
- Uso das cores do Breeze para as linhas salvas e modificadas
- Melhoria das cores padrão das bordas dos ícones do esquema "Normal"
- Autobrace: Só inserir parênteses quando a próxima letra for vazia ou não-alfanumérica
- Autobrace: Se remover o parênteses inicial com o Backspace, remover o final também
- Autobrace: Só estabelecer a conexão uma vez
- Autobrace: Remoção dos parênteses de fechamento em algumas circunstâncias
- Correção da substituição de atalhos não ser propagada para a janela principal
- Erro 342659 - A cor de "realce de colchetes" é imperceptível (correção do esquema Normal) (erro <a href='https://bugs.kde.org/show_bug.cgi?id=342659'>342659</a>)
- Adição de cores padrão adequadas para a cor "Número da linha atual"
- Correspondência de colchetes e colchetes automáticos: compartilhamento de código
- Correspondência de colchetes: proteção contra <i>maxLines</i> negativos
- Correspondência de colchetes: só porque o novo intervalo corresponde ao antigo, não significa que não é necessária a atualização
- Adição da largura de meio espaço para permitir a pintura do cursor no fim da linha
- Correção de alguns problemas com o HiDPI no contorno dos ícones
- Correção do erro #310712: remoção dos espaços finais também na linha com o cursor (erro <a href='https://bugs.kde.org/show_bug.cgi?id=310712'>310712</a>)
- Só mostrar a mensagem "marcação definida" quando o modo de entrada do VI estiver ativo
- Remoção de &amp; do texto do botão (erro <a href='https://bugs.kde.org/show_bug.cgi?id=345937'>345937</a>)
- Correção da atualização da cor do número da linha atual (erro <a href='https://bugs.kde.org/show_bug.cgi?id=340363'>340363</a>)
- Implementação da inserção de colchetes ao escrever um colchete sobre uma seleção (erro <a href='https://bugs.kde.org/show_bug.cgi?id=350317'>350317</a>)
- Parênteses automáticos (erro <a href='https://bugs.kde.org/show_bug.cgi?id=350317'>350317</a>)
- Correção de alerta no realce de sintaxe (erro <a href='https://bugs.kde.org/show_bug.cgi?id=344442'>344442</a>)
- Não deslocar a coluna com a mudança de linha dinâmica ligada
- Lembrar se o realce foi configurado pelo usuário nas sessões para não perdê-lo ao salvar após a restauração (erro <a href='https://bugs.kde.org/show_bug.cgi?id=332605'>332605</a>)
- Correção da dobragem no TeX (erro <a href='https://bugs.kde.org/show_bug.cgi?id=328348'>328348</a>)
- Correção do erro #327842: A detecção do fim do comentário com o estilo do C não funcionava (erro <a href='https://bugs.kde.org/show_bug.cgi?id=327842'>327842</a>)
- Gravação/restauração da mudança de linha dinâmica na gravação/reposição da sessão (erro <a href='https://bugs.kde.org/show_bug.cgi?id=284250'>284250</a>)

### KTextWidgets

- Adição de um novo submenu ao KTextEdit para mudar o idioma na verificação ortográfica
- Correção do carregamento das configurações padrão do Sonnet

### KWallet Framework

- Uso do KDE_INSTALL_DBUSINTERFACEDIR para instalar as interfaces D-Bus
- Correção dos avisos do arquivo de configuração do KWallet no início da sessão (erro <a href='https://bugs.kde.org/show_bug.cgi?id=351805'>351805</a>)
- Prefixo adequado do resultado do kwallet-pam

### KWidgetsAddons

- Adição do elemento de contêiner flexível: KCollapsibleGroupBox
- KNewPasswordWidget: correção da ausência de inicialização de cores
- Introdução do KNewPasswordWidget

### KXMLGUI

- kmainwindow: Pré-preenchimento da informação do tradutor quando disponível. (erro <a href='https://bugs.kde.org/show_bug.cgi?id=345320'>345320</a>)
- Possibilidade de associar a tecla do menu de contexto (inferior direita) aos atalhos (erro <a href='https://bugs.kde.org/show_bug.cgi?id=165542'>165542</a>)
- Adição de função para consultar a localização do arquivo XML <i>standards</i>
- Permissão para uso do <i>framework</i> <b>kxmlgui</b> sem nenhum arquivo instalado
- Adição de dependências obrigatórias que faltavam

### Plasma Framework

- Correção dos itens da <i>TabBar</i> que iniciavam dispostos de forma inadequada, e podia ser visto, por exemplo, no Kickoff após o início do Plasma
- Correção do problema existente ao soltar arquivos na área de trabalho ou no painel, que deixava de apresentar uma seleção com as ações que podiam ser executadas
- Considerar o QApplication::wheelScrollLines a partir do ScrollView
- Uso do BypassWindowManagerHint apenas na plataforma X11
- Remoção do plano de fundo antigo do painel
- Campo incremental mais legível com tamanhos pequenos
- Histórico de visualização colorido
- Calendário: Possibilidade de clicar com o ponteiro do mouse sobre toda a área do cabeçalho
- Calendário: Não usar o número do dia atual no <i>goToMonth</i>
- Calendário: Correção da visão de atualização de décadas
- Ícones do tema Breeze, quando carregados através do IconItem
- Correção da propriedade <i>minimumWidth</i> do objeto <i>Button</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=353584'>353584</a>)
- Introdução do sinal <i>appletCreated</i>
- Ícone do Plasma Breeze: Adição de elementos <i>id</i> do SVG no Touchpad
- Ícone Plasma Breeze: alteração do Touchpad para o tamanho 22x22px
- Ícone Breeze: adição do ícone do widget às notas
- Um script para substituir cores pré-definidas por folhas de estilo
- Aplicação do SkipTaskbar no ExposeEvent
- Não definir o SkipTaskbar em cada evento

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
