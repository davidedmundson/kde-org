---
aliases:
- ../announce-applications-16.12.2
changelog: true
date: 2017-02-09
description: KDE veröffentlicht die KDE-Anwendungen 16.12.2
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 16.12.2
version: 16.12.2
---
09. Februar 2017. Heute veröffentlicht KDE die zweite Aktualisierung der <a href='../16.12.0'>KDE-Anwendungen 16.12</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 20 aufgezeichnete Fehlerkorrekturen enthalten unter anderem Verbesserungen für kdepim, Dolphin, Kate, Kdenlive, KTouch und Okular.

Diese Veröffentlichung enthält die Version der KDE Development Platform %1 mit langfristige Unterstützung.
