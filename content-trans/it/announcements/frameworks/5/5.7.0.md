---
aliases:
- ../../kde-frameworks-5.7.0
date: '2015-02-14'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Generale

- A number of fixes for compiling with the upcoming Qt 5.5

### KActivities

- Fixed starting and stopping activities
- Fixed activity preview showing wrong wallpaper occasionally

### KArchive

- Create temporary files in the temp dir rather than in the current directory

### KAuth

- Fixed generation of KAuth DBus helper service files

### KCMUtils

- Fixed assert when dbus paths contain a '.'

### KCodecs

- Added support for CP949 to KCharsets

### KConfig

- kconf_update no longer processes *.upd file from KDE SC 4. Add "Version=5" to top of the upd file for updates that should be applied to Qt5/KF5 applications
- Fixed KCoreConfigSkeleton when toggling a value with saves in between

### KConfigWidgets

- KRecentFilesAction: fixed menu entry order (so it matches the kdelibs4 order)

### KCoreAddons

- KAboutData: Call addHelpOption and addVersionOption automatically, for convenience and consistency
- KAboutData: Bring back "Please use http://bugs.kde.org to report bugs." when no other email/url is set
- KAutoSaveFile: allStaleFiles() now works as expected for local files, fixed staleFiles() too
- KRandomSequence now uses int's internally and exposes int-api for 64-bit unambiguity
- Mimetype definitions: *.qmltypes and *.qmlproject files also have the text/x-qml mime type
- KShell: make quoteArgs quote urls with QChar::isSpace(), unusual space characters were not handled properly
- KSharedDataCache: fix creation of directory containing the cache (porting bug)

### KDBusAddons

- Added helper method KDEDModule::moduleForMessage for writing more kded-like daemons, such as kiod

### KDeclarative

- Added a plotter component
- Added overload method for Formats::formatDuration taking int
- New properties paintedWidth and paintedHeight added to QPixmapItem and QImageItem
- Fixed painting QImageItem and QPixmapItem

### Kded

- Add support for loading kded modules with JSON metadata

### KGlobalAccel

- Now includes the runtime component, making this a tier3 framework
- Made the Windows backend work again
- Re-enabled the Mac backend
- Fixed crash in KGlobalAccel X11 runtime shutdown

### KI18n

- Mark results as required to warn when API is misused
- Added BUILD_WITH_QTSCRIPT buildsystem option to allow a reduced feature-set on embedded systems

### KInit

- OSX: load the correct shared libraries at runtime
- Mingw compilation fixes

### KIO

- Fixed crash in jobs when linking to KIOWidgets but only using a QCoreApplication
- Fixed editing web shortcuts
- Added option KIOCORE_ONLY, to compile only KIOCore and its helper programs, but not KIOWidgets or KIOFileWidgets, thus reducing greatly the necessary dependencies
- Added class KFileCopyToMenu, which adds Copy To / Move To" to popupmenus
- SSL-enabled protocols: added support for TLSv1.1 and TLSv1.2 protocols, remove SSLv3
- Fixed negotiatedSslVersion and negotiatedSslVersionName to return the actual negotiated protocol
- Apply the entered URL to the view when clicking the button that switches the URL navigator back to breadcrumb mode
- Fixed two progress bars/dialogs appearing for copy/move jobs
- KIO now uses its own daemon, kiod, for out-of-process services previously running in kded, in order to reduce dependencies; currently only replaces kssld
- Fixed "Could not write to &lt;path&gt;" error when kioexec is triggered
- Fixed "QFileInfo::absolutePath: Constructed with empty filename" warnings when using KFilePlacesModel

### KItemModels

- Fixed KRecursiveFilterProxyModel for Qt 5.5.0+, due to QSortFilterProxyModel now using the roles parameter to the dataChanged signal

### KNewStuff

- Always reload xml data from remote urls

### KNotifications

- Documentation: mentioned the file name requirements of .notifyrc files
- Fixed dangling pointer to KNotification
- Fixed leak of knotifyconfig
- Install missing knotifyconfig header

### KPackage

- Renamed kpackagetool man to kpackagetool5
- Fixed installation on case-insensitive filesystems

### Kross

- Fixed Kross::MetaFunction so it works with Qt5's metaobject system

### KService

- Include unknown properties when converting KPluginInfo from KService
- KPluginInfo: fixed properties not being copied from KService::Ptr
- OS X: performance fix for kbuildsycoca4 (skip app bundles)

### KTextEditor

- Fixed high-precision touchpad scrolling
- Do not emit documentUrlChanged during reload
- Do not break cursor position on document reload in lines with tabs
- Do not re(un)fold the first line if it was manually (un)folded
- vimode: command history through arrow keys
- Do not try to create a digest when we get a KDirWatch::deleted() signal
- Performance: remove global initializations

### KUnitConversion

- Fixed infinite recursion in Unit::setUnitMultiplier

### KWallet

- Automatically detect and convert old ECB wallets to CBC
- Fixed the CBC encryption algorithm
- Ensured wallet list gets updated when a wallet file gets removed from disk
- Remove stray &lt;/p&gt; in user-visible text

### KWidgetsAddons

- Use kstyleextensions to specify custom control element for rendering kcapacity bar when supported, this allow the widget to be styled properly
- Provide an accessible name for KLed

### KWindowSystem

- Fixed NETRootInfo::setShowingDesktop(bool) not working on Openbox
- Added convenience method KWindowSystem::setShowingDesktop(bool)
- Fixes in icon format handling
- Added method NETWinInfo::icccmIconPixmap provides icon pixmap from WM_HINTS property
- Added overload to KWindowSystem::icon which reduces roundtrips to X-Server
- Added support for _NET_WM_OPAQUE_REGION

### NetworkmanagerQt

- Do not print a message about unhandled "AccessPoints" property
- Added support for NetworkManager 1.0.0 (not required)
- Fixed VpnSetting secrets handling
- Added class GenericSetting for connections not managed by NetworkManager
- Added property AutoconnectPriority to ConnectionSettings

#### Plasma Framework

- Fixed errorneously opening a broken context menu when middle clicking Plasma popup
- Trigger button switch on mouse wheel
- Never resize a dialog bigger than the screen
- Undelete panels when applet gets undeleted
- Fixed keyboard shortcuts
- Restore hint-apply-color-scheme support
- Reload the configuration when plasmarc changes
- ...

### Solid

- Added energyFull and energyFullDesign to Battery

### Buildsystem changes (extra-cmake-modules)

- New ECMUninstallTarget module to create an uninstall target
- Make KDECMakeSettings import ECMUninstallTarget by default
- KDEInstallDirs: warn about mixing relative and absolute installation paths on the command line
- Added ECMAddAppIcon module to add icons to executable targets on Windows and Mac OS X
- Fixed CMP0053 warning with CMake 3.1
- Do not unset cache variables in KDEInstallDirs

### Frameworkintegration

- Fix updating of single click setting at runtime
- Multiple fixes to the systemtray integration
- Only install color scheme on toplevel widgets (to fix QQuickWidgets)
- Update XCursor settings on X11 platform

Puoi discutere e condividere idee su questo rilascio nella sezione dei commenti nell'<a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>articolo sul Dot</a>.
