---
aliases:
- ../announce-applications-15.12.0
changelog: true
date: 2015-12-16
description: KDE rilascia Applications 15.12.
layout: application
release: applications-15.12.0
title: KDE rilascia KDE Applications 15.12.0
version: 15.12.0
---
16 dicembre 2015. Oggi KDE rilascia KDE Applications 15.12.

KDE è entusiasta di annunciare il rilascio delle KDE Applications 15.12, l'aggiornamento di dicembre 2015 per KDE Applications. Questa versione porta una nuova applicazione e aggiunte di funzionalità e correzioni di bug alle applicazioni esistenti. La squadra si impegna a portare sempre la migliore qualità al tuo desktop e a queste applicazioni, quindi contiamo su di te per inviare le tue segnalazioni.

In questa versione, abbiamo aggiornato tutta una serie di applicazioni per utilizzare il nuovo <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>, inclusi <a href='https://edu.kde.org/applications/all/artikulate/'>Artikulate</a>, <a href='https://www.kde.org/applications/system/krfb/'>Krfb</a>, <a href='https://www.kde.org/applications/system/ksystemlog/'>KSystemLog</a>, <a href='https://games.kde.org/game.php?game=klickety'>Klickety</a> e altri giochi KDE, oltre all'interfaccia del dell'estensione immagini KDE e le sue librerie di supporto. Ciò porta il numero totale di applicazioni che utilizzano KDE Frameworks 5 a 126.

### Una nuova aggiunta spettacolare

Dopo 14 anni di appartenenza a KDE, KSnapshot è stato ritirato e sostituito con una nuova applicazione per la creazione di schermate, Spectacle.

Con nuove funzionalità e un'interfaccia utente completamente nuova, Spectacle rende l'acquisizione di schermate più facile e discreta che mai. Oltre a quello che potevi fare con KSnapshot, con Spectacle ora puoi acquisire schermate composite di menu a comparsa insieme alle finestre genitrici o fare schermate dell'intero schermo (o della finestra attualmente attiva) senza nemmeno avviare Spectacle, semplicemente utilizzando le scorciatoie da tastiera Maiusc+Stamp e Meta+Stamp rispettivamente.

Abbiamo anche ottimizzato in modo aggressivo il tempo di avvio dell'applicazione, per ridurre al minimo l'intervallo di tempo tra l'avvio dell'applicazione e l'acquisizione dell'immagine.

### Pulizia ovunque

Molte delle applicazioni hanno subito un miglioramento significativo in questo ciclo, oltre a stabilità e correzioni di bug.

<a href='https://userbase.kde.org/Kdenlive'>Kdenlive</a>, l'editor video non lineare, ha visto soluzioni importanti per la sua interfaccia utente. Ora puoi copiare e incollare gli elementi sulla linea temporale e disattivare facilmente la trasparenza in una determinata traccia. I colori dell'icona si adattano automaticamente al tema dell'interfaccia utente principale, rendendoli più facili da vedere.

{{<figure src="/announcements/applications/15.12.0/ark1512.png" class="text-center" width="600px" caption=`Ark ora può mostrare i commenti ZIP`>}}

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, il gestore dell'archivio, ora può visualizzare commenti incorporati negli archivi ZIP e RAR. Abbiamo migliorato il supporto per i caratteri Unicode nei nomi di file negli archivi ZIP e ora puoi rilevare archivi danneggiati e recuperare il maggior numero possibile di dati da essi. Puoi anche aprire file archiviati nella loro applicazione predefinita e abbiamo risolto il trascinamento e rilascio sul desktop, così come le anteprime per i file XML.

### Tutto gioco e niente lavoro

{{<figure src="/announcements/applications/15.12.0/ksudoku1512.png" class="text-center" width="600px" caption=`Nuova schermata di benvenuto di KSudoku (su Mac OS X)`>}}

Gli sviluppatori di KDE Games hanno lavorato duramente negli ultimi mesi per ottimizzare i nostri giochi per un'esperienza più fluida e ricca, e abbiamo molte novità in quest'area per il divertimento dei nostri utenti.

In <a href='https://www.kde.org/applications/games/kgoldrunner/'>KGoldrunner</a>, abbiamo aggiunto due nuovi insiemi di livelli, uno che consente di scavare durante la caduta e uno no. Abbiamo aggiunto soluzioni per diversi insiemi di livelli esistenti. Per una sfida aggiuntiva, ora impediamo di scavare durante la caduta in alcuni insiemi di livelli più datati.

In <a href='https://www.kde.org/applications/games/ksudoku/'>KSudoku</a>, ora puoi stampare rompicapi di Mathdoku e Killer Sudoku. La nuova disposizione a più colonne nella schermata di benvenuto di KSudoku rende più facile vedere più elementi dei tanti tipi di rompicapi disponibili e le colonne si regolano automaticamente man mano che la finestra viene ridimensionata. Abbiamo fatto lo stesso con Palapeli, rendendo più facile vedere più elementi della tua collezione rompicapi contemporaneamente.

Abbiamo anche incluso correzioni di stabilità per giochi come KNavalBattle, Klickety, <a href='https://www.kde.org/applications/games/kshisen/'>KShisen</a> e <a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a> e l'esperienza complessiva è ora notevolmente migliorata. KTuberling, Klickety e KNavalBattle sono stati anche aggiornati per utilizzare il nuovo KDE Frameworks 5.

### Correzioni importanti

Non odi semplicemente quando la tua applicazione preferita si arresta in modo anomalo nel momento peggiore? Lo facciamo anche noi, e per rimediare abbiamo lavorato molto duramente per sistemare molti bug per te, ma probabilmente abbiamo tralasciato qualcosa, quindi non dimenticare di <a href='https://bugs.kde.org'>segnalarli</a>!

Nel nostro file manager  <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, abbiamo incluso varie correzioni per la stabilità e alcune correzioni per rendere più fluido lo scorrimento. In <a href='https://www.kde.org/applications/education/kanagram/'>Kanagram</a>, abbiamo risolto un problema fastidioso con il testo bianco su sfondi bianchi. In <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, abbiamo tentato di correggere un arresto anomalo che si verificava durante l'arresto, oltre a ripulire l'interfaccia utente e aggiungere un nuovo strumento di pulizia della cache.

La <a href='https://userbase.kde.org/Kontact'>suite Kontact</a> ha visto tonnellate di funzionalità aggiunte, grandi correzioni e ottimizzazioni delle prestazioni. In effetti, c'è stato così tanto sviluppo in questo ciclo che abbiamo portato il numero di versione a 5.1. La squadra è al lavoro e non vede l'ora di ricevere i vostri riscontri.

### Andando avanti

Come parte dello sforzo di modernizzare le nostre offerte, abbiamo eliminato alcune applicazioni da Applicazioni KDE e non le stiamo più rilasciando a partire da Applicazioni KDE 15.12

Abbiamo eliminato 4 applicazioni dal rilascio: Amor, KTux, KSnapshot e SuperKaramba. Come indicato sopra, KSnapshot è stato sostituito da Spectacle e Plasma sostituisce essenzialmente SuperKaramba come motore di oggetti. Abbiamo abbandonato i salvaschermo indipendenti perché il blocco dello schermo viene gestito in modo molto diverso nei desktop moderni.

Abbiamo anche rilasciato 3 pacchetti di grafica (kde-base-artwork, kde-wallpapers e kdeartwork); il loro contenuto non era cambiato da molto tempo.

### Elenco completo dei cambiamenti
