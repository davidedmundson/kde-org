---
aliases:
- ../announce-applications-16.04.1
changelog: true
date: 2016-05-10
description: KDE rilascia KDE Applications 16.04.1
layout: application
title: KDE rilascia KDE Applications 16.04.1
version: 16.04.1
---
10 maggio 2016. Oggi KDE ha rilasciato il primo aggiornamento di stabilizzazione per <a href='../16.04.0'>KDE Applications 16.04</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 25 errori corretti includono, tra gli altri, miglioramenti a kdepim, Ark, Kate, Dolphin, Kdenlive, Lokalize e Spectacle.

Questo rilascio include inoltre le versioni con supporto a lungo termine di KDE Development Platform 4.14.20.
