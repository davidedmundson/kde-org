---
aliases:
- ../announce-applications-18.12.2
changelog: true
date: 2019-02-07
description: KDE rilascia Applications 18.12.2.
layout: application
major_version: '18.12'
release: applications-18.12.2
title: KDE rilascia KDE Applications 18.12.2
version: 18.12.2
---
{{% i18n_date %}}

Oggi KDE ha rilasciato il secondo aggiornamento di stabilizzazione per <a href='../18.12.0'>KDE Applications 18.12</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Più di una dozzina di errori corretti includono, tra gli altri, miglioramenti a Kontact, Ark, Konsole, Lokalize e Umbrello.

I miglioramenti includono:

- Ark non elimina più i file salvati dall'interno del visore incorporato</li>
- La rubrica degli indirizzi ora ricorda i compleanni quando si uniscono i contatti</li>
- Sono stati risolti vari problemi di mancato aggiornamento dei grafici in Umbrello</li>
