---
aliases:
- ../announce-applications-16.04-rc
custom_spread_install: true
date: 2016-04-05
description: KDE rilascia la Release Candidate di KDE Applications 16.04
layout: application
release: applications-16.03.90
title: KDE rilascia la Release Candidate di KDE Applications 16.04
---
7 aprile 2016. Oggi KDE ha rilasciato la release candidate della nuova versione di KDE Applications. Con il &quot;congelamento&quot; di dipendenze e funzionalità, l'attenzione degli sviluppatori KDE è adesso concentrata sulla correzione dei bug e sull'ulteriore rifinitura dei programmi.

Dato che molte applicazioni sono ora basate su KDE Frameworks 5, KDE Applications 16.04 ha bisogno della qualità e dell'esperienza utente. Gli utenti &quot;reali&quot; sono fondamentali per mantenere la qualità di KDE, perché gli sviluppatori non possono testare completamente ogni possibile configurazione. Contiamo su di voi per aiutarci a trovare i bug al più presto possibile perché possano essere eliminati prima della versione finale. Valutate la possibilità di contribuire al gruppo di sviluppo installando la versione <a href='https://bugs.kde.org/'>e segnalando ogni problema</a>.

#### Installazione dei pacchetti binari di KDE Applications 16.04 Release Candidate

<em>Pacchetti</em>. Alcuni fornitori di sistemi Linux/UNIX hanno gentilmente messo a disposizione pacchetti binari di KDE Applications 16.04 Release Candidate (internamente 16.03.90) per alcune versioni delle rispettive distribuzioni, e in altri casi hanno provveduto i volontari della comunità. Altri pacchetti binari, così come aggiornamenti ai pacchetti ora disponibili, potrebbero essere disponibili nelle prossime settimane.

<em>Posizioni dei pacchetti</em>. Per l'elenco aggiornato dei pacchetti binari disponibili di cui il progetto KDE è stato informato, visita <a href='http://community.kde.org/KDE_SC/Binary_Packages'>il wiki Community</a>.

#### Compilazione di KDE Applications 16.04 Release Candidate

Il codice sorgente completo per KDE Applications 16.04 Release Candidate può essere <a href='http://download.kde.org/unstable/applications/16.03.90/src/'>scaricato liberamente</a>. Le istruzioni per la compilazione e l'installazione sono disponibili dalla <a href='/info/applications/applications-16.03.90'>pagina di informazioni di KDE Applications 16.04 Release Candidate</a>.
