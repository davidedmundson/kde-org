---
aliases:
- ../announce-applications-16.08-beta
date: 2016-07-22
description: KDE rilascia Applications 16.08 Beta.
layout: application
release: applications-16.07.80
title: KDE rilascia la beta di KDE Applications 16.08
---
22 luglio 2016. Oggi KDE ha rilasciato la beta della nuova versione di KDE Applications. Con il &quot;congelamento&quot; di dipendenze e funzionalità, l'attenzione degli sviluppatori KDE è adesso concentrata sulla correzione degli errori e sull'ulteriore rifinitura del sistema.

Controlla le <a href='https://community.kde.org/Applications/16.08_Release_Notes'>note di rilascio della comunità</a> per informazioni sui nuovi archivi tar, sui programmi che ora sono basati su KF5 e sui problemi noti. Un annuncio più completo verrà reso disponibile in concomitanza con la versione finale

I rilasci di KDE Applications 16.08 hanno bisogno di una verifica accurata per mantenere e migliorare la qualità e l'esperienza utente. Gli utenti &quot;reali&quot; sono fondamentali per mantenere la qualità di KDE, perché gli sviluppatori non possono testare completamente ogni possibile configurazione. Contiamo su di voi per aiutarci a trovare gli errori il più presto possibile affinché possano essere eliminati prima della versione finale. Valutate la possibilità di partecipare alla squadra installando la versione &quot;beta&quot; <a href='https://bugs.kde.org/'>e segnalando qualsiasi problema</a>.
