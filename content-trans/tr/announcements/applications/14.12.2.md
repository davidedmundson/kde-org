---
aliases:
- ../announce-applications-14.12.2
changelog: true
date: '2015-02-03'
description: KDE Uygulamalar 14.12.2.'yi Gönderdi
layout: application
title: KDE, KDE Uygulamaları 14.12.2'yi Paketliyor
version: 14.12.2
---
February 3, 2015. Today KDE released the second stability update for <a href='../14.12.0'>KDE Applications 14.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

20 den fazla hata gidermesi, anagram oyunu Kanagram, Umbrello UML Modelleyici, belge gösterici Okular ve sanal dünya Marble uygulamaları için kaydedildi.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.16, KDE Development Platform 4.14.5 and the Kontact Suite 4.14.5.
