---
aliases:
- ../announce-applications-19.04.1
changelog: true
date: 2019-05-09
description: KDE Uygulamalar 19.04.1.'i Gönderdi
layout: application
major_version: '19.04'
release: applications-19.04.1
title: KDE, KDE Uygulamalar 19.04.1'i Gönderdi
version: 19.04.1
---
{{% i18n_date %}}

Bugün KDE <a href='../19.04.0'>KDE Uygulamaları 19.04</a> için ilk kararlılık güncellemesini yayınladı. Bu sürüm yalnızca hata düzeltmeleri ve çeviri güncellemelerini içerir, herkes için güvenli ve hoş bir güncelleme sağlar.

Kaydedilen yaklaşık yirmi hata düzeltmesi, diğerleri arasında Kontak, Ark, Cantor, Dolphin, Kdenlive, Spectacle, Umbrello'daki iyileştirmeleri içerir.

İyileştirmeler şunları içerir:

- Tagging files on the desktop no longer truncates the tag name
- A crash in KMail's text sharing plugin has been fixed
- Several regressions in the video editor Kdenlive have been corrected
