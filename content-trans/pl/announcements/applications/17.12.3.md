---
aliases:
- ../announce-applications-17.12.3
changelog: true
date: 2018-03-08
description: KDE wydało Aplikacje KDE 17.12.3
layout: application
title: KDE wydało Aplikacje KDE 17.12.3
version: 17.12.3
---
8 marca 2018. Dzisiaj KDE wydało trzecie uaktualnienie stabilizujące <a href='../17.12.0'>Aplikacji KDE 17.12</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

About 25 recorded bugfixes include improvements to Kontact, Dolphin, Gwenview, JuK, KGet, Okular, Umbrello, among others.

Wśród ulepszeń znajdują się:

- Akregator no longer erases the feeds.opml feed list after an error
- Gwenview's fullscreen mode now operates on the correct filename after renaming
- Several rare crashes in Okular have been identified and fixed
