---
aliases:
- ../announce-applications-17.12-beta
custom_spread_install: true
date: 2017-11-17
description: KDE wydało Aplikacje 17.12 Beta.
layout: application
release: applications-17.11.80
title: KDE wydało betę Aplikacji KDE 17.12
---
17 listopada 2017. Dzisiaj KDE wydało wersję beta nowej wersji Aplikacji KDE. Wersja ta zamraża wszelkie zmiany w zależnościach i funkcjonalności, a zespół KDE będzie się skupiał jedynie na naprawianiu w niej błędów i dalszym udoskonalaniu.

Check the <a href='https://community.kde.org/Applications/17.12_Release_Notes'>community release notes</a> for information on tarballs that are now KF5 based and known issues. A more complete announcement will be available for the final release

Aplikacje KDE, wydanie 17.12 potrzebuje wypróbowania go w szerokim zakresie, aby utrzymać i ulepszyć jakość i odczucia użytkownika. Obecnie użytkownicy są znaczącym czynnikiem przy utrzymywaniu wysokiej jakości KDE, bo programiści po prostu nie mogą wypróbować każdej możliwej konfiguracji. Liczymy, że wcześnie znajdziesz błędy, tak aby mogły zostać poprawione przed wydaniem końcowym. Proszę rozważyć dołączenie do zespołu poprzez zainstalowanie bety i <a href='https://bugs.kde.org/'>zgłaszanie wszystkich błędów</a>.

#### Instalowanie pakietów binarnych Aplikacji KDE 17.12 Beta 

<em>Pakiety</em>. Niektórzy wydawcy systemów operacyjnych Linux/UNIX  uprzejmie dostarczyli pakiety binarne Aplikacji KDE 7.12 Beta (wewnętrznie 17.11.80) dla niektórych wersji ich dystrybucji, a w niektórych przypadkach dokonali tego wolontariusze społeczności. Dodatkowe pakiety binarne, tak samo jak uaktualnienia do pakietów już dostępnych, mogą stać się dostępne w przeciągu nadchodzących tygodni.

<em>Położenie Pakietów</em>. Po bieżącą listę dostępnych pakietów  binarnych, o których został poinformowany Projekt KDE, zajrzyj na  stronę <a href='http://community.kde.org/Binary_Packages'> Społeczności Wiki</a>.

#### Kompilowanie Aplikacji KDE 17.12 Beta

Pełny kod źródłowy dla Aplikacji KDE 7.12 Beta1 można <a href='http://download.kde.org/unstable/applications/17.11.80/src/'>pobrać bez opłaty</a>.Instrukcje na temat kompilowania i instalowania są dostępne na <a href='/info/applications/applications-17.11.80.php'>Stronie informacyjnej Aplikacji KDE Beta 17.12</a>.
