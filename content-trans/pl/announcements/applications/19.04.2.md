---
aliases:
- ../announce-applications-19.04.2
changelog: true
date: 2019-06-06
description: KDE wydało Aplikacje 19.04.2.
layout: application
major_version: '19.04'
release: applications-19.04.2
title: KDE wydało Aplikacje KDE 19.04.2
version: 19.04.2
---
{{% i18n_date %}}

Dzisiaj KDE wydało drugie uaktualnienie stabilizujące <a href='../19.04.0'>Aplikacji KDE 19.04</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

Nearly fifty recorded bugfixes include improvements to Kontact, Ark, Dolphin, JuK, Kdenlive, KmPlot, Okular, Spectacle, among others.

Wśród ulepszeń znajdują się:

- A crash with viewing certain EPUB documents in Okular has been fixed
- Secret keys can again be exported from the Kleopatra cryptography manager
- The KAlarm event reminder no longer fails to start with newest PIM libraries
