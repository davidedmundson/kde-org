---
aliases:
- ../announce-applications-15.08.2
changelog: true
date: 2015-10-13
description: KDE wydało Aplikacje KDE 15.08.2
layout: application
title: KDE wydało Aplikacje KDE 15.08.2
version: 15.08.2
---
13 października 2015. Dzisiaj KDE wydało drugie uaktualnienie stabilizujące <a href='../15.08.0'>Aplikacji KDE 15.08</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

Więcej niż 30 zarejestrowanych poprawek błędów uwzględnia ulepszenia do ark, gwenview, kate, kbruch, kdelibs, kdepim, lokalize oraz umbrello.

To wydanie zawiera także długoterminowo wspieraną wersję Platformy Programistycznej KDE 4.14.13.
