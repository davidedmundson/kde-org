---
title: KDE for Travelers
description: Travel the World Using KDE Applications
images:
- /for/travelers/thumbnail.png
sassFiles:
- /scss/for/travelers.scss
other:
 - name: Organic Maps
   description: Offline hiking, biking, trails and navigation
   link: https://organicmaps.app/
   img: organic-maps-logo.svg
 - name: Öffi
   description: Train and bus navigator. Includes delays and replacement bus service
   link: https://oeffi.schildbach.de/index.html
   img: offi-logo.jpg
 - name: Transportr
   description: Free public transport assistant
   link: https://transportr.app/
   img: transportr-logo.png
 - name: Träwelling
   link: https://traewelling.de
   description: Share your trips with your friends
   img: traweling-logo.png
 - name: Freifunk-Karte
   description: You need free WiFi on your road? It shows the German Freifunk Nodes on a OpenStreetMap.
   link: https://f-droid.org/en/packages/de.freifunk_karte.freifunk_karte/
   img: freifunk.png
 - name: GeoNotes
   description: A simple app to create georeferenced notes. 
   link: https://f-droid.org/en/packages/de.hauke_stieler.geonotes/
   img: geonotes.png
 - name: CityZen
   description: City navigation app
   link: https://f-droid.org/en/packages/com.cityzen.cityzen/
   img: cityzen.png
 - name: Street­Complete
   description: OpenStreetMap surveyor app
   link: https://f-droid.org/en/packages/de.westnordost.streetcomplete/
   img: openstreetcomplet.png
---

{{< container class="text-center text-small itinerary" >}}

![](https://apps.kde.org/app-icons/org.kde.itinerary.svg)

## KDE Itinerary

KDE Itinerary is a digital travel assistant that protects your privacy. It makes collecting all the information about your travel inside a single application easy and straightforward. KDE Itinerary is available for [Plasma Mobile](https://plasma-mobile.org/) and Android.

{{< store_badge type="fdroid" link="https://community.kde.org/Android/FDroid#KDE_F-Droid_Release_Repository" >}}

{{< /container >}}

{{< feature-container img="itinerary-trip.png" >}}

## Store your reservations

Store all the information about your reservations in Itinerary. This includes QR-codes, check-in times, arrivial times, real-time delays, seat reservations, coach layout, and more.

Itinerary supports train, bus and flight bookings, as well as hotel, restaurant, event and rental car reservations. Traveling in a group? Not a problem, Itinerary supports multi-traveler bookings.

{{< /feature-container >}}

{{< feature-container img="itinerary-ticket.png" reverse="true" >}}

## Local first

Itinerary automatically extracts booking data from various input formats. It's all performed locally on **your** device and your data is not sent to any remote servers.

This works best when using [KMail](https://kontact.kde.org/components/kmail/) to extract tickets from your email and then [KDE Connect](https://kdeconnect.kde.org) to transfer tickets to your phone. This also works great with [Nextcloud Mail](https://apps.nextcloud.com/apps/mail) and [DavDroid](https://f-droid.org/en/packages/at.bitfire.davdroid/) to sync your tickets from [Nextcloud](https://nextcloud.com/).

![KMail ticket extraction showing a train trip from Berlin to Tübingen](kmail.png)

{{< /feature-container >}}

{{< feature-container img="itinerary-connections.png" >}}

## Add your connections

Aside from finding reservations automatically in your email, Itinerary lets you add train trips manually to your journey, find alternative connections if your train is cancelled, or, for some providers, import your train trip directly from your reservation number.

{{< /feature-container >}}

{{< feature-container img="itinerary-opening-hours.png" reverse="true" >}}

## Find your way

Powered by [OpenStreetMap](https://www.openstreetmap.org), the indoor map at train stations or airports can be a life saver. Use Itinerary to locate your platform is, and, if you have seat reservation and the train layout is available, it can even show you exactly which platform section is best for you.

![Train station map in KDE Itinerary, highlighting relevant platform sections.](https://www.volkerkrause.eu/assets/posts/139/kde-itinerary-platform-section-highlighting.jpg)

The indoor map also shows you which shops and resturantes are currently open, which elevator is broken (yet again!), where the toilets are and where the correct exit is.

{{< /feature-container >}}

{{< feature-container img="itinerary-live-status.png" >}}

## Real time

It is rare that a train or bus departs or arrives on time. Itinerary keeps you updated when delays are announced.

On supported train and long distance buses, Itinerary will also use the onboard APIs to fetch the current live status of the vehicle and keep you updated on your current position and any announcements.

{{< /feature-container >}}

{{< section class="arianna py-5" >}}

## Arianna

Arianna is an excellent ebook reader that lets you read your favorite books while traveling. Arianna will track your reading progress
and classify your books by genre and author automatically.

{{< for/app-links learn="https://apps.kde.org/arianna/" dark="true" >}}

![Screenshot of Arianna](https://cdn.kde.org/screenshots/arianna/reader.png)

{{< /section >}}

{{< section class="pt-5" >}}

## Kasts

Enjoy listening to podcasts on the move with Kasts! Subscribe to your favorite podcasts and get updated as soon as a new episode is out.

{{< for/app-links learn="https://apps.kde.org/kasts/" >}}

![Screenshot of Kasts](https://cdn.kde.org/screenshots/kasts/kasts-desktop.png)

Kasts is also available on Android and [Plasma Mobile](https://plasma-mobile.org).

{{< /section >}}

{{< section class="small-fdroid" >}}

{{< store_badge type="fdroid" link="https://community.kde.org/Android/FDroid#KDE_F-Droid_Release_Repository" >}}

{{< /section >}}

{{< section class="kgeotag py-5" >}}

## KGeoTag

KGeoTag is a geotagging program. It lets you tag your images with geocoordinates by matching them to a corresponding GPX track or by manually setting them by drag and dropping the images, or entering the coordinates by hand.

It is very helpful in combination with [KPhotoAlbum](https://www.kphotoalbum.org/) or [Digikam](https://www.digikam.org/) when you want to organize your photo collection after your trip and then visualize all the locations you visited.

{{< for/app-links learn="https://kgeotag.kde.org/" dark="true" >}}

![Screenshot of KGeoTag showing a track on a map with some image preview allong the track](https://cdn.kde.org/screenshots/kgeotag/kgeotag_shadow.png)

{{< /section >}}

{{< section class="pt-5" >}}

## Marble

Explore the world with Marble. Marble contains a huge collection of maps that let you travel all over the globe from your desktop. Visit remote places via detailed satellite images, travel the world before the discovery of America, and check out the average temperature and precipitation in winter and summer in other countries.

Marble also allows you to bookmark your favorite locations and lets you find  your way thanks to its routing algorithm and OpenStreetMap powered maps.

{{< for/app-links learn="https://apps.kde.org/marble/" >}}

![Screenshot of Kasts](https://cdn.kde.org/screenshots/marble/marble-world.png)

{{< /section >}}

{{< container class="text-small" >}}

## Other open source apps for you

Here are some other applications from other open source communities that will make your travels fun.

{{< link-boxes >}}

{{< /container >}}
