2006-10-09 05:26 +0000 [r593791]  ianw

	* branches/KDE/3.5/kdegames/kgoldrunner/src/kgoldrunner.cpp,
	  branches/KDE/3.5/kdegames/kgoldrunner/src/kgoldrunnerui.rc: BUG:
	  135144 Make actions for increase/decrease speed repeatable, not
	  toggle.

2006-10-09 09:04 +0000 [r593833]  mueller

	* branches/KDE/3.5/kdegames/ksnake/snake.cpp: fix undefined
	  operation

2006-10-11 13:11 +0000 [r594491]  woebbe

	* branches/KDE/3.5/kdegames/kpat/klondike.cpp: backport of rev.
	  594480 isGameLost(): don't use invalid iterator

2006-10-11 13:21 +0000 [r594497]  woebbe

	* branches/KDE/3.5/kdegames/kpat/klondike.cpp: even nicer

2006-10-11 15:07 +0000 [r594541]  woebbe

	* branches/KDE/3.5/kdegames/kpat/klondike.cpp: redeal(): fixed
	  broken reverse iteration

2006-10-12 02:11 +0000 [r594702]  ianw

	* branches/KDE/3.5/kdegames/libkdegames/kstdgameaction.cpp: Swap
	  roles of light bulb and wizard's wand icons. Light bulb becomes
	  Hint action: wand becomes Solve.

2006-10-14 05:58 +0000 [r595343]  ianw

	* branches/KDE/3.5/kdegames/kgoldrunner/src/kgrfigure.cpp,
	  branches/KDE/3.5/kdegames/kgoldrunner/src/kgrfigure.h,
	  branches/KDE/3.5/kdegames/kgoldrunner/src/kgrcanvas.cpp: Fix
	  game-engine deadlocks and a wrong comparison in KGrCanvas code.

2006-10-18 03:28 +0000 [r596638]  mattr

	* branches/KDE/3.5/kdegames/kmines/field.cpp,
	  branches/KDE/3.5/kdegames/kmines/field.h: Apply patch from bug
	  #133257 to fix that bug for the reporter. No response from the
	  assignee (who appears to be inactive for quite awhile). Thanks
	  for the patch! BUG: 133257

2006-10-23 05:15 +0000 [r598267]  ianw

	* branches/KDE/3.5/kdegames/doc/kgoldrunner/tute008.png: Corrected
	  the picture to agree with the handbook text. Changed the window
	  frame to Plastik style.

2006-10-28 21:51 +0000 [r599868]  binner

	* branches/KDE/3.5/kdegames/lskat/lskat.desktop: fix typo

2006-11-07 19:25 +0000 [r603080]  aacid

	* branches/KDE/3.5/kdegames/katomic/levels/level_84 (removed),
	  branches/KDE/3.5/kdegames/katomic/levels/level_83 (added): Move
	  level_84 to level_83 following Dmitry advise

2006-11-17 10:57 +0000 [r605590]  coolo

	* branches/KDE/3.5/kdegames/kpat/freecell-solver/cmd_line.c: Shlomi
	  says this should be more correct CCBUG: 93492

2006-11-20 01:55 +0000 [r606334]  ianw

	* branches/KDE/3.5/kdegames/kgoldrunner/src/kgrgame.h,
	  branches/KDE/3.5/kdegames/kgoldrunner/src/kgrdialog.cpp,
	  branches/KDE/3.5/kdegames/kgoldrunner/src/main.cpp,
	  branches/KDE/3.5/kdegames/kgoldrunner/README,
	  branches/KDE/3.5/kdegames/kgoldrunner/src/kgrdialog.h,
	  branches/KDE/3.5/kdegames/kgoldrunner/kgoldrunner.kdevelop,
	  branches/KDE/3.5/kdegames/kgoldrunner/src/kgoldrunner.cpp,
	  branches/KDE/3.5/kdegames/kgoldrunner/src/kgrfigure.h,
	  branches/KDE/3.5/kdegames/kgoldrunner/kgoldrunner.lsm,
	  branches/KDE/3.5/kdegames/kgoldrunner/AUTHORS,
	  branches/KDE/3.5/kdegames/kgoldrunner/src/kgrgame.cpp,
	  branches/KDE/3.5/kdegames/kgoldrunner/src/kgoldrunner.h: Update
	  author's email address

2006-12-02 22:43 +0000 [r609973]  mueller

	* branches/KDE/3.5/kdegames/kpat/freecell-solver/preset.c:
	  compile++

2006-12-12 18:31 +0000 [r612829]  aacid

	* branches/KDE/3.5/kdegames/kspaceduel/topwidget.cpp: Fix two
	  shortcuts using Ctrl+N, new round is Ctrl+R now BUGS: 128797

2006-12-15 20:28 +0000 [r613952]  pino

	* branches/KDE/3.5/kdegames/knetwalk/src/knetwalk.desktop: Use the
	  standard section.

2006-12-20 18:23 +0000 [r615224]  aacid

	* branches/KDE/3.5/kdegames/knetwalk/src/Makefile.am: Install the
	  desktop file so knetwalk appears on the menus

2006-12-20 20:13 +0000 [r615243]  aacid

	* branches/KDE/3.5/kdegames/knetwalk/src/highscores.cpp: Set the
	  histogram ranges, without them it crashes when trying to add a
	  highscore

2006-12-20 20:33 +0000 [r615248]  aacid

	* branches/KDE/3.5/kdegames/libkdegames/highscore/kexthighscore_internal.cpp:
	  Initialize _bound to true, this fixes the crash for games that
	  don't have histogram ranges set

2007-01-15 09:15 +0000 [r623692]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdewebdev/VERSION,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/VERSION,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: increase version

