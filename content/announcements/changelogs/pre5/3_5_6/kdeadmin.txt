2006-11-03 09:46 +0000 [r601427]  mlaurent

	* branches/KDE/3.5/kdeadmin/knetworkconf/backends/platform.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/network.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/service.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/network-conf.in:
	  Add support for future mandriva-2007.1

2006-11-26 14:27 +0000 [r607968]  lueck

	* branches/KDE/3.5/kdeadmin/doc/ksysv/index.docbook,
	  branches/KDE/3.5/kdeadmin/doc/kpackage/index.docbook,
	  branches/KDE/3.5/kdeadmin/doc/knetworkconf/index.docbook:
	  backport from trunk: fixed typos, wrong guiitems, EBN-error ksysv
	  1 fuzzy, kpackage 3 fuzzy, knetworkconf 4 fuzzy
	  CCMAIL:kde-i18n-doc@kde.org

2006-12-05 15:48 +0000 [r610793]  lueck

	* branches/KDE/3.5/kdeadmin/doc/kpackage/index.docbook,
	  branches/KDE/3.5/kdeadmin/doc/knetworkconf/index.docbook:
	  backport from trunk

2006-12-10 16:24 +0000 [r612247]  jriddell

	* branches/KDE/3.5/kdeadmin/knetworkconf/backends/platform.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/network.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/service.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/network-conf.in:
	  Add support for Kubuntu Feisty, 7.04

2006-12-10 16:39 +0000 [r612254]  jriddell

	* branches/KDE/3.5/kdeadmin/knetworkconf/backends/platform.pl.in:
	  Improve human readable names

2006-12-15 20:25 +0000 [r613950]  pino

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/kcm_knetworkconfmodule.desktop:
	  Fix category name.

2007-01-15 09:15 +0000 [r623692]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdewebdev/VERSION,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/VERSION,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: increase version

