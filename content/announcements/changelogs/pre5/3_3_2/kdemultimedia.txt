Dir: kdemultimedia
----------------------------
new version numbers



Dir: kdemultimedia/akode/arts_plugin
----------------------------
Backport of EOF-fix and direct file-access fallback if mmap fails.



Dir: kdemultimedia/akode/lib
----------------------------
fix compile (gcc 4.0)
----------------------------
Backport of EOF-fix and direct file-access fallback if mmap fails.
----------------------------
Backport mmap crash-fix
CCBUG: 93343



Dir: kdemultimedia/akode/plugins/mpc_decoder
----------------------------
fix compile (gcc 4.0)



Dir: kdemultimedia/akode/plugins/mpc_decoder/mppdec
----------------------------
fix compile (gcc 4.0)



Dir: kdemultimedia/akode/plugins/mpeg_decoder
----------------------------
Backporting error-handling changes, to play more buggy mp3s



Dir: kdemultimedia/akode/plugins/xiph_decoder
----------------------------
fix compile (gcc 4.0)



Dir: kdemultimedia/arts/builder
----------------------------
Backport it++



Dir: kdemultimedia/arts/gui/kde
----------------------------

- Moved the volume/dB calculation into an extra file to reuse it easily for a Volumefader.

- Added a test...

- removed the volume()-properties from the LevelMeter since it isn't responsible for this.

- with this commit the LevelMeter looks exactly the same (ie. shows the same values) as the old Levelmeter in artscontrol ;-)

- more work is needed on the tickmarks...




Dir: kdemultimedia/juk
----------------------------
Backporting:
Make sure that we set the volume to the current level after changing the
audio backend.
----------------------------
Didn't realize that this had gone into the branch as well.  Backporting my changes:

Doing some clean up so that there's a clean split between the visible item
and the current item in the PlaylistCollection.  The current item should
always correspond to the playlist-like methods -- playing things, advancing
tracks, etc.  The visible playlist is useful for things like the tag editor
and k3b exporter.

BUG:91467
----------------------------
increase version
----------------------------
Backport it++ -> ++it
----------------------------
Backporting:

Clean up all of the mess with the visible / current playlist split.  This
should finally make all of the actions and stuff work properly.
----------------------------
Backport fix for 92180 (Select another playlist when current is deleted) to KDE 3.3.

CCBUG:92180



Dir: kdemultimedia/kaudiocreator
----------------------------
fix compile (gcc 4.0)



Dir: kdemultimedia/kioslave/audiocd/kcmaudiocd
----------------------------
fix i18n breakage
----------------------------
backport Bug 89288: When entering the audio CD control module, status is "changed"



Dir: kdemultimedia/kioslave/audiocd/plugins/lame
----------------------------
fix i18n breakage
----------------------------
Backported fixes to awful typos.
----------------------------
Backport: Bug 71267: UI inconsistency from ogg to mp3



Dir: kdemultimedia/kioslave/audiocd/plugins/vorbis
----------------------------
Bug 66551: Wrong range in ogg vorbis quality based encoding settings
(normal)
----------------------------
fix i18n breakage



Dir: kdemultimedia/kioslave/audiocd/plugins/wav
----------------------------
fix compile (gcc 4.0)



Dir: kdemultimedia/kmix
----------------------------
Backporting from version 2.2rc1

- Minor cleanups
- Layout redesign ( AGAIN ), for finally center icons, switches and labels in
  both proper formats, Vertical and horizontal
- Horizontal kmix now proper raise volume from left to right, vanishing the
  weird sensation of lower to up the volume
- Balance is fixed in sense of not relying on master volume directly, just
  affecting the proper channel and respecting top volume
- Tickers now proper set inside non stereo linked sliders
----------------------------
Backport:
- Popup frame patch by Zack Rusin
----------------------------
The "Fix annoyances backport" from Kmix
- Fixed Capture volume. Previous problem always was setting capture volume to 0
  preventing use of any type of capture
- Fixed rec switch detection. Now, just one possible rec switch is enabled
- It's Alive, it's ALIVE.... Esken managed the problem of live change of volume. Pretty cool
- Warning, Coolness factor ahead: New horizontal layout by Esken
- Get rid of annoy jumping mouse pointer
- Change the terrible non usable middle click button on doc for a easy button,
  whichs makes easy to user understando how bring mixer up and down
- Change some of icons.
- Fixed some crashes situations by esken:
Double-deletion of KMix MainWindow lead to crash on exit.
No-deletion of KMix MainWindow lead to crash on next start (or login), as KMix
was still half-alive.
BUG: 89235
BUG: 87348
BUG: 84898
- A patch from Takashi Iwai, for doing better "mixer element-to-icon" matches.



Dir: kdemultimedia/kmix/pics
----------------------------
The "Fix annoyances backport" from Kmix
- Fixed Capture volume. Previous problem always was setting capture volume to 0
  preventing use of any type of capture
- Fixed rec switch detection. Now, just one possible rec switch is enabled
- It's Alive, it's ALIVE.... Esken managed the problem of live change of volume. Pretty cool
- Warning, Coolness factor ahead: New horizontal layout by Esken
- Get rid of annoy jumping mouse pointer
- Change the terrible non usable middle click button on doc for a easy button,
  whichs makes easy to user understando how bring mixer up and down
- Change some of icons.
- Fixed some crashes situations by esken:
Double-deletion of KMix MainWindow lead to crash on exit.
No-deletion of KMix MainWindow lead to crash on next start (or login), as KMix
was still half-alive.
BUG: 89235
BUG: 87348
BUG: 84898
- A patch from Takashi Iwai, for doing better "mixer element-to-icon" matches.



Dir: kdemultimedia/kscd
----------------------------
Backport:
Escape trackname in label, so "&" shows as a "&" and not an underline

Reset revision, category etc when inserting a new
record for which cddb-lookup failed



Dir: kdemultimedia/kscd/libwm
----------------------------
Backport patch by aseigo:
merging some SUSE patches

--

Without this patch, when changing disc, the disc was spinning like crazy,
but after changing track it would work.

CCBUG: 92197



Dir: kdemultimedia/kscd/libwm/include
----------------------------
remove extern declarations of variables that are defined static



Dir: kdemultimedia/noatun/modules/kjofol-skin
----------------------------
Backport string length



Dir: kdemultimedia/noatun/modules/splitplaylist
----------------------------
backport fix to 65834



Dir: kdemultimedia/noatun/modules/winskin
----------------------------
backport



Dir: kdemultimedia/noatun/modules/winskin/vis
----------------------------
backport fix to voodoo crashing bug
