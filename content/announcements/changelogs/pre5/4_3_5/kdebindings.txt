------------------------------------------------------------------------
r1058522 | rdale | 2009-12-04 15:47:36 +0000 (Fri, 04 Dec 2009) | 2 lines

* Skip the QGraphicsSceneEvent copy constructor

------------------------------------------------------------------------
r1058523 | rdale | 2009-12-04 15:51:39 +0000 (Fri, 04 Dec 2009) | 2 lines

* Skip any methods which have QScopedPointers in their args

------------------------------------------------------------------------
r1060690 | rdale | 2009-12-09 16:35:41 +0000 (Wed, 09 Dec 2009) | 6 lines

* Applied a patch from Lubos Lunak to make the qt smoke lib build with Qt 4.6,
thanks for the patch..

CCMAIL: kde-bindings@kde.org
CCMAIL: l.lunak@suse.cz

------------------------------------------------------------------------
