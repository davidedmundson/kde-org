------------------------------------------------------------------------
r1057683 | murrant | 2009-12-03 05:54:10 +0000 (Thu, 03 Dec 2009) | 6 lines

Tell rdesktop to not have window decorations by sending it the -D option so it gets embedded at the proper location in 
krdc.  Works around an issue 
with compiz.

Backport of 1057682

------------------------------------------------------------------------
r1065337 | scripty | 2009-12-23 04:13:55 +0000 (Wed, 23 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1065967 | scripty | 2009-12-25 04:06:24 +0000 (Fri, 25 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1066150 | scripty | 2009-12-26 03:56:32 +0000 (Sat, 26 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1066826 | alexmerry | 2009-12-28 01:26:36 +0000 (Mon, 28 Dec 2009) | 3 lines

Backport r1066810: Don't crash if the raw buffer is the wrong size.


------------------------------------------------------------------------
r1066858 | scripty | 2009-12-28 04:22:19 +0000 (Mon, 28 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1072379 | scripty | 2010-01-10 04:10:52 +0000 (Sun, 10 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1072830 | scripty | 2010-01-11 04:18:48 +0000 (Mon, 11 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1076371 | scripty | 2010-01-18 04:09:15 +0000 (Mon, 18 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
