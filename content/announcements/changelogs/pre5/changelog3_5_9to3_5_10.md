---
aliases:
- ../changelog3_5_9to3_5_10
hidden: true
title: KDE 3.5.10 Changelog
---

<h2>Changes in KDE 3.5.10</h2>
    <h3 id="kdeadmin"><a name="kdeadmin">kdeadmin</a><span class="allsvnchanges"> [ <a href="3_5_10/kdeadmin.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://knetworkconf.sourceforge.net/" name="knetworkconf">KNetworkconf</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Features:</em>
      <ul>
        <li class="feature">Add support for Debian Lenny 5.0. See SVN commit <a href="http://websvn.kde.org/?rev=824461&amp;view=rev">824461</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="3_5_10/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kio">kio</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix the authentication-dialogue being put behind the application's window. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=121803">121803</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=849216&amp;view=rev">849216</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="3_5_10/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://konsole.kde.org/" name="konsole">Konsole</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix zmodem download dialog not starting. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=145177">145177</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=846506&amp;view=rev">846506</a>. </li>
      </ul>
      </div>
      <h4><a name="kicker">Kicker</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Improvements:</em>
      <ul>
          <li class="improvement">Add a little margin around big applet icons. See SVN commit <a href="http://websvn.kde.org/?rev=839328&amp;view=rev">839328</a>. </li>
          <li class="improvement">Use a themed simple arrow button in applet handles, panel scroll buttons and system tray. See SVN commits <a href="http://websvn.kde.org/?rev=839328&amp;view=rev">839328</a> and <a href="http://websvn.kde.org/?rev=839825&amp;view=rev">839825</a>. </li>
          <li class="improvement">The applet handle margin is now just one pixel. See SVN commit <a href="http://websvn.kde.org/?rev=839347&amp;view=rev">839347</a>. </li>
          <li class="improvement">Kicker now automatically restarts when the kde widget style changes. See SVN commit <a href="http://websvn.kde.org/?rev=839366&amp;view=rev">839366</a>. </li>
          <li class="improvement">The taskbar background pixmap is not partly copied in each applet container anymore. See SVN commit <a href="http://websvn.kde.org/?rev=839721&amp;view=rev">839721</a>. </li>
          <li class="improvement">The background of panel buttons and applets is now refreshed during a move. See SVN commit <a href="http://websvn.kde.org/?rev=839721&amp;view=rev">839721</a>. </li>
          <li class="improvement">Use "simple" buttons like panel buttons in the quicklauncher applet, the lockout applet and the naughty applet. See SVN commits <a href="http://websvn.kde.org/?rev=839752&amp;view=rev">839752</a>, <a href="http://websvn.kde.org/?rev=848270&amp;view=rev">848270</a> and <a href="http://websvn.kde.org/?rev=848804&amp;view=rev">848804</a>. </li>
          <li class="improvement">Fix the run applet whose minimum width was too low. Now it does not shrink smaller than its label. See SVN commit <a href="http://websvn.kde.org/?rev=839811&amp;view=rev">839811</a>. </li>
          <li class="improvement">Make the menu applet always immutable, so that it cannot be removed. See SVN commit <a href="http://websvn.kde.org/?rev=844919&amp;view=rev">844919</a>. </li>
          <li class="improvement">Make the taskbar not flicker anymore on desktop changes. See SVN commit <a href="http://websvn.kde.org/?rev=846570&amp;view=rev">846570</a>. </li>
          <li class="improvement">When a panel has a background image that is colorized according to the global palette, its border is colored the same way. See SVN commit <a href="http://websvn.kde.org/?rev=846700&amp;view=rev">846700</a>. </li>
          <li class="improvement">When moving a panel, the frame is now drawn with top-level unmanaged windows, using the title bar color. See SVN commit <a href="http://websvn.kde.org/?rev=847603&amp;view=rev">847603</a>. </li>
          <li class="improvement">When the mouse enters a taskbar button in transparent mode, a shadow is drawn instead of the standard button background. See SVN commit <a href="http://websvn.kde.org/?rev=848322&amp;view=rev">848322</a>. </li>
          <li class="improvement">In the pager applet, a shadow is drawn when the mouse enters a pager button, unless in elegant mode. See SVN commit <a href="http://websvn.kde.org/?rev=848323&amp;view=rev">848323</a>. </li>
          <li class="improvement">Pager buttons do not flicker anymore. See SVN commit <a href="http://websvn.kde.org/?rev=848323&amp;view=rev">848323</a>. </li>
          <li class="improvement">When the panel is locked, the "configure panel" menu entry is hidden only if it has been disabled by Kiosk. See SVN commit <a href="http://websvn.kde.org/?rev=848803&amp;view=rev">848803</a>. </li>
          <li class="improvement">In transparent mode, do not draw themed applet handle, but a transparent rectangle shadow. See SVN commit <a href="http://websvn.kde.org/?rev=848806&amp;view=rev">848806</a>. </li>
          <li class="improvement">Remove the delayed layout when using the mouse wheel on the clock applet. See SVN commit <a href="http://websvn.kde.org/?rev=848971&amp;view=rev">848971</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix the kicker add applet dialog layout. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=108602">108602</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=839328&amp;view=rev">839328</a>. </li>
        <li class="bugfix ">Fix the anti-aliased clock applet drawing routine. See SVN commit <a href="http://websvn.kde.org/?rev=839347&amp;view=rev">839347</a>. </li>
        <li class="bugfix ">When the global palette is changed, the clock applet date does not lose its transparency anymore. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=164139">164139</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=839385&amp;view=rev">839385</a>. </li>
        <li class="bugfix ">Some buttons could not be dragged from the applet dialog. See SVN commit <a href="http://websvn.kde.org/?rev=839571&amp;view=rev">839571</a>. </li>
        <li class="bugfix ">Fix the behaviour of drag &amp; drop on scrollable panels. See SVN commit <a href="http://websvn.kde.org/?rev=839659&amp;view=rev">839659</a>. </li>
        <li class="bugfix ">Fix transparency management in panels, taskbar, media applet et minipager. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=118617">118617</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=144170">144170</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=839721&amp;view=rev">839721</a>. </li>
        <li class="bugfix ">The minipager now draws the desktop names with a shadow in transparent mode. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133166">133166</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=839721&amp;view=rev">839721</a>. </li>
        <li class="bugfix ">The drag &amp; drop indicator background is now refreshed correctly. See SVN commit <a href="http://websvn.kde.org/?rev=839721&amp;view=rev">839721</a>. </li>
        <li class="bugfix ">The quicklauncher applet transparency was fixed. See SVN commit <a href="http://websvn.kde.org/?rev=839752&amp;view=rev">839752</a>. </li>
        <li class="bugfix ">The quicklauncher layout algorithm was fixed. See SVN commit <a href="http://websvn.kde.org/?rev=839752&amp;view=rev">839752</a>. </li>
        <li class="bugfix ">Fix the system tray applet transparency management. See SVN commit <a href="http://websvn.kde.org/?rev=839825&amp;view=rev">839825</a>. </li>
        <li class="bugfix ">Fix the system tray layout algorithm. See SVN commit <a href="http://websvn.kde.org/?rev=844828&amp;view=rev">844828</a>. </li>
        <li class="bugfix ">Fix the background transparency of the menu applet. See SVN commit <a href="http://websvn.kde.org/?rev=844919&amp;view=rev">844919</a>. </li>
        <li class="bugfix ">Fix the frame style in the clock applet. It was incorrectly set. See SVN commit <a href="http://websvn.kde.org/?rev=846692&amp;view=rev">846692</a>. </li>
        <li class="bugfix ">The taskbar viewport palette is correctly refreshed when the global color settings are changed. See SVN commit <a href="http://websvn.kde.org/?rev=848591&amp;view=rev">848591</a>. </li>
        <li class="bugfix ">The clock applet finally answers correctly to global palette changes. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=98398">98398</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=848935&amp;view=rev">848935</a>. </li>
        <li class="bugfix ">Correctly initialize the timezone foreground color in the clock applet. See SVN commit <a href="http://websvn.kde.org/?rev=848971&amp;view=rev">848971</a>. </li>
      </ul>
      </div>
      <h4><a name="kate">Kate</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Kate is finally restored on the correct desktop. The opened files progress window displayed at session restore is now put on the current desktop now, so that the user can see it. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=117618">117618</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=846328&amp;view=rev">846328</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeedu"><a name="kdeedu">kdeedu</a><span class="allsvnchanges"> [ <a href="3_5_10/kdeedu.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://edu.kde.org/ktouch/" name="ktouch">ktouch</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Re-add Norvegian nb keyboard file fixed. See SVN commit <a href="http://websvn.kde.org/?rev=825873&amp;view=rev">825873</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="3_5_10/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://kpdf.kde.org" name="kpdf">KPDF</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crash on some broken files. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=157777">157777</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=774669&amp;view=rev">774669</a>. </li>
        <li class="bugfix crash">Fix crash due to a off by one memory write. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=158387">158387</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=158549">158549</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=785716&amp;view=rev">785716</a>. </li>
        <li class="bugfix crash">Fix crash on some PDF using JBIG2 streams. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=164568">164568</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=822788&amp;view=rev">822788</a>. </li>
        <li class="bugfix crash">Fix crash on malformed documents while showing the Properties dialog. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=166145">166145</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=836688&amp;view=rev">836688</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdepim"><a name="kdepim">kdepim</a><span class="allsvnchanges"> [ <a href="3_5_10/kdepim.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://www.astrojar.org.uk/kalarm" name="kalarm">KAlarm</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Improvements:</em>
      <ul>
          <li class="improvement">In New From Template menu, show list of template names in sorted order. See SVN commit <a href="http://websvn.kde.org/?rev=801392&amp;view=rev">801392</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix Find not working with a new search text after a failed search. See SVN commit <a href="http://websvn.kde.org/?rev=804054&amp;view=rev">804054</a>. </li>
        <li class="bugfix ">Fix recurrence count being lost when using alarm templates. See SVN commit <a href="http://websvn.kde.org/?rev=800279&amp;view=rev">800279</a>. </li>
        <li class="bugfix ">Display default font correctly in alarm edit dialog sample text. See SVN commit <a href="http://websvn.kde.org/?rev=844773&amp;view=rev">844773</a>. </li>
        <li class="bugfix ">Fix recurrence count shown in alarm edit dialog once alarm has triggered. See SVN commit <a href="http://websvn.kde.org/?rev=801402&amp;view=rev">801402</a>. </li>
        <li class="bugfix ">Show background color selector for file display alarms. See SVN commit <a href="http://websvn.kde.org/?rev=848952&amp;view=rev">848952</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdetoys"><a name="kdetoys">kdetoys</a><span class="allsvnchanges"> [ <a href="3_5_10/kdetoys.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kweather">kweather</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Add Belarus stations. See SVN commit <a href="http://websvn.kde.org/?rev=825901&amp;view=rev">825901</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="3_5_10/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/kgpg/" name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix potential crashes due to wrong object deletion. See SVN commit <a href="http://websvn.kde.org/?rev=818283&amp;view=rev">818283</a>. </li>
        <li class="bugfix ">Fix memleak when loading key images in key info window. See SVN commit <a href="http://websvn.kde.org/?rev=818284&amp;view=rev">818284</a>. </li>
        <li class="bugfix ">Fix exporting multiple keys to keyserver at once. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=154528">154528</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=822717&amp;view=rev">822717</a>. </li>
        <li class="bugfix ">Fix problems when communicating with Onak keyservers. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=139774">139774</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=830494&amp;view=rev">830494</a>. </li>
      </ul>
      </div>
    </div>

    <h3 id="arts"><a name="arts">arts</a><span class="allsvnchanges"> [ <a href="3_5_10/arts.txt">all SVN changes</a> ]</span></h3>
    <h3 id="kdeaddons"><a name="kdeaddons">kdeaddons</a><span class="allsvnchanges"> [ <a href="3_5_10/kdeaddons.txt">all SVN changes</a> ]</span></h3>
    <h3 id="kdeartwork"><a name="kdeartwork">kdeartwork</a><span class="allsvnchanges"> [ <a href="3_5_10/kdeartwork.txt">all SVN changes</a> ]</span></h3>
    <h3 id="kdebindings"><a name="kdebindings">kdebindings</a><span class="allsvnchanges"> [ <a href="3_5_10/kdebindings.txt">all SVN changes</a> ]</span></h3>
    <h3 id="kdegames"><a name="kdegames">kdegames</a><span class="allsvnchanges"> [ <a href="3_5_10/kdegames.txt">all SVN changes</a> ]</span></h3>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="3_5_10/kdelibs.txt">all SVN changes</a> ]</span></h3>
    <h3 id="kdenetwork"><a name="kdenetwork">kdenetwork</a><span class="allsvnchanges"> [ <a href="3_5_10/kdenetwork.txt">all SVN changes</a> ]</span></h3>
    <h3 id="kdesdk"><a name="kdesdk">kdesdk</a><span class="allsvnchanges"> [ <a href="3_5_10/kdesdk.txt">all SVN changes</a> ]</span></h3>
    <h3 id="kdewebdev"><a name="kdewebdev">kdewebdev</a><span class="allsvnchanges"> [ <a href="3_5_10/kdewebdev.txt">all SVN changes</a> ]</span></h3>
    <h3 id="kdeaccessibility"><a name="kdeaccessibility">kdeaccessibility</a><span class="allsvnchanges"> [ <a href="3_5_10/kdeaccessibility.txt">all SVN changes</a> ]</span></h3>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="3_5_10/kdebase.txt">all SVN changes</a> ]</span></h3>
    <h3 id="kdemultimedia"><a name="kdemultimedia">kdemultimedia</a><span class="allsvnchanges"> [ <a href="3_5_10/kdemultimedia.txt">all SVN changes</a> ]</span></h3>
    <h3 id="kdevelop"><a name="kdevelop">kdevelop</a><span class="allsvnchanges"> [ <a href="3_5_10/kdevelop.txt">all SVN changes</a> ]</span></h3>