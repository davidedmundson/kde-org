---
aliases:
- ../changelog4_1_2to4_1_3
hidden: true
title: KDE 4.1.3 Changelog
---

<h2>Changes in KDE 4.1.3</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_1_3/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kdeui">kdeui</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Make the "clear" action undo-aware. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=172031">172031</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=866969&amp;view=rev">866969</a>. </li>
      </ul>
      </div>
      <h4><a name="khtml">khtml</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Optimizations:</em><ul>
        <li class="optimize">Significantly improve the efficiency of how we paint images when full-page scaling, by doing scaling directly with imload.  See SVN commit <a href="http://websvn.kde.org/?rev=867171&amp;view=rev">867171</a>. </li>
        <li class="optimize">Optimize canvas element - save calls to non-inline QImage::width(). About 0.25% CPU cycles less http://www.nihilogic.dk/labs/javascript_canvas_fractals/ See SVN commit <a href="http://websvn.kde.org/?rev=867600&amp;view=rev">867600</a>. </li>
        <li class="optimize">Optimize canvas element - set color components directly on the scan line rather than reading and setting QColor values with pixel() and setPixel(). Saved about 4-6% of cpu cycles on a heavy duty example. See SVN commit <a href="http://websvn.kde.org/?rev=867730&amp;view=rev">867730</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Support position:fixed on the 'body' element even in quirk mode. See SVN commit <a href="http://websvn.kde.org/?rev=864535&amp;view=rev">864535</a>. </li>
        <li class="bugfix ">Fix offsetTop/Left/Parent properties to be more in conformance with draft CSSOM View module specification. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=170091">170091</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=170055">170055</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=864536&amp;view=rev">864536</a>. </li>
        <li class="bugfix ">Fixed potential out-of-bounds access in editing. See SVN commit <a href="http://websvn.kde.org/?rev=865762&amp;view=rev">865762</a>. </li>
        <li class="bugfix ">Support focus/blur on all elements (likely mozilla extension), used by Youtube quicklist Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=169988">169988</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=867176&amp;view=rev">867176</a>. </li>
        <li class="bugfix ">Stop combo boxes not updating themselves anymore after first use. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=164709">164709</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=868031&amp;view=rev">868031</a>. </li>
        <li class="bugfix ">Make sure the hash is %-decoded; fixes the W3C slide tool. See SVN commit <a href="http://websvn.kde.org/?rev=868614&amp;view=rev">868614</a>. </li>
        <li class="bugfix ">Fake window.scrollbars a bit better. Page checking for a .visible property will always get undefined (false) for now Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=172334">172334</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=869061&amp;view=rev">869061</a>. </li>
        <li class="bugfix ">Fix for the google maps. Honour CSSOM View Module draft specification for Offset* properties by returning null for display:none objects Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=171896">171896</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=153745">153745</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=869314&amp;view=rev">869314</a>. </li>
        <li class="bugfix ">Fix onchange event on linedits/textareas. See SVN commit <a href="http://websvn.kde.org/?rev=873038&amp;view=rev">873038</a>. </li>
        <li class="bugfix "> Rework how we do onchange for input/checkboxes, unbreaking it and simplifying things. As we only want to fire it in response to user events, this just does it in defaultEventHandler. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=148118">148118</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=165607">165607</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=170451">170451</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=873059&amp;view=rev">873059</a>. </li>
        <li class="bugfix ">Use computedStyle() without directly asking the render object. There are still some cases where it is a hard requirement. See SVN commit <a href="http://websvn.kde.org/?rev=871476&amp;view=rev">871476</a>. </li>
        <li class="bugfix ">Fixed disabled &lt;button&gt; dispatching events and getting pressed-down. Probably want to tweak the CSS some more to grey out the font, though. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=170159">170159</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=873068&amp;view=rev">873068</a>. </li>
        <li class="bugfix ">As we do our own event transporting we have to do our own bubbling for nested QWidgets embedded as from controls. Fixes the click on the clear button for the file input element. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=153035">153035</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=873119&amp;view=rev">873119</a>. </li>
        <li class="bugfix ">Set keyboard focus on the KLineEdit of file input elements when clicking on it. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=153035">153035</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=873152&amp;view=rev">873152</a>. </li>
        <li class="bugfix ">Deliver key events to a widget's focus widget. This might be different than the main widget in the case of KUrlRequester (&lt;input type="file"&gt;). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=153035">153035</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=873196&amp;view=rev">873196</a>. </li>
        <li class="bugfix ">Fixing width, height and padding unit and values for visible as well as invisible elements. Fixes some jquery test suite failures. See SVN commit <a href="http://websvn.kde.org/?rev=873497&amp;view=rev">873497</a>. </li>
        <li class="bugfix ">Fixed margin values on invisible elements. Also fixed computed value of percentage value. See SVN commit <a href="http://websvn.kde.org/?rev=873514&amp;view=rev">873514</a>. </li>
        <li class="bugfix ">DocumentImpl::unsubmittedChanges actually relied on modified things having 'M' at the end of their state serialization, which broke notification of changes. Fix this by adding an appropriate abstraction. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=173159">173159</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=873587&amp;view=rev">873587</a>. </li>
        <li class="bugfix crash">backport: don't crash when the khtmlpart is deleted while a print dialog is open Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=144940">144940</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=873635&amp;view=rev">873635</a>. </li>
        <li class="bugfix ">Track the event target for all mouse buttons. Fixes mmb pasting in input type=text form widgets. See SVN commit <a href="http://websvn.kde.org/?rev=873657&amp;view=rev">873657</a>. </li>
        <li class="bugfix ">Fix static positioning of positioned inline replaced elements. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=172487">172487</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=873676&amp;view=rev">873676</a>. </li>
        <li class="bugfix ">Fix determination of painted surface for containers with text-only children. See SVN commit <a href="http://websvn.kde.org/?rev=873677&amp;view=rev">873677</a>. </li>
        <li class="bugfix ">Partly fix the scroll event dispatching, so that window.onscroll works (fix /.'s floating comment box). See SVN commit <a href="http://websvn.kde.org/?rev=873679&amp;view=rev">873679</a>. </li>
        <li class="bugfix ">Do not skip empty CSS rules - they must appear in the cssRules array. digg.com, for instance, needs this to perform some unspeakable CSS hacks. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=170411">170411</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=165734">165734</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=873668&amp;view=rev">873668</a>. </li>
        <li class="bugfix ">Fix incomplete repaints happening from time to time when jumping early during page load. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=166413">166413</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=873671&amp;view=rev">873671</a>. </li>
        <li class="bugfix ">Various RTL layout application (-reverse cmdline option) fixes: implement RTL scrollbars on CSS containers ; fix iframes scrollbars in RTL mode ; fix wrong direction when scrolling horizontally ; fix grey block on left of view. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=172258">172258</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=170679">170679</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=873671&amp;view=rev">873671</a>. </li>
        <li class="bugfix ">Various HTML Frames fixes : make frame resizing dynamic ; eliminate most frame flicker by cleaning up RenderWidget's resizing code. See SVN commit <a href="http://websvn.kde.org/?rev=873673&amp;view=rev">873673</a>. </li>
        <li class="bugfix ">Bring our preferred min max width calculation algorithm for floats closer to Gecko 1.9+ and WebCore. As a result the interoperability with Gecko 1.9+ is stunning, but somewhat worse with IE/Opera. Fix e.g extremetech.com articles layout. See SVN commit <a href="http://websvn.kde.org/?rev=873680&amp;view=rev">873680</a>. </li>
        <li class="bugfix ">Fix crash when fixed-layout tables specify all % width as 0 (affects new ebay myebay version). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=172557">172557</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=874965&amp;view=rev">874965</a>. </li>
        <li class="bugfix ">Don't return -1 for location.port when no port is set (regression due to change in KUrl semantics). Fixes recent quotes on google finance page. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=172371">172371</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=875314&amp;view=rev">875314</a>. </li>
        <li class="bugfix ">Fix how  we find existing frames for the purpose of form target= submission. This fixes file upload for drupal 6/jquery.form --- its submit  was incorrectly blocked as a popup attempt. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=102044">102044</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=172073">172073</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=875819&amp;view=rev">875819</a>. </li>
        <li class="bugfix ">Fix signature mis-match here, unbreaks loading of accessibility/user stylesheet, and also crash on exit when one is set. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=167268">167268</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=164796">164796</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=876287&amp;view=rev">876287</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_1_3/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
        <h4><a href="http://dolphin.kde.org" name="dolphin">Dolphin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crash because of an URL redirection. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=155591">155591</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=875564&amp;view=rev">875564</a>. </li>
        <li class="bugfix crash">Fix crash when showing previews of images having an extremly large aspect ratio. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=170547">170547</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=871813&amp;view=rev">871813</a>. </li>
      </ul>
      </div>
        <h4><a name="kwin">KWin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix drawing problems in compositing mode when KRunner changes size. See SVN commit <a href="http://websvn.kde.org/?rev=866586&amp;view=rev">866586</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegames"><a name="kdegames">kdegames</a><span class="modulehomepage"> [ <a href="http://games.kde.org">homepage</a> ] </span><span class="allsvnchanges"> [ <a href="4_1_3/kdegames.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://games.kde.org/game.php?game=ktuberling" name="ktuberling">KTuberling</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix icon not having transparent background. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=171923">171923</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=868579&amp;view=rev">868579</a>. </li>
      </ul>
      </div>
      <h4><a href="http://games.kde.org/game.php?game=kshisen" name="kshisen">KShisen</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Game -&gt; Restart Game resets the game timer now. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=144476">144476</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=875419&amp;view=rev">875419</a>. </li>
        <li class="bugfix ">Clear the redo history as well when resetting the current game. See SVN commit <a href="http://websvn.kde.org/?rev=875464&amp;view=rev">875464</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_1_3/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://okular.kde.org" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">When saving a selection of the document as image, assume the wanted format is PNG if not recognized from the file name. See SVN commit <a href="http://websvn.kde.org/?rev=866615&amp;view=rev">866615</a>. </li>
        <li class="bugfix ">Make the memory handling work for real with &gt;= 4 GB of RAM. See SVN commit <a href="http://websvn.kde.org/?rev=867873&amp;view=rev">867873</a>. </li>
        <li class="bugfix ">Select all the text when opening the find bar. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=172585">172585</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=869969&amp;view=rev">869969</a>. </li>
        <li class="bugfix ">Fix drawing glitches with note, stamp, and rect/ellipse annotations. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=160396">160396</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=870165&amp;view=rev">870165</a>. </li>
        <li class="bugfix ">Safer toolbar editing. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=168528">168528</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=171186">171186</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=870503&amp;view=rev">870503</a>. </li>
        <li class="bugfix ">Allow to configure the shortcuts (and not only that) for the actions of the "View Mode" menu. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=172661">172661</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=870511&amp;view=rev">870511</a>. </li>
        <li class="bugfix ">Do not ignore (white)spaces in the search query when searching within the text of a page. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=172244">172244</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=873278&amp;view=rev">873278</a>. </li>
        <li class="bugfix ">DjVu backend: be sure to print correctly. See SVN commit <a href="http://websvn.kde.org/?rev=875560&amp;view=rev">875560</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdepim"><a name="kdepim">kdepim</a><span class="modulehomepage"> [ <a href="http://pim.kde.org/">homepage</a> ] </span><span class="allsvnchanges"> [ <a href="4_1_3/kdepim.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://www.astrojar.org.uk/kalarm" name="kalarm">KAlarm</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix very high CPU usage by KAlarm when there are alarms with sub-repetitions, or deferrals, with periods greater than 1 week. (Fix is in kdepimlibs.) Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=172246">172246</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=865199&amp;view=rev">865199</a> and <a href="http://websvn.kde.org/?rev=865203&amp;view=rev">865203</a>. </li>
        <li class="bugfix ">Fix alarms not triggering correctly after laptop wakes from hibernation. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=173034">173034</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=874976&amp;view=rev">874976</a>. </li>
        <li class="bugfix ">Fix inability to change or cancel alarm deferral times. See SVN commit <a href="http://websvn.kde.org/?rev=870668&amp;view=rev">870668</a>. </li>
        <li class="bugfix ">Fix addition and deletion of alarms to KOrganizer. See SVN commit <a href="http://websvn.kde.org/?rev=874069&amp;view=rev">874069</a>. </li>
      </ul>
      </div>
      <h4><a href="http://kontact.kde.org/kmail/" name="kmail">KMail</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Improve detection of when a line in the mail text should be rendered right-to-left. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=134036">134036</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=866979&amp;view=rev">866979</a>. </li>
        <li class="bugfix ">Fix KMail sending invalid charsets like "ISO 8859-xx" instead of "ISO-8859-xx". Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=171947">171947</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=870426&amp;view=rev">870426</a>. </li>
        <li class="bugfix ">Fix another crash on exit when using IMAP. See SVN commit <a href="http://websvn.kde.org/?rev=875811&amp;view=rev">875811</a>. </li>
        <li class="bugfix ">Fix a crash when opening a just added attachment that got quoted-printable encoded and contained a CRLF linefeed. See SVN commit <a href="http://websvn.kde.org/?rev=877115&amp;view=rev">877115</a>. </li>
        <li class="bugfix ">Give the "Keep replies in this folder" setting precedence over the sent mail folder configured in the identity. See SVN commit <a href="http://websvn.kde.org/?rev=877116&amp;view=rev">877116</a>. </li>
        <li class="bugfix ">Fix possible random mail duplication with disconnected IMAP. See SVN commit <a href="http://websvn.kde.org/?rev=877938&amp;view=rev">877938</a>. </li>
      </ul>
      </div>
      <h4><a href="http://kontact.kde.org/korganizer/" name="korganizer">KOrganizer</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Make sure the next/previous month buttons in the date navigator work as expected. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=122333">122333</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=870427&amp;view=rev">870427</a>. </li>
      </ul>
      </div>      
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="modulehomepage"> [ <a href="http://utils.kde.org">homepage</a> ] </span><span class="allsvnchanges"> [ <a href="4_1_3/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/okteta/" name="okteta">Okteta</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Value fields in the Decoding table now fit max value length. See SVN commit <a href="http://websvn.kde.org/?rev=172324&amp;view=rev">172324</a>. </li>
      </ul>
      </div>
      <h4><a href="http://utils.kde.org/projects/kgpg/" name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix regular expression for domain names. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=172850">172850</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=871887&amp;view=rev">871887</a> and <a href="http://websvn.kde.org/?rev=871927&amp;view=rev">871927</a>. </li>
        <li class="bugfix ">Save changes after editing a key group. See SVN commit <a href="http://websvn.kde.org/?rev=871927&amp;view=rev">871927</a>. </li>
        <li class="bugfix ">Fix sorting by trust showing the secret pairs at the wrong end of the list. See SVN commit <a href="http://websvn.kde.org/?rev=874984&amp;view=rev">874984</a>. </li>
        <li class="bugfix crash">Fix crash when trying to show key properties of a group member. See SVN commit <a href="http://websvn.kde.org/?rev=876356&amp;view=rev">876356</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdenetwork"><a name="kdenetwork">kdenetwork</a><span class="allsvnchanges"> [ <a href="4_1_3/kdenetwork.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://kopete.kde.org" name="kopete">Kopete</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Don't add duplicate contacts. See SVN commits <a href="http://websvn.kde.org/?rev=865118&amp;view=rev">865118</a> and <a href="http://websvn.kde.org/?rev=865119&amp;view=rev">865119</a>. </li>
        <li class="bugfix crash">Fix crash during KDE logout. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=172011">172011</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=869875&amp;view=rev">869875</a>. </li>
        <li class="bugfix crash">Fix occasional crash when accounts change status to online or offline. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=172985">172985</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=872280&amp;view=rev">872280</a>. </li>
        <li class="bugfix ">Speed up parsing of message with many emoticons. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=172485">172485</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=872808&amp;view=rev">872808</a>. </li>
        <li class="bugfix crash">Fix occasional ICQ/AIM login crash. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=172997">172997</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=872809&amp;view=rev">872809</a>. </li>
        <li class="bugfix crash">Fix crash if Jabber has contact with empty Jabber ID. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=173136">173136</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=875552&amp;view=rev">875552</a>. </li>
        <li class="bugfix crash">Fix crash when any device was removed while webcam is present. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=172997">172997</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=872809&amp;view=rev">872809</a>. </li>
        <li class="bugfix ">Don't add new line at the end of ICQ/AIM richtext message. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146380">146380</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=876034&amp;view=rev">876034</a>. </li>
      </ul>
      </div>
    </div>