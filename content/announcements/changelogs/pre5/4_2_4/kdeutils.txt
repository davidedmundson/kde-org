------------------------------------------------------------------------
r962821 | dakon | 2009-05-03 12:37:05 +0000 (Sun, 03 May 2009) | 1 line

fix memleak
------------------------------------------------------------------------
r962827 | dakon | 2009-05-03 12:39:29 +0000 (Sun, 03 May 2009) | 4 lines

remove i18n string concatenations

They are replaced by the proper i18nc() call that was already used at other places, so this at the end only removes a string.

------------------------------------------------------------------------
r962835 | dakon | 2009-05-03 12:41:38 +0000 (Sun, 03 May 2009) | 1 line

fix compile
------------------------------------------------------------------------
r962851 | dakon | 2009-05-03 12:45:30 +0000 (Sun, 03 May 2009) | 1 line

fix layout
------------------------------------------------------------------------
r963164 | mboquien | 2009-05-04 01:07:37 +0000 (Mon, 04 May 2009) | 6 lines

Backport commit 963162

asinh was calculated instead of sinh.

CCBUG:191490

------------------------------------------------------------------------
r966491 | scripty | 2009-05-11 09:06:59 +0000 (Mon, 11 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r969880 | scripty | 2009-05-19 07:35:43 +0000 (Tue, 19 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r970831 | mboquien | 2009-05-20 22:19:10 +0000 (Wed, 20 May 2009) | 4 lines

Backport r970340.

CCBUG:190098

------------------------------------------------------------------------
r972457 | rkcosta | 2009-05-24 23:42:15 +0000 (Sun, 24 May 2009) | 9 lines

Backport commit 972450.

Remove the single folder notice label.
Since it had wordWrap set to true, it was breaking
the layout and allowing the widget to be resized
to (0,0).

CCBUG: 193868

------------------------------------------------------------------------
r972567 | pino | 2009-05-25 10:01:23 +0000 (Mon, 25 May 2009) | 2 lines

adapt to Pi being a function now (and no more a static constant)

------------------------------------------------------------------------
r973317 | dakon | 2009-05-26 20:26:07 +0000 (Tue, 26 May 2009) | 10 lines

Use --fixed-list-mode for GnuPG key output.

This is the default since GPG 2.0.10 and in general makes the life more easy.
Should have been used forever. Without this KGpg will not show useful output 
when someone uses recent GPG.

Backport of r952350 from trunk

BUG:188473

------------------------------------------------------------------------
