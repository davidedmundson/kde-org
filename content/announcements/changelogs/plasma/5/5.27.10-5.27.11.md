---
title: Plasma 5.27.11 complete changelog
version: 5.27.11
hidden: true
plasma: true
type: fulllog
---

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Flatpak: Fix up prompt to delete uninstalled app's data. [Commit.](http://commits.kde.org/discover/a84acbe8b5b87d1a6e9071ce61426d9622311f0c) Fixes bug [#478704](https://bugs.kde.org/478704)
+ Gitlab-ci: Port to new CI template. [Commit.](http://commits.kde.org/discover/a419da7955319874f951ba083de76b2787206385) 
+ Rpm-ostree: Add more comments. [Commit.](http://commits.kde.org/discover/1380c4aba1da53e5c7bff21e1cbe45d4c2ebb323) 
+ Rpm-ostree: Fix ostree container support. [Commit.](http://commits.kde.org/discover/8e9f3d248c94dcab0504e592d6540d33826032e8) 
+ Fix SteamOSBackend isValid check. [Commit.](http://commits.kde.org/discover/8b13c9618a137adb2deb759c1fcd8e623c0ba7b3) 
{{< /details >}}

{{< details title="KDE GTK Config" href="https://commits.kde.org/kde-gtk-config" >}}
+ Round x11GlobalScaleFactor instead of flooring it. [Commit.](http://commits.kde.org/kde-gtk-config/6b3865a72baff3932fbe89ef0c44ff1b9bb4cd10) Fixes bug [#480828](https://bugs.kde.org/480828)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Tabbox: match Shift+Backtab against Shift+Tab. [Commit.](http://commits.kde.org/kwin/c8b7717953a28ac43f1fa5e69c9eceae4ea7e868) Fixes bug [#438991](https://bugs.kde.org/438991)
+ Backends/drm: Undo fade-out effect upon unsuccessful DPMS Off. [Commit.](http://commits.kde.org/kwin/26d36b9ac2414adfba344fa16a268976adb7728d) Fixes bug [#477916](https://bugs.kde.org/477916). See bug [#481520](https://bugs.kde.org/481520)
+ Autotests/integration/outputchanges: add geometry restore test. [Commit.](http://commits.kde.org/kwin/d1850d0db0439ec057c26ee07e36b7693c03d77a) 
+ Placementtracker: save geometry restores more explicitly. [Commit.](http://commits.kde.org/kwin/99e5010f443836432bd43d406a3221c1e9d7c197) 
+ Placementtracker: don't set geometry to geometry restores. [Commit.](http://commits.kde.org/kwin/437fba0f44eca82105a0c63df6d2521a1eb059c6) Fixes bug [#473602](https://bugs.kde.org/473602)
+ Plugins/screencast: set frame timer to one shot. [Commit.](http://commits.kde.org/kwin/3544df1abaf8c5b1d34158890f3122071c7b01b1) Fixes bug [#469777](https://bugs.kde.org/469777)
+ Wayland/textinput_v2: copy the data instead of assuming ownership. [Commit.](http://commits.kde.org/kwin/2f0faf45858d6dbafa00875e584bc1f6b1861b3a) Fixes bug [#481239](https://bugs.kde.org/481239)
+ Window: use normal keyboard modifiers for triggering custom tiling. [Commit.](http://commits.kde.org/kwin/fa15cb67d2b21001458aba9e4bb9ef39d5e4c141) Fixes bug [#465858](https://bugs.kde.org/465858)
+ Input_event: remove modifiersRelevantForTabBox. [Commit.](http://commits.kde.org/kwin/428040bbd17a87df49816d51e6adfe54bdc60ced) 
+ Xkb: caps lock is not shift lock. [Commit.](http://commits.kde.org/kwin/f0aeb21f2d8e338e26b622f329b793290c747e14) 
+ Backends/drm: commit m_next state properly. [Commit.](http://commits.kde.org/kwin/5b7183c77a2be9c3c26f9f209e783cb1a34118e0) Fixes bug [#477451](https://bugs.kde.org/477451)
+ Address licence of plugin.h. [Commit.](http://commits.kde.org/kwin/f76bd7a70eb802c29ed68cc714531ee5cfd67b3b) 
+ Xdgshellwindow: make maxSize always >= minSize by enforcing the same minimum. [Commit.](http://commits.kde.org/kwin/0d59f48fbf2cbbcc71556f0ce005d939c604f2c4) Fixes bug [#478269](https://bugs.kde.org/478269)
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Handle the translations for systemstats. [Commit.](http://commits.kde.org/libksysguard/20bab8218b736f30443ad21a7572bb9927c7f9b1) 
{{< /details >}}

{{< details title="Plasma Browser Integration" href="https://commits.kde.org/plasma-browser-integration" >}}
+ 🍒🍒Download Job: Truncate excessively long URLs. [Commit.](http://commits.kde.org/plasma-browser-integration/84e738f1abb2dc1cc5f6d1541dc509951dd719b7) 
+ MPrisPlugin: Limit string and URL lengths in metadata. [Commit.](http://commits.kde.org/plasma-browser-integration/12cdcab7c3945022dba0a0b5de5a5b4d8ab132b6) 
+ MPrisPlugin: Rate-limit property changes. [Commit.](http://commits.kde.org/plasma-browser-integration/11779544bf44d1ab55c87fbe2b2c8e2d586c4112) 
{{< /details >}}

{{< details title="Plasma Systemmonitor" href="https://commits.kde.org/plasma-systemmonitor" >}}
+ Workaround QTBUG-84858. [Commit.](http://commits.kde.org/plasma-systemmonitor/7d0cd220ff76bf001189fc32c466ff4f51869cd6) Fixes bug [#464893](https://bugs.kde.org/464893)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Notification: ensure arg is StructureType when decoding ImageHint. [Commit.](http://commits.kde.org/plasma-workspace/55a279591494e227eeaf1f21bc86084eeb0a7c19) Fixes bug [#481033](https://bugs.kde.org/481033)
+ Dataengines/weather: Reset wind info when parsing new data (envcan). [Commit.](http://commits.kde.org/plasma-workspace/7e7c042a93d294cf79ee440ac16383495e5b7294) Fixes bug [#481492](https://bugs.kde.org/481492)
+ Handle previously ignored LC_* values. [Commit.](http://commits.kde.org/plasma-workspace/febe536419921acad74bf5a61c47b7ecfccef843) Fixes bug [#480379](https://bugs.kde.org/480379)
+ Klipper: Expose selected files through desktop portal. [Commit.](http://commits.kde.org/plasma-workspace/c7c8dc6f7a2d7d8afe9a44c9fd2444b12f2514e8) Fixes bug [#476600](https://bugs.kde.org/476600)
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Kbd backlight: Fix double brightness restore on LidOpen-resume. [Commit.](http://commits.kde.org/powerdevil/b30c584d6a724d350b7b8f6a30cc4bbbf9dbac3d) 
{{< /details >}}

