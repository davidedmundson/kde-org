---
title: Plasma 5.25.3 complete changelog
version: 5.25.3
hidden: true
plasma: true
type: fulllog
---
{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ ApplicationResourceButton: switch to individual ToolTip instances. [Commit.](http://commits.kde.org/discover/71623684d6d638481eb007e0e9ee8e54c9ae0274) Fixes bug [#456129](https://bugs.kde.org/456129)
{{< /details >}}

{{< details title="kde-cli-tools" href="https://commits.kde.org/kde-cli-tools" >}}
+ Fix DBus name for KPluginMetaData based KCMs. [Commit.](http://commits.kde.org/kde-cli-tools/e8ee619cbc410f39a264060f18f771546575d268) Fixes bug [#455943](https://bugs.kde.org/455943)
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Wallpapers/potd: only pass 1920x1080 or 3840x2160 to bing provider. [Commit.](http://commits.kde.org/kdeplasma-addons/e70af19f2b5fe62ecb2901fd046e1c0b2275c720) 
+ Switchers/thumbnails: Fix icon cropped when text is large enough. [Commit.](http://commits.kde.org/kdeplasma-addons/e58ad3af86850b21706f139b0025c6c42317cc6d) Fixes bug [#451997](https://bugs.kde.org/451997)
+ Switchers/compact: Fix dialog sizes not getting updated. [Commit.](http://commits.kde.org/kdeplasma-addons/8fd7c783f6f21fac7eb8c6761902942db1ecea2b) Fixes bug [#422447](https://bugs.kde.org/422447)
+ Switchers/thumbnailgrid: Fix layout when window count changes. [Commit.](http://commits.kde.org/kdeplasma-addons/2b0069baa5bf8b901bfffa288fe1188ef3faaeed) Fixes bug [#441241](https://bugs.kde.org/441241)
{{< /details >}}

{{< details title="Info Center" href="https://commits.kde.org/kinfocenter" >}}
+ Remove root Messages.sh there's no "kinfocenter" app anymore. [Commit.](http://commits.kde.org/kinfocenter/9b86662ee15de2ae0ad5c91f6b4ea28bc5923f3f) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ [kcm/kwindesktop] Emit rowsChanged signal to fix default state highlighting. [Commit.](http://commits.kde.org/kwin/b739df05ecf239d064a9619f9454646dd9968581) 
+ Backends/drm: also check for properties in DrmPipeline::needsModeset. [Commit.](http://commits.kde.org/kwin/d0634166267c6ee02b4a4caeaa945e07302e222f) Fixes bug [#455814](https://bugs.kde.org/455814)
+ Backends/drm: handle disconnected but not removed connector objects properly. [Commit.](http://commits.kde.org/kwin/ce7d01df48ff5dc0893a669f7f6faeeee5d7dc60) 
+ Windowview: handle windows from other virtual desktops better. [Commit.](http://commits.kde.org/kwin/03b71af9f47ab86e6525b945ffd1bca17ffc178b) 
+ Map XdgSurface to XdgWMBase instances properly. [Commit.](http://commits.kde.org/kwin/9f4797210b003f362ad8dc01c3c6394e1374470e) Fixes bug [#456349](https://bugs.kde.org/456349)
+ Fix flickering in slide effect with multi screen. [Commit.](http://commits.kde.org/kwin/50f0df8f44fd77272dae00a12dfce2b406db9cc6) 
+ Backends/drm: don't create a new output every time. [Commit.](http://commits.kde.org/kwin/a782d18c592696acf7f786401b72318280858d25) 
+ Wayland: Send drm-lease-device::done event to the correct resource. [Commit.](http://commits.kde.org/kwin/37c6fb5ef8c210af1dd9acc15edd8a0f065d7228) 
+ Backends/drm: don't remove connectors the kernel doesn't consider removed. [Commit.](http://commits.kde.org/kwin/913ca1d6e8cf06602816cd3c7f26b0c2a47bdbd7) Fixes bug [#456298](https://bugs.kde.org/456298)
+ Xdgactivation: Properly prevent disabled activation notifications. [Commit.](http://commits.kde.org/kwin/155b3cd4907e619e2ae0ec4feca873a213f839d0) Fixes bug [#454937](https://bugs.kde.org/454937)
+ TabBox: Do not highlight selected window for fullscreen switchers. [Commit.](http://commits.kde.org/kwin/034e99ba0ca852a047a2b26b8fb3d373afe2ec51) Fixes bug [#449180](https://bugs.kde.org/449180)
+ Ship kconf update script to clean animation factor from kwinrc. [Commit.](http://commits.kde.org/kwin/832e46864e62f9c99147c1621353c8149ed2b2b8) 
+ Fix apply button with animation slider speed. [Commit.](http://commits.kde.org/kwin/c6324da184d2a5bd3bc21d0380a3a7ae4650e6a0) 
+ Write animation speed to kdeglobals. [Commit.](http://commits.kde.org/kwin/67f6a323f83dca99b7502fa1d8fe0bdc1b0db3cf) Fixes bug [#431259](https://bugs.kde.org/431259)
+ Xdgactivation: Do not notify when applications try to activate themselves. [Commit.](http://commits.kde.org/kwin/ea2ec2170c1e6f61fd32398a66669c887e625ac8) 
+ Set all timestamps for all touch events. [Commit.](http://commits.kde.org/kwin/b079c8caa23f8b40733a45159f51607928596a4b) Fixes bug [#456133](https://bugs.kde.org/456133)
+ Backends/drm: fix common mode generation. [Commit.](http://commits.kde.org/kwin/d4d4a05444e18963359f3fa6fe482ed6a419aa19) Fixes bug [#455477](https://bugs.kde.org/455477)
+ Screencast: Do not send events when moving the cursor outside the viewport. [Commit.](http://commits.kde.org/kwin/a500eac16136d2a1be3d13b35fb5870cd8c86135) 
+ Screencasting: Have cursor move frames also send damage information. [Commit.](http://commits.kde.org/kwin/dae1abfdcbd8158017c56685a24ac07cb25d6b6e) 
+ Screencast: Also send the header when we just send the cursor update. [Commit.](http://commits.kde.org/kwin/a915974335d4dc405c6818047cb7791a18fd939e) 
+ Backends/drm: suppress logging for direct scanout. [Commit.](http://commits.kde.org/kwin/e10ecec362bd0bb39d10ad17d4ac6e4f921254db) Fixes bug [#456089](https://bugs.kde.org/456089)
{{< /details >}}

{{< details title="oxygen-sounds" href="https://commits.kde.org/oxygen-sounds" >}}
+ Update kf5 version requirement to 5.94. [Commit.](http://commits.kde.org/oxygen-sounds/a8786bf95003bde03df699e016a9ce510f86c502) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ [applets/pager] Round displayed window geometry to avoid size "jumps". [Commit.](http://commits.kde.org/plasma-desktop/00a3851c911701afe32868f89497b46feff19b73) Fixes bug [#456488](https://bugs.kde.org/456488)
+ [applets/pager] Fix switching desktops on drag & hover. [Commit.](http://commits.kde.org/plasma-desktop/a96da36970a4d87825946df3fb65da230f6fa123) Fixes bug [#416878](https://bugs.kde.org/416878)
+ Applets/kicker: check model count before porting old favorite items. [Commit.](http://commits.kde.org/plasma-desktop/3c4544df712688c0ca8b8ae79566b65c8b8a4f74) Fixes bug [#456411](https://bugs.kde.org/456411)
+ Applets/kickoff: remove highlight visibility conditions. [Commit.](http://commits.kde.org/plasma-desktop/9c9b53b6bf021ddc8709a34db998fb21f4138f61) Fixes bug [#448526](https://bugs.kde.org/448526)
+ Fix translation domain for KRunner KCM. [Commit.](http://commits.kde.org/plasma-desktop/88f624c0056f90ec307eb7631488b87ddac3021e) Fixes bug [#455624](https://bugs.kde.org/455624)
+ Desktoppackage: add `Accessible.name` to email button. [Commit.](http://commits.kde.org/plasma-desktop/ed50ac6ac2500fa40c3c20af43c5bc11a337dbf5) 
+ [kcms/componentchooser] Pass parent window to ksycoca progress dialog. [Commit.](http://commits.kde.org/plasma-desktop/bab58e080592e609a42ee4f8ae9ee03df3975802) 
+ Applets/kickoff: fix grid delegate tooltips not appearing on hover. [Commit.](http://commits.kde.org/plasma-desktop/2008b18bb06a88a9dd09c270fb573cc059154654) 
+ [kcms/tablet] Fix crash when opening KCM for the second time. [Commit.](http://commits.kde.org/plasma-desktop/c6b50001166dbf69ee5d3c00283adffcb6219370) Fixes bug [#451233](https://bugs.kde.org/451233)
+ Applets/kickoff: Prevent empty menu from opening. [Commit.](http://commits.kde.org/plasma-desktop/bc6c5fee3683fc1cf44f971ab5aa02fd1f5b8442) Fixes bug [#455927](https://bugs.kde.org/455927)
+ Applets/taskmanager: press space to activate task. [Commit.](http://commits.kde.org/plasma-desktop/be5c9751cbd15868a0904196d1a7603fb6c41ab3) 
+ Fixup bf55b39: change Kirigami import version to 2.19. [Commit.](http://commits.kde.org/plasma-desktop/ec7b564f043f5252d6dd6a32ba2ea3bec23d3f49) 
+ Panel: import version of Kirigami that supports InputMethod.willShowOnActive. [Commit.](http://commits.kde.org/plasma-desktop/bf55b393134cc4d0f4a0b5c2f31f49894b76f30a) 
+ Make the Keyboard KCM config spare layout spinbox enable the Save button (Fixes #36). [Commit.](http://commits.kde.org/plasma-desktop/b4223f51dcfb2a8ae026b4accb16a75c4796edd1) 
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Lockscreen: Use temporary fix for black background. [Commit.](http://commits.kde.org/plasma-mobile/d85b20409e0daa012ad10cc766331e76e32afa1d) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Guard against cursor theme changing in animation timer. [Commit.](http://commits.kde.org/plasma-workspace/f844007bd8b6d165791e6d09f15c5accf78734eb) Fixes bug [#456526](https://bugs.kde.org/456526)
+ Fix "PanelSpacer::containmentGraphicObject()" plasmashell segfault. [Commit.](http://commits.kde.org/plasma-workspace/db0f5e68f26bc65d666fad1a9f16fbada3c0909b) Fixes bug [#450663](https://bugs.kde.org/450663)
+ Fix StatusNotifierItem MidClick. [Commit.](http://commits.kde.org/plasma-workspace/627d6ab6772bd4088107dbb642871996221a7e92) Fixes bug [#456466](https://bugs.kde.org/456466)
+ Kcms/lookandfeel: Set all defaults when saving the default package. [Commit.](http://commits.kde.org/plasma-workspace/0093ea7db5ad53f2b775c6d1b0a0b59c2bb1b564) Fixes bug [#456275](https://bugs.kde.org/456275)
+ Kcms/colors: Properly apply tinting to the window titlebar. [Commit.](http://commits.kde.org/plasma-workspace/9f46232368583ef97d617782ee29f9eabd20eda8) Fixes bug [#455395](https://bugs.kde.org/455395). Fixes bug [#454047](https://bugs.kde.org/454047)
+ [kcms/icons] Pass parent window to ksycoca progress dialog. [Commit.](http://commits.kde.org/plasma-workspace/c00bfbf619cab34f77121555f205bb783e70b69a) 
+ Applets/kicker: Get rid of a separator just above title menu item. [Commit.](http://commits.kde.org/plasma-workspace/520e30a64a4e7d13c56fb8cd1df27698cae234ea) Fixes bug [#449132](https://bugs.kde.org/449132)
+ Fix password field in lock screen not clearing after failed login attempt. [Commit.](http://commits.kde.org/plasma-workspace/298ffbc22253bb96dc2b08e482856c6a01d250d6) Fixes bug [#455227](https://bugs.kde.org/455227)
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ AppChooser: Address error message. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/908b849f33f7f654afada035e0c1c792afad4d2b) 
+ Screencast: fix window stream restoration. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/20f29eddc3e325a2bd17d810a565f1e3dbb084d0) 
{{< /details >}}

