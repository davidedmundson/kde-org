---
aliases:
- ../announce-3.5.3
custom_about: true
custom_contact: true
date: '2006-05-31'
description: Project Ships Third Translation and Service Release for Leading Free
  Software Desktop.
title: KDE 3.5.3 Release Announcement
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE Project Ships Third Translation and Service Release for Leading Free Software Desktop
</h3>

<p align="justify">
  KDE 3.5.3 features improvements in startup speed, 
  a significant audit of small bugs, and some
  well-tested new functionality.
  
</p>

<p align="justify">
  The <a href="/">KDE
  Project</a> today announced the immediate availability of KDE 3.5.3,
  a maintenance release for the latest generation of the most advanced and
  powerful <em>free</em> desktop for GNU/Linux and other UNIXes. Unusually
  for a maintenance release, new features were implemented due to the long
  release cycle of the eagerly-awaited KDE 4. Stability and speed were also 
  improved, along with increasingly complete translations in 65 languages.
</p>

<p align="justify">
  Significant enhancements include:
</p>

<ul>
  <li>KDE startup sequence reordered to improve startup time.</li>
  <li>Over 800 minor issues fixed thanks to <a href="http://scan.coverity.com">Coverity</a>,
  as part of a project initiated with funding from the United States Government.</li>
  <li>Small new features were implemented in Akregator, KMail and KAlarm. This  release
  alters the tradition of limiting "maintenance releases" to bug fixes. 
  "Feature freeze" was relaxed somewhat to accommodate qualitative improvements 
  held back by the long release cycle of stable 3.5.x series.
  </li>
</ul>

<p align="justify">
  KDE 3.5.3 ships with a basic desktop and fifteen other packages (PIM, administration, 
  network, edutainment, utilities, multimedia, games,
  artwork, web development and more).  KDE's award-winning tools and
  applications are available in 65 languages including
  two new languages: Vietnamese and Kazakh.
</p>

<p align="justify">
  For a more detailed list of improvements since the 
  <a href="../3.5.2">KDE 3.5.2 release</a> in
  March 2006, please refer to the
  <a href="/announcements/changelogs/changelog3_5_2to3_5_3">KDE 3.5.3 Changelog</a>.
</p>

<h4>
  Installing KDE 3.5.3 Binary Packages
</h4>
<p align="justify">
  <em>Package Creators</em>.
  Some operating system vendors have kindly provided binary packages of
  KDE 3.5.3 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  download server at
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.3/">download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now
  available, may become available over the coming weeks.
</p>

<p align="justify">
  <a name="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE
  Project has been informed, please visit the
  <a href="/info/1-2-3/3.5.3">KDE 3.5.3 Info Page</a>.
</p>

<h4>
  Compiling KDE 3.5.3
</h4>
<p align="justify">
  <em>Source Code</em>.
  The complete source code for KDE 3.5.3 may be
  <a href="http://download.kde.org/stable/3.5.3/src/">freely
  downloaded</a>.  Instructions on compiling and installing KDE 3.5.3
  are available from the <a href="/info/1-2-3/3.5.3#binary">KDE
  3.5.3 Info Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a> project that exists and grows only because of the
help of many volunteers that donate their time and effort. KDE
is always looking for new volunteers and contributions, whether its
help with coding, bug fixing or reporting, writing documentation,
translations, promotion, money, etc. All contributions are gratefully
appreciated and eagerly accepted. Please read through the <a href="/community/donations/">Supporting
KDE page</a> for further information. </p>

<p align="justify">
We look forward to hearing from you soon!
</p>

<h4>
  About KDE
</h4>
<p align="justify">
  KDE is an <a href="/community/awards/">award-winning</a>, independent <a href="/people/">project of hundreds</a>
  of developers, translators, artists and other professionals worldwide collaborating over the Internet
  to create and freely distribute a sophisticated, customizable and stable
  desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform.</p>

<p align="justify">
  KDE provides a stable, mature desktop including a state-of-the-art browser
  (<a href="http://konqueror.kde.org/">Konqueror</a>), a personal information
  management suite (<a href="http://kontact.org/">Kontact</a>), a full 
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking application and utilities, and an
  efficient, intuitive development environment featuring the excellent IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.</p>

<p align="justify">
  KDE is working proof
  that the Open Source "Bazaar-style" software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Trademark Notices.</em>
  KDE<sup>&#174;</sup> and the K Desktop Environment<sup>&#174;</sup> logo are 
  registered trademarks of KDE e.V.

Linux is a registered trademark of Linus Torvalds.

UNIX is a registered trademark of The Open Group in the United States and
other countries.

All other trademarks and copyrights referred to in this announcement are
the property of their respective owners.
</font>

</p>

<hr/>

<h4>Press Contacts</h4>
<table cellpadding="10" align="center"><tr valign="top">
<td>

<b>Africa</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="&#x6d;ai&#x6c;t&#x6f;&#00058;&#105;n&#102;&#x6f;&#45;&#x61;&#x66;r&#105;c&#0097;&#x40;k&#100;&#x65;&#x2e;o&#114;g">&#105;&#110;&#0102;&#00111;-&#x61;&#102;ri&#99;a&#x40;kde.o&#114;g</a><br />

</td>

<td>
<b>Asia and India</b><br />
  Pradeepto Bhattacharya<br/>
  A-4 Sonal Coop. Hsg. Society<br/>
  Plot-4, Sector-3,<br/>
  New Panvel,<br/>
  Maharashtra.<br/>
  India 410206<br/>
  Phone : +91-9821033168<br/>
<a href="ma&#0105;&#108;to&#00058;inf&#00111;-&#97;&#115;&#x69;a&#x40;kde.or&#x67;">info-asia kde.org</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europe</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="mai&#108;to&#x3a;in&#x66;o&#x2d;e&#0117;ro&#112;&#x65;&#00064;kde.o&#x72;g">&#x69;n&#102;&#111;-&#x65;u&#114;&#x6f;&#x70;&#101;&#0064;&#107;&#x64;e.&#x6f;&#x72;g</a>
</td>

<td>
<b>North America</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Phone: (416)-925-4030 <br />
<a href="ma&#105;l&#116;&#x6f;:i&#x6e;&#x66;o&#x2d;no&#x72;&#0116;h&#00097;&#x6d;e&#114;i&#099;&#0097;&#64;k&#100;&#101;&#x2e;&#0111;&#114;&#x67;">&#105;&#110;&#102;o-&#00110;or&#116;h&#x61;me&#114;&#105;c&#097;&#x40;&#0107;de.org</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="m&#97;&#105;&#x6c;&#x74;&#111;&#00058;i&#x6e;f&#111;-&#x6f;&#x63;&#x65;&#x61;n&#x69;a&#x40;&#00107;&#x64;&#x65;.or&#0103;">&#105;&#110;fo&#045;oc&#x65;a&#110;&#x69;&#97;&#00064;&#x6b;de&#46;o&#x72;&#103;</a><br />
</td>

<td>
<b>South America</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="&#109;a&#105;&#108;&#x74;&#x6f;&#058;&#105;&#110;f&#x6f;-s&#x6f;u&#x74;&#104;&#x61;m&#x65;&#0114;ica&#x40;k&#x64;e.or&#x67;">&#x69;&#110;&#x66;o-&#115;ou&#00116;ha&#x6d;e&#x72;ica&#064;&#x6b;d&#101;&#0046;&#111;&#114;&#x67;</a><br />
</td>

</tr></table>
