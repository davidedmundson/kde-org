---
qtversion: 5.15.2
date: 2023-08-17
layout: framework
libCount: 83
---


### Baloo

* Document uses and abuses of Baloo::IndexerConfig
* [BalooSearch] Add sort order option
* [Codecs] Make encode/decode methods static, cleanup
* [TermGeneratorTest] Extend phrase coverage
* [AdvancedQueryParserTest] Extend phrase coverage
* [SearchStore] Move private helpers to anonymous namespace
* [BasicIndexingJob] Ignore filename based mimetype for empty files (bug 420339)
* Reduce filesystem dependencies in more tests
* [DocumentUrlDbTest] Remove file system dependencies from test
* [DocumentUrlDb] Split document addition and file tree creation
* [Transaction] Use DocumentUrlDB::contains
* [DocumentUrlDB] Add `contains` method
* Remove proxy for obsolete org.kde.baloo interface
* [Transaction] Remove Transaction::exec convenience method

### Extra CMake Modules

* KDEInstallDirs: fix description of KXMLGUIDIR & LOCALEDIR
* KDEGitCommitHooks: Allow passing in of custom scripts (bug 472361)
* qml-plasmoid techbase.kde.org/Projects/Plasma does not exist

### KArchive

* Support reading file sizes from ZIP64 extended fields

### KCoreAddons

* kurlmimedata: limit amount of in-flight FDs for portal submission (bug 472716)
* kurlmimedata: don't portal symlinks (bug 464225)
* KSignalHandler: possibility to register signal handler as early as possible

### KCrash

* On windows, look for drkonqi.exe

### KDESU

* SuProcess: Disable echo in the PTY before starting sudo (bug 452532)
* PtyProcess: Allow calling enableLocalEcho before PTY creation

### KDE GUI Addons

* fix google-maps-geo-handler

### KHolidays #

* Update Swedish holidays

### KIconThemes

* Do not automatically set breeze as the fallback theme on Android

### KImageFormats

* psd: Fix UB type punning
* Treat 3-channel MCH images as CMY images

### KIO

* Use switcheroo-control to find discrete GPUs (bug 449106)
* file: preserve mode on put (bug 471539)
* KFileWidget: Use targetUrl to extract urls, better handle absolute urls cases (bug 459485)

### Kirigami

* Change Instantiator to Repeater in NavigationTabBar (bug 467860)
* Avatar: Always use uppercase initials

### KJS

* setitimer(, NULL, ) is not supported

### KPackage Framework

* KPackageTool: Show deprecation warning when metadata.desktop file is used

### KPeople

* QML plugin: use KPeople namespace meta object, instead of duplicating

### KRunner

* runnerpython cgit.kde.org does not exist

### KTextEditor

* completion: Fix only start chars of items are matched
* Fix chaotic up/down cursor movement in Block Selection Mode
* Fix backspace behavior for empty lines with cursor beyond line’s end (block selection mode)

### Plasma Framework

* dataengine: do not emit removal signals while iterators are open (bug 446531)

### QQC2StyleBridge

* Don't create Sonnet.Settings if not needed

### Syntax Highlighting

* Hare language syntax highlighting

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
