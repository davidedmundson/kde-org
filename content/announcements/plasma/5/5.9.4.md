---
aliases:
- ../../plasma-5.9.4
changelog: 5.9.3-5.9.4
date: 2017-03-21
layout: plasma
youtube: lm0sqqVcotA
figure:
  src: /announcements/plasma/5/5.9.0/plasma-5.9.png
  class: text-center mt-4
asBugfix: true
---

* Discover: Fix enabling/disabling sources. <a href="https://commits.kde.org/discover/7bdaa6d2f478be5422d4ef002518f2eabb1961dc">Commit.</a> Fixes bug <a href="https://bugs.kde.org/377327">#377327</a>
* [Kicker] Fix highlighting favorites. <a href="https://commits.kde.org/plasma-desktop/db297ab5acb93f88c238778e8682effe3032bf4f">Commit.</a> See bug <a href="https://bugs.kde.org/377652">#377652</a>. Phabricator Code review <a href="https://phabricator.kde.org/D5064">D5064</a>
* System Settings: Set the correct desktop file name when in a KDE session. <a href="https://commits.kde.org/systemsettings/f61f9d8c100fe94471b1a8f23ac905e9311b7436">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D5006">D5006</a>
