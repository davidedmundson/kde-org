---
date: 2023-12-05
changelog: 5.27.9-5.27.10
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Powerdevil Backlighthelper: Implement support for the FreeBSD backlight(8) interface. [Commit.](http://commits.kde.org/powerdevil/105cfe2a6252b06308067037c877c7422d822e17)
+ KWin: Allow running kwin5 windowed in kwin6. [Commit.](http://commits.kde.org/kwin/6de40716b5aaad5df9c75bc39b998bb04f0e60ac)
+ Discover: Support AppStream 1.0 and 0.x versions. [Commit.](http://commits.kde.org/discover/2b7c8508b407453e6b6f1f5f52c75f1575c5821f)
