---
aliases:
- ../../plasma-5.2.2
changelog: 5.2.1-5.2.2
date: '2015-03-24'
layout: plasma
---

{{<figure src="/announcements/plasma/5/5.2.0/full-screen.png" alt="Dual monitor setup" class="text-center" width="600px">}}

{{< i18n_date >}}

{{< i18n "annc-plasma-bugfix-intro" "5" "5.2.2" >}}

{{% i18n "annc-plasma-bugfix-minor-release-1" "5.2" "/announcements/plasma/5/5.2.0" "2015" %}}

{{< i18n "annc-plasma-bugfix-worth-5" >}}

{{< i18n "annc-plasma-bugfix-last" >}}

- Translated documentation is now available
- Create gtk-3.0 directory before putting the settings file into it. <a href='http://quickgit.kde.org/?p=breeze.git&a=commit&h=8614a8245741a1282a75a36cb7c67d181ec435a0'>Commit.</a>
- KScreen: fix rounding error in updateRootProperties(). <a href='http://quickgit.kde.org/?p=kscreen.git&a=commit&h=a2d488623344e968e3e65627824e7ae369247094'>Commit.</a>
- KWin: Fix installation of GHNS material. <a href='http://quickgit.kde.org/?p=kwin.git&a=commit&h=9bddd0fe8a7a909e0704ce215666d4a8a23a8307'>Commit.</a>
- Muon: Wait to fetch the KNS backend until we have OCS providers. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=a644be96c4c446b904a194278f35bb0e0540aabc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344840'>#344840</a>
- Plasma Desktop: Extract UI messages. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&a=commit&h=12f750d497c5f2def14d89ad669057e13197b6f8'>Commit.</a>
- Plasma Networkmanager: Make sure SSID will be displayed properly when using non-ASCII characters. <a href='http://quickgit.kde.org/?p=plasma-nm.git&a=commit&h=cbd1e7818471ae382fb25881c138e0228a44dac4'>Commit.</a> See bug <a href='https://bugs.kde.org/342697'>#342697</a>
