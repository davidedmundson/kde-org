---
aliases:
- ../../plasma-5.20.4
changelog: 5.20.3-5.20.4
date: 2020-12-01
layout: plasma
video: true
asBugfix: true
---

+ Use plasma theme icons in kickoff leave view. [Commit.](http://commits.kde.org/plasma-desktop/2da94367a93d53dd9ede58cc2e38a1b376c963a5) See bug [#429280](https://bugs.kde.org/429280)
+ Weight main categories properly. [Commit.](http://commits.kde.org/kinfocenter/1b666ba9f39d0a1c99820e4350fd90b9bab04f21) Fixes bug [#429153](https://bugs.kde.org/429153)
+ Discover: Display title in application page. [Commit.](http://commits.kde.org/discover/d32292b9808baf57d04f37feb8c90cf5f11bafdd)
