---
aliases:
- ../announce-applications-17.12-rc
custom_spread_install: true
date: 2017-12-01
description: KDE Ships Applications 17.12 Release Candidate.
layout: application
release: applications-17.11.90
title: KDE Ships Release Candidate of KDE Applications 17.12
---

December 1, 2017. Today KDE released the release candidate of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

Check the <a href='https://community.kde.org/Applications/17.12_Release_Notes'>community release notes</a> for information on new tarballs, tarballs that are now KF5 based and known issues. A more complete announcement will be available for the final release

The KDE Applications 17.12 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the release candidate <a href='https://bugs.kde.org/'>and reporting any bugs</a>.

#### Installing KDE Applications 17.12 Release Candidate Binary Packages

<em>Packages</em>.
Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 17.12 Release Candidate (internally 17.11.90) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>.
For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/Binary_Packages'>Community Wiki</a>.

#### Compiling KDE Applications 17.12 Release Candidate

The complete source code for KDE Applications 17.12 Release Candidate may be <a href='http://download.kde.org/unstable/applications/17.11.90/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications/applications-17.11.90'>KDE Applications 17.12 Release Candidate Info Page</a>.
