---
version: "6.1.0"
title: "KDE Frameworks 6.1.0"
type: info/frameworks6
date: 2024-04-12
signer: Jonathan Esk-Riddell
signing_fingerprint: E0A3EB202F8E57528E13E72FD7574483BB57B18D
custom_annc: kdes-6th-megarelease-alpha
draft: false
---
