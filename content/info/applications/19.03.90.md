---
title: "KDE Applications 19.03.90 Info Page"
announcement: /announcements/announce-applications-19.03-rc
layout: applications
signer: Christoph Feck
signing_fingerprint: F23275E4BF10AFC1DF6914A6DBD2CE893E2D1C87
binary_package: false
---
