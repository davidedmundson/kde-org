---
title: "KDE Applications 16.04.3 Info Page"
announcement: /announcements/announce-applications-16.04.3
layout: applications
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
