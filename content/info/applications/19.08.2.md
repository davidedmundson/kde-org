---
title: "KDE Applications 19.08.2 Info Page"
announcement: /announcements/announce-applications-19.08.2
layout: applications
signer: Christoph Feck
signing_fingerprint: F23275E4BF10AFC1DF6914A6DBD2CE893E2D1C87
binary_package: false
---
