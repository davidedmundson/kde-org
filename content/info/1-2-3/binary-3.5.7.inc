<ul>

<!-- ARCH LINUX -->
<li><a href="http://www.archlinux.org/">Arch Linux</a>
  :
  <ul type="disc">
    <li>i686 Packages: <a href="ftp://ftp.archlinux.org/extra/os/i686">ftp://ftp.archlinux.org/extra/os/i686</a></li>
    <li>
      To install: pacman -S kde
    </li>
  </ul>
  <p />
</li>

<!-- Debian -->
<li><a href="http://www.debian.org/">Debian</a>
    <ul type="disc">
      <li>
         Install from Debian Unstable
      </li>
    </ul>
  <p />
</li>

<!-- KUBUNTU -->
<li><a href="http://www.kubuntu.org/">Kubuntu</a>
    <ul type="disc">
      <li>
         Kubuntu 7.04 (Feisty): <a href="http://kubuntu.org/announcements/kde-357.php">i386 and AMD64</a>
      </li>
    </ul>
  <p />
</li>

<!-- Mandriva -->
<li><a href="http://www.mandriva.com/">Mandriva</a>
    <ul type="disc">
      <li>
         Mandriva 2007.1: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.7/Mandriva/">i586</a>
      </li>
    </ul>
  <p />
</li>

<!-- Pardus -->
<li>
 <a href="http://www.pardus.org.tr/">Pardus</a>
    <ul type="disc">
      <li>
         Pardus 2007: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.7/Pardus/">Intel i386</a>
      </li>
    </ul>
  <p />
</li>

<!-- kde-redhat -->
<!--
<li><a href="http://kde-redhat.sourceforge.net/">KDE RedHat (unofficial) Packages</a>:
(<a href="http://apt.kde-redhat.org/apt/kde-redhat/kde-org.txt">README</a>)
<ul type="disc">

 <li>All distributions: 
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/all/">(noarch,SRPMS)</a></li>

 <li>Red Hat Enterprise Linux 4:
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/redhat/kde.repo">(kde.repo)</a>,
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/redhat/4/i386/">(i386)</a>

 	<a href="http://apt.kde-redhat.org/apt/kde-redhat/redhat/4/x86_64/">(x86_64)</a>
 </li>

 <li>Fedora Core 5:
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/kde.repo">(kde.repo)</a>,
 	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/5/i386/">(i386)</a>
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/5/x86_64/">(x64_64)</a>
</li>

 <li>Fedora Core 6:
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/kde.repo">(kde.repo)</a>,
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/6/i386/">(i386)</a>
 	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/6/x86_64/">(x64_64)</a>
</li>

<li>Fedora 7:
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/kde.repo">(kde.repo)</a>,
        <a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/7/i386/">(i386)</a>
        <a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/7/x86_64/">(x64_64)</a>

</li>

</ul>
 <p />
</li>
-->

<!-- SUSE LINUX -->
<li>
  <a href="http://www.opensuse.org">openSUSE/SUSE Linux</a><br>
  The SUSE KDE packages will be publicly developed in the openSUSE project from
now on. This means that the packages are no longer available via the
supplementary tree from ftp.suse.com, but via the <a
href="http://en.opensuse.org/Build_Service">Build Service</a> repositories
from openSUSE. Please read <a
href="http://en.opensuse.org/Build_Service/User">this page</a> for
installation instructions.<br>
  <ul type="disc">
    <li>
        <a
href="http://software.opensuse.org/download/repositories/KDE:/KDE3/openSUSE_10.2/">openSUSE
10.2 YUM repository</a> (64bit and 32bit)
    </li>
    <li>
        <a
href="http://software.opensuse.org/download/repositories/KDE:/KDE3/SUSE_Linux_10.1/">SUSE
Linux 10.1 YUM repository</a> (64bit and 32bit)
    </li>
    <li>
        <a
href="http://software.opensuse.org/download/repositories/KDE:/KDE3/SUSE_Linux_10.0/">SUSE
Linux 10.0 YUM repository</a> (64bit and 32bit)
    </li>
  </ul>
  <p />
</li>


</ul>
