---
title: "KDE 2.0 FAQ"
---

<h2>kdm refuses to let me log in</h2>

Reason: PAM problems (RedHat systems)

Red Hat has released a PAM security update. Download a new RPM matching
your distribution's version from RH's
<a href="http://www.redhat.com/support/errata/">errata</a>.

<h2>Most KDE applications crash immediately</h2>

Reason:<u>Use of Qt 2.2.1</u> 

<p>There was an incompatibility between libpng 1.0.8 or newer and
Qt older than version 2.2.2. This caused every application to crash
as soon as anything png related was invoked. 
To fix this, either downgrade libpng to 1.0.7 or (recommended)
upgrade Qt to version 2.2.2. 
See the <a href="http://www.libpng.org/pub/png/libpng.html">libpng homepage</a>.</p>

<h2>Most KDE applications crash immediately</h2>

Reason:<u>Environment variables set incorrectly</u> 

<p>The environment variables KDEDIR, KDEHOME, PATH and
LD_LIBRARY_PATH, if set, must point to the correct KDE 2.0 
locations. RedHat users should make sure to change their <i>kde.csh</i> 
and <i>kde.sh</i> scripts.</p>

<h2>Most KDE applications crash immediately</h2>

Reason:<u>Missing ksycoca database</u> 

<p>In some installations the KDE program <i>kbuildsycoca</i> fails
to properly create a file called <i>ksycoca</i>. This file is
needed by KDE to operate properly. Depending on the environment
variable <i>$KDEHOME</i> this file should either be found in
<i>~/.kde/share/config/ksycoca</i> or
<i>~/.kde2/share/config/ksycoca</i>.</p>

Make sure that your <i>$KDEHOME</i> environment variable is
always set to the same value. Leaving it empty is OK, it will then
assume <i>~/.kde</i> in which case you should have a recent
<i>ksycoca</i> in <i>~/.kde/share/config/ksycoca</i>.

Run <i>kbuildsycoca</i> from a command line to generate the
<i>ksycoca</i> file.

## DCOPServer fails to start

Reason:<u>Too old version of Qt</u> 

<p>You need to have at least Qt version 2.2.1. Version 2.2.0 is
<b>NOT</b> sufficient, no matter what your RPM says.</p>

## Logout problems

Various people have reported that they are not able to log out from
KDE. Others have reported that logging out takes a very long time.
No exact reason for this failure is currently known. 

## Why can't I hear any sound?

Stefan Westerfeld has compiled a <a
href="http://www.arts-project.org/doc/handbook/faq.html">
aRts FAQ</a> that will help you going. 

<h2>Why is Konqueror/khtml unable to display some images on the
web?</h2>

Reason:<u>You didn't compile Qt to include support for those
formats</u>

Make sure that you included support for them while compiling
Qt.

+ for GIF images, include -gif during configuring (if you have a
GIF license).

+ for JPEG images, make sure that you have jpeglib6b installed
while configuring kdelibs.

+ for MNG movies, make sure that you compiled Qt with
-system-libmng

## Konqueror problems

See the <a href="http://konqueror.kde.org/faq.html">Konqueror
FAQ</a> for Frequently Asked Questions and their answers concerning
KDEs webbrowser and file-manager. 

<h2>KMail and KNode frequently crash (Caldera Open Linux 2.4)</h2>

Reason:<u>A conflict between the mimelib versions distributed with
KDE 1.x and 2.0</u> 

<p>Updated RPMS will be made available soon. Temporary
workaround:</p>

```bash
cd /opt/kde/lib
rm libmimelib.so
ln -s /usr/lib/libmimelib.so.1.0.1  libmimelib.so
rm libmimelib.so.1
ln -s /usr/lib/libmimelib.so.1.0.1  libmimelib.so.1
```

<h2>Logout and other tasks take very long</h2>

Reason:<u>Problems resolving your hostname</u>

Upon certain action KDE does a lookup of your hostname. If you rely on
DNS for resolving and the network is slow or down this will lead to a
immense slowdown.

Fix: add your host with name and IP address to <tt>/etc/hosts</tt>
and make sure that <tt>/etc/host.conf</tt> contains <tt>order hosts,bind</tt>
(or similar).

