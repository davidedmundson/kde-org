Title:           ksmserver: Unauthorized users can access session manager
Risk Rating:     High
CVE:             CVE-2024-36041
Versions:        plasma-workspace <= 6.0.6
Author:          David Edmundson <davidedmundson@kde.org>
Date:            31/05/2024

Overview
========
KSmserver, KDE's XSMP manager, incorrectly allows connections via ICE
based purely on the host, allowing all local connections. This allows
another user on the same machine to gain access to the session
manager.

A well crafted client could use the session restore feature to execute
arbitrary code as the user on the next boot.

Solution
========

For Plasma 6 Either:
 - Update plasma-workspace to 6.0.5.1

 - Apply patch https://invent.kde.org/plasma/plasma-workspace/-/commit/8047b93defa4be4c6c1864e93c41fddcef2969bb AND ensure the binary "iceauth" is installed

 - Apply patches https://invent.kde.org/plasma/plasma-workspace/-/commit/8047b93defa4be4c6c1864e93c41fddcef2969bb AND https://invent.kde.org/plasma/plasma-workspace/-/commit/a061b08e90ec5554d5ff8c57ae1218130c76e27a

For Plasma 5 Either:
 - Update plasma-workspace to 5.27.11.1

 - Apply patch https://invent.kde.org/plasma/plasma-workspace/-/commit/da843d3fdb143ed44094c8e6246cfb8305f6f09f AND ensure the binary "iceauth" is installed

 - Apply patches https://invent.kde.org/plasma/plasma-workspace/-/commit/da843d3fdb143ed44094c8e6246cfb8305f6f09f AND https://invent.kde.org/plasma/plasma-workspace/-/commit/1d5aa1d27bff87b2d242ed759cfb2ce15a5c3de7

Credits
=======
Thanks to Fabian Vogt for the report and work on the patches.
