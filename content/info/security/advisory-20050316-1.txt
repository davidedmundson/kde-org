-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

KDE Security Advisory: Local DCOP denial of service vulnerability
Original Release Date: 20050316
URL: http://www.kde.org/info/security/advisory-20050316-1.txt

0. References
        http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2005-0396


1. Systems affected:

        All KDE version prior to KDE 3.4 on systems where multiple users
        have access.


2. Overview:

        Sebastian Krahmer of the SUSE LINUX Security Team reported a local
        denial of service vulnerability in KDE's Desktop Communication
        Protocol (DCOP) daemon better known as dcopserver.

        A local user can lock up the dcopserver of arbitrary other users
        on the same machine by stalling the DCOP authentication process.

        Although it is not possible to by pass the authentication process
        this way, it can cause a significant reduction in desktop
        functionality for the affected users.

        The Common Vulnerabilities and Exposures project (cve.mitre.org)
        has assigned the name CAN-2005-0396 to this issue.

      
3. Impact:

        A local user can lock up the dcopserver of arbitrary other users
        on the same machine. This can cause a significant reduction in
        desktop functionality for the affected users including, but not
        limited to, the inability to browse the internet and the inability
        to start new applications.


4. Solution:

        Upgrade to KDE 3.4.

        For older versions of KDE Source code patches have been made
        available which fix these vulnerabilities. Contact your OS vendor /
        binary package provider for information about how to obtain updated
        binary packages.


5. Patch:

        A patch for KDE 3.1.x is available from
        ftp://ftp.kde.org/pub/kde/security_patches

        377c49d8224612fbf09f70f3c09d52f5  post-3.1.5-kdelibs-dcop.patch

        A patch for KDE 3.2.x is available from
        ftp://ftp.kde.org/pub/kde/security_patches

        0948701bffb082c65784dc8a2b648ef0  post-3.2.3-kdelibs-dcop.patch

        A patch for KDE 3.3.x is available from
        ftp://ftp.kde.org/pub/kde/security_patches

        7309e259ae1f29be08bbb70e580da3fb  post-3.3.2-kdelibs-dcop.patch


6. Time line and credits:

        21/02/2005 KDE Security informed by SUSE LINUX.
        21/02/2005 Patches applied to KDE CVS.
        02/03/2005 Vendors notified
        16/03/2005 KDE Security Advisory released.
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.0 (GNU/Linux)

iD8DBQFCOBbvN4pvrENfboIRAsQvAJ9FM5bL5Df4JgklNr3v5u6uOdLUDACeLBTE
s+amHw7dStDCkECtiKr5G5U=
=TiOt
-----END PGP SIGNATURE-----
